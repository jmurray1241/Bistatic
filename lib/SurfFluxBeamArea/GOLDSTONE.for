
      IMPLICIT NONE

      REAL*8 SENSOR_LAT_DEG,SENSOR_AZ_DEG,SENSOR_EL_DEG
      REAL*8 SENSOR_FULL_BEAM_WIDTH_DEG
      REAL*8 ALT1_KM,ALT2_KM
      REAL*8 AREA_KM2,RNG1_KM,RNG2_KM
      INTEGER JALT
      
C  GOLDSTONE PARAMETERS
      SENSOR_LAT_DEG = 35.42399D0
      SENSOR_AZ_DEG = 89.902573360199995D0
      SENSOR_EL_DEG = 75.011983817649991D0
      SENSOR_FULL_BEAM_WIDTH_DEG = 0.037D0
	  
C  SENSOR_LAT_DEG taken from Matney's circincs program
C  SENSOR_AZ_DEG and SENSOR_EL_DEG are averages of DSS-14 and DSS-15 azimuths and elevations
C  SENSOR_FULL_BEAM_WIDTH_DEG taken as the 6 dB beamwidth of the gain product of DSS-14 and DSS-15 assuming airy disk gain patterns

      OPEN(UNIT=11,FILE='GOLDSTONE_AREAS_75E.CSV',STATUS='UNKNOWN')
      WRITE(11,*) 'ALT1_KM, ALT2_KM, RNG1_KM, RNG2_KM, AREA_KM^2'

      DO JALT = 1,100

        ALT1_KM = DBLE(JALT - 1) * 50.0D0
        ALT2_KM = DBLE(JALT) * 50.0D0

        CALL SURF_FLUX_BEAM_AREA
     &   (SENSOR_LAT_DEG,SENSOR_AZ_DEG,SENSOR_EL_DEG,
     &    SENSOR_FULL_BEAM_WIDTH_DEG,
     &    ALT1_KM,ALT2_KM,
     &    AREA_KM2,RNG1_KM,RNG2_KM)

        WRITE(11,80) ALT1_KM,ALT2_KM,RNG1_KM,RNG2_KM,AREA_KM2

      END DO
      
      CLOSE(11,STATUS='KEEP')
      
C      PAUSE
80	  format(f8.3,',',f8.3,',',f9.4,',',f9.4,',',f13.7)

      
      END
