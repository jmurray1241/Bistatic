C**************************************************************
C  Compute the area of a beam segment to calculate the surface
C  area flux used by ORDEM and Haystack
C
C  The program assumes a narrow beam, and approximates the angle
C  between the beam pointing and the Earth-radius vector is constant
C  throughout the beam segment.
C
C  The model assumes an elliptical Earth, so altitude of sensor
C  ignored
C
C  Inputs:
C    SENSOR_LAT_DEG - telescope latitude [deg]
C    SENSOR_AZ_DEG - telescope pointing azimuth [deg]
C    SENSOR_EL_DEG - telescope pointing elevation [deg]
C    SENSOR_FULL_BEAM_WIDTH_DEG - telescope full beam width [deg]
C    ALT1_KM - lower altitude of bin [km]
C    ALT2_KM - upper altitude of bin [km]
C  Outputs:
C    AREA_KM2 - surface area used for flux calculations [km^2]
C    RNG1_KM - lower range of bin [km]
C    RNG2_KM - upper range of bin [km]
C
C**************************************************************
C  Mark Matney
C  NASA ODPO
C  March 22, 2019
C**************************************************************

      SUBROUTINE SURF_FLUX_BEAM_AREA
     &  (SENSOR_LAT_DEG,SENSOR_AZ_DEG,SENSOR_EL_DEG,
     &   SENSOR_FULL_BEAM_WIDTH_DEG,
     &   ALT1_KM,ALT2_KM,
     &   AREA_KM2,RNG1_KM,RNG2_KM)

      IMPLICIT NONE

      REAL*8 SENSOR_LAT_DEG,SENSOR_AZ_DEG,SENSOR_EL_DEG
      REAL*8 SENSOR_FULL_BEAM_WIDTH_DEG
      REAL*8 ALT1_KM,ALT2_KM
      REAL*8 AREA_KM2,RNG1_KM,RNG2_KM

      REAL*8 U_RAD,RTEL_KM(1:3)
      REAL*8 CAZ_TEL,SAZ_TEL
      REAL*8 CEL_TEL,SEL_TEL

      REAL*8 NORTH(1:3)
      REAL*8 EAST(1:3)
      REAL*8 UP(1:3)
      REAL*8 LOOK_N(1:3)

      REAL*8 RNG_KM_FROM_ALT_KM

      REAL*8 RAD_KM,R_KM(1:3)
      REAL*8 RNGDOTR
      
      INTEGER J

      REAL*8 DTR,PI,RE_KM
      PARAMETER (RE_KM = 6378.135D0)

C**************************************************************

      PI = ACOS(-1.0D0)
      DTR = PI / 180.0D0

C  Earth ellipse conversions from "Astronomical Algorithms", Meeus, 1998
C  Telescope position is defined in artificial X-Z plane,
C  ignoring altitude of observer
      U_RAD = ATAN(0.99664719D0 * TAN(SENSOR_LAT_DEG * DTR))
      RTEL_KM(1) = RE_KM * COS(U_RAD)
      RTEL_KM(2) = 0.0D0
      RTEL_KM(3) = RE_KM * SIN(U_RAD) * 0.99664719D0

      CAZ_TEL = COS(SENSOR_AZ_DEG * DTR)
      SAZ_TEL = SIN(SENSOR_AZ_DEG * DTR)

      CEL_TEL = COS(SENSOR_EL_DEG * DTR)
      SEL_TEL = SIN(SENSOR_EL_DEG * DTR)

      NORTH(1) = -SIN(SENSOR_LAT_DEG * DTR)
      NORTH(2) = 0.0D0
      NORTH(3) = COS(SENSOR_LAT_DEG * DTR)

      EAST(1) = 0.0D0
      EAST(2) = 1.0D0
      EAST(3) = 0.0D0

      UP(1) = EAST(2) * NORTH(3) - EAST(3) * NORTH(2)
      UP(2) = EAST(3) * NORTH(1) - EAST(1) * NORTH(3)
      UP(3) = EAST(1) * NORTH(2) - EAST(2) * NORTH(1)

      LOOK_N(1) = CAZ_TEL * CEL_TEL * NORTH(1)
     &          + SAZ_TEL * CEL_TEL * EAST(1)
     &          + SEL_TEL * UP(1)
      LOOK_N(2) = CAZ_TEL * CEL_TEL * NORTH(2)
     &          + SAZ_TEL * CEL_TEL * EAST(2)
     &          + SEL_TEL * UP(2)
      LOOK_N(3) = CAZ_TEL * CEL_TEL * NORTH(3)
     &          + SAZ_TEL * CEL_TEL * EAST(3)
     &          + SEL_TEL * UP(3)

      RNG1_KM = RNG_KM_FROM_ALT_KM(ALT1_KM,LOOK_N,RTEL_KM)
      RNG2_KM = RNG_KM_FROM_ALT_KM(ALT2_KM,LOOK_N,RTEL_KM)

      RAD_KM = 0.0D0
      DO J = 1,3
          R_KM(J) = RTEL_KM(J)
     &      + 0.5D0 * (RNG1_KM + RNG2_KM) * LOOK_N(J)
          RAD_KM = RAD_KM + R_KM(J)**2
      END DO
      RAD_KM = SQRT(RAD_KM)

C  COS OF ANGLE BETWEEN SENSOR BORESIGHT ANGLE AND RADIUS FROM CENTER OF EARTH
      RNGDOTR = ( LOOK_N(1) * R_KM(1)
     &          + LOOK_N(2) * R_KM(2)
     &          + LOOK_N(3) * R_KM(3) ) / RAD_KM

      AREA_KM2 = (SENSOR_FULL_BEAM_WIDTH_DEG * DTR)
     &         * (PI / 2.0D0)
     &         * (RNG2_KM**2 - RNG1_KM**2)
     &         * RNGDOTR  ! CORRECTION FOR VERTICAL ANGLE

      RETURN
      END

C**************************************************************
c  Convert an altitude to a sensor range for a given sensor
c  location and pointing angle

      REAL*8 FUNCTION RNG_KM_FROM_ALT_KM(ALT_KM,LOOK_N,RTEL_KM)

      IMPLICIT NONE

      REAL*8 ALT_KM
      REAL*8 LOOK_N(1:3),RTEL_KM(1:3),RAD(1:3)
      REAL*8 RE_KM
      REAL*8 XXX
      REAL*8 RNG1,RNG2,RNG3
      REAL*8 ALT1,ALT2,ALT3

      PARAMETER (RE_KM = 6378.135D0)

      INTEGER  J

      RNG1 = 0.0D0
      XXX = 0.0D0
      DO J = 1,3
        RAD(J) = RTEL_KM(J) + RNG1 * LOOK_N(J)
        XXX = XXX + RAD(J)**2
      END DO
      ALT1 = SQRT(XXX) - RE_KM

      RNG3 = 2.0D0 * ALT_KM + 1.0D0
 10   XXX  = 0.0D0
      DO J = 1,3
        RAD(J) = RTEL_KM(J) + RNG3 * LOOK_N(J)
        XXX  = XXX + RAD(J)**2
      END DO
      ALT3 = SQRT(XXX) - RE_KM

      IF (ALT3 .LT. ALT_KM) THEN  ! not yet bracketed
        RNG3 = 2.0D0 * RNG3
        GOTO 10
      ENDIF

C  Answer bracketed, begin iterations
 20   RNG2 = 0.5D0 * (RNG1 + RNG3)

      IF ((RNG3 - RNG1) < 1.0D-6) GOTO 30

      XXX = 0.0D0
      DO J = 1,3
        RAD(J) = RTEL_KM(J) + RNG2 * LOOK_N(J)
        XXX  = XXX + RAD(J)**2
      END DO
      ALT2 = SQRT(XXX) - RE_KM

      IF (ALT2 .LT. ALT_KM) THEN
        ALT1 = ALT2
        RNG1 = RNG2
        GOTO 20
      ELSE
        IF (ALT2 .GT. ALT_KM) THEN
          ALT3 = ALT2
          RNG3 = RNG2
          GOTO 20
        ELSE
          GOTO 30  !  Exact match!
        ENDIF
      ENDIF

 30   RNG_KM_FROM_ALT_KM = RNG2

      RETURN
      END

