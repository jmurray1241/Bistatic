# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:30:55 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.HUSIR)
RX = deepcopy(obs.AO)


# intersection_altitude_km = 1000.  # km
intersection_altitude_km = 997.5  # km

RX_azimuth   = 0.  # deg
RX_elevation = 90.  # deg


RX.set_pointing(RX_azimuth, RX_elevation)

bs.point_ant2ant(RX.orbit_alt2slant(intersection_altitude_km), RX, TX)

AO_X_receiver     = Receiver(T_sys=30.0, BW= 2.4*bs.GHz, peak_gain=bs.G_k2G(4.,3*bs.cm), HPBW= 0.57*bs.arcmin, band='X')
HUSIR_transmitter = Transmitter(power=250*bs.kW, pulse_length=1.6384*bs.ms, wavelength=3.0*bs.cm, peak_gain=bs.dB2lin(67.23), band='X')

TX.set_transmitter( HUSIR_transmitter)
RX.set_receiver(AO_X_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\HUSIR-Arecibo"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)