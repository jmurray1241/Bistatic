# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:28:15 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.AO)
RX = deepcopy(obs.GBT)


intersection_altitude_km = 1000.  # km
TX_azimuth   = 0.  # deg
TX_elevation = 90.  # deg


TX.set_pointing(TX_azimuth, TX_elevation)

bs.point_ant2ant(TX.orbit_alt2slant(intersection_altitude_km), TX, RX)

AO_S_transmitter   = Transmitter(power=1*bs.MW, pulse_length=3*bs.ms, BW=None, wavelength=bs.f2lam(2380*bs.MHz), peak_gain=bs.dB2lin(73.7), band='S')
GBT_S_receiver     = Receiver(T_sys=18.0, BW=300*bs.MHz, peak_gain=bs.G_k2G(2.0,bs.f2lam(2380*bs.MHz)), HPBW= 5.8*bs.arcmin, band='S')

TX.set_transmitter(AO_S_transmitter)
RX.set_receiver(GBT_S_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\GBT"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
    
import matplotlib.pyplot as plt
   
plt.close('all')