# -*- coding: utf-8 -*-
"""
Created on Fri Nov 09 15:23:59 2018

@author: jimurray
"""

import observer2 as obsimport bistatic as bs
import numpy as np
import matplotlib.pyplot as plt

from conversion import MHz


sizes = np.logspace(-3.,0., 100)
#sizes = np.array([.00316,.005,.01,.0316,.05,.1])


bs.point_ant2ant(1000., obs.AO, obs.VLBI)

R = obs.VLBI.alt2slant(obs.AO.slant2alt(1000.))

obs.VLBI.set_transmitter(obs.tx.VLBI_L)
obs.AO.set_receiver(obs.rx.AO_ALFA)
obs.AO.receiver.BW = obs.VLBI.transmitter.BW

L = bs.powerVsize(sizes,obs.VLBI,obs.AO, R = R)

obs.VLBI.set_transmitter(obs.tx.VLBI_S)
obs.AO.set_receiver(obs.rx.AO_S)
obs.AO.receiver.BW = obs.VLBI.transmitter.BW

S = bs.powerVsize(sizes,obs.VLBI,obs.AO, R = R)

obs.VLBI.set_transmitter(obs.tx.VLBI_C)
obs.AO.set_receiver(obs.rx.AO_C)
obs.AO.receiver.BW = obs.VLBI.transmitter.BW

C = bs.powerVsize(sizes,obs.VLBI,obs.AO, R = R)

obs.VLBI.set_transmitter(obs.tx.VLBI_X)
obs.AO.set_receiver(obs.rx.AO_X)
obs.AO.receiver.BW = obs.VLBI.transmitter.BW

X = bs.powerVsize(sizes,obs.VLBI,obs.AO, R = R)

powers = [L, S, C, X]
labels = ["L-band", "S-band", "C-band", "X-band"]

for power,label in zip(powers,labels):
#    print label
#    for size , p in zip(sizes,power):
#        print size, p
    
    plt.semilogx(sizes,10*np.log10(power),label = obs.VLBI.name + ": " + label)
    
plt.xlabel("Size [m]" , fontsize = 24)
plt.ylabel("Power [dBW]" , fontsize = 24)
#plt.title("Power History for " + RADAR , fontsize = 24)
plt.xticks(fontsize = 20)
plt.yticks(fontsize = 20)
plt.legend(fontsize = 20)
plt.grid()
plt.tight_layout()