# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 16:33:01 2021

@author: jimurray
"""

import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.AO)
RX = deepcopy(obs.AO)


intersection_altitude_km = 1000.  # km
TX_azimuth   = 0.  # deg
TX_elevation = 90.  # deg

RX_azimuth   = 0.  # deg
RX_elevation = 90.  # deg

TX.set_pointing(TX_azimuth, TX_elevation)
RX.set_pointing(RX_azimuth, RX_elevation)

AO_UHF_receiver    = Receiver(T_sys=66.0, BW=   2*bs.MHz, peak_gain=bs.G_k2G(20.,bs.f2lam(430.*bs.MHz)),   HPBW=12.24*bs.arcmin, band='UHF')
NGAT_UHF_transmitter = Transmitter(power=10*bs.MW, pulse_length=2.1*bs.ms, BW=None, wavelength=bs.f2lam( 430*bs.MHz), peak_gain=bs.G_k2G(20.,bs.f2lam(430.*bs.MHz)), band='UHF')

TX.set_transmitter(NGAT_UHF_transmitter)
RX.set_receiver(AO_UHF_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\NGAT UHF"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
import matplotlib.pyplot as plt
   
plt.close('all')