#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 01:21:04 2020

@author: jamesmurray
"""

import os

import numpy as np
import pandas as pd

from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.config as config

from time import time

from scipy.optimize import curve_fit

def func(x, a, b):
     return 1 + a/(x)**b
 
def func_time(x, a,b,c):
    return a*x**2+b*x+c
 
def min_filter(buf, span = 3., normalized = False):
    filt_buf = zeros(len(buf))
    for i in range(len(buf)):
        start = int(max([0,i-span/2]))
        stop = int(min([len(buf), i+span/2]))
        filt_buf[i] = np.min(buf[start:stop])
    return filt_buf    


HUSIR_75E = deepcopy(obs.HUSIR)
HUSIR_75E.set_pointing(90.,75.)
HUSIR_75E.name = "HUSIR 75E"
#ant = HUSIR_75E
HUSIR_10S = deepcopy(obs.HUSIR)
HUSIR_10S.set_pointing(180.,10.)
HUSIR_10S.name = "HUSIR 10S"

HUSIR_20S = deepcopy(obs.HUSIR)
HUSIR_20S.set_pointing(180.,20.)
HUSIR_20S.name = "HUSIR 20S"

start = 50
stop = 1950
step = 1
bigstep = 50
starts = np.arange(start, stop+bigstep, bigstep)
areasValtitude = []
stop = start + bigstep

Samples = np.arange(1,501,1)

altitudes = np.arange(start, stop+step, step, dtype = 'float')


times = []
for ant in [HUSIR_75E, HUSIR_10S, HUSIR_20S]:
    areasVsamples = []

    for samples in Samples:
        start_time = time()
        print("Start Altitude: %i km   Samples: %i " % (int(start), int(samples)))
        area = bs.areaValt(altitudes, ant, ant, samples = samples, plot = False, supress_output = True)[:-1]
        areasVsamples.append(area)
        stop_time = time()
        times.append(stop_time - start_time)

    areasVsamples = np.array(areasVsamples).T[0]
    areasValtitude.append(areasVsamples/max(areasVsamples))
    plot(Samples, areasValtitude[-1], label = ant.name)
    np.savetxt(os.path.join(config.path,'results','error_analysis','areaVsamples_output '+ant.name+'.csv'), np.array([Samples,areasValtitude[-1]]).T, delimiter = ',')

    


#areasValtitude = areasValtitude[0]
#popt, pcov = curve_fit(func, Samples[100:], areasValtitude[100:])
#print popt


np.savetxt(os.path.join(config.path,'results','error_analysis','areaVsamples_output.csv'), np.array([Samples,areasValtitude]).T, delimiter = ',')
