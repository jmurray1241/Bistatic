# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 19:03:12 2020

@author: jimurray
"""

import os

import numpy as np
import pandas as pd

from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.config as config

HUSIR_75E = deepcopy(obs.HUSIR)
HUSIR_75E.set_pointing(90.,75.)

HUSIR_20S = deepcopy(obs.HUSIR)
HUSIR_20S.set_pointing(180.,20.)

HUSIR_10S = deepcopy(obs.HUSIR)
HUSIR_10S.set_pointing(180.,10.)


start = 0
stop = 2500
step = 1
bigstep = 50
samples = 29
width = .15

altitudes = np.arange(start, stop+step, step, dtype = 'float')

start_altitudes = altitudes[:-1].reshape((stop-start)/bigstep, bigstep).T[0]
stop_altitudes  =  altitudes[1:].reshape((stop-start)/bigstep, bigstep).T[bigstep-1]

names = ['ALT1_KM', 'ALT2_KM', 'RNG1_KM', 'RNG2_KM', 'AREA_KM^2']

ants = [HUSIR_75E, HUSIR_20S, HUSIR_10S]
filenames = ['HAYSTACK_AREAS_75E_bistatic.CSV', 'HAYSTACK_AREAS_20S_bistatic.CSV', 'HAYSTACK_AREAS_10S_bistatic.CSV']
filenames = ['HAYSTACK_AREAS_75E_bistatic_vector.CSV', 'HAYSTACK_AREAS_20S_bistatic_vector.CSV', 'HAYSTACK_AREAS_10S_bistatic_vector.CSV']

for i, ant in enumerate(ants):
    
    area = bs.areaValt2(altitudes, ant, ant, samples = samples, plot = False, width = width)[:-1]
    area = np.sum(area.reshape((stop-start)/bigstep, bigstep), axis = 1)
    start_ranges = ant.orbit_alt2slant(start_altitudes)
    stop_ranges  = ant.orbit_alt2slant(stop_altitudes)
    
    ants[i].area = pd.DataFrame(np.array([start_altitudes, stop_altitudes, \
                   start_ranges, stop_ranges, area]).T, columns = names)
    
    ants[i].area.to_csv(os.path.join(config.path,'results','Beam_Areas', filenames[i]), index = False)
    print(area)