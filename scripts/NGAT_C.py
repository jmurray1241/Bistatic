# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:48:19 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.AO)
RX = deepcopy(obs.AO)


intersection_altitude_km = 1000.  # km
TX_azimuth   = 0.  # deg
TX_elevation = 90.  # deg


TX.set_pointing(TX_azimuth, TX_elevation)

bs.point_ant2ant(TX.orbit_alt2slant(intersection_altitude_km), TX, RX)

NGAT_C_transmitter = Transmitter(power=  5*bs.MW, pulse_length=3.0*bs.ms, BW=None, wavelength=bs.f2lam(5000*bs.MHz), peak_gain=bs.G_k2G(18.,bs.f2lam(5000*bs.MHz)), band='C')
NGAT_C_receiver    = Receiver(T_sys=25.0, BW= 10*bs.MHz, peak_gain=bs.G_k2G(18.,bs.f2lam(5000*bs.MHz)), HPBW=1.0*bs.arcmin, band='C')

TX.set_transmitter(NGAT_C_transmitter)
RX.set_receiver(NGAT_C_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

RX.receiver.T_sys = 186.
TX.transmitter.pulse_length = 1.6384*bs.ms


flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\NGAT C Monostatic"
output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\High System Temp\\C"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
import matplotlib.pyplot as plt
   
plt.close('all')