# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:28:15 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.GBT)
RX = deepcopy(obs.GBT)


intersection_altitude_km = 1000.  # km
TX_azimuth   = 90.  # deg
TX_elevation = 75.  # deg


TX.set_pointing(TX_azimuth, TX_elevation)

bs.point_ant2ant(TX.orbit_alt2slant(intersection_altitude_km), TX, RX)

GBT_Ka_transmitter = Transmitter(power=  0.7*bs.kW, pulse_length=100*bs.us, BW=None, wavelength=bs.f2lam(35*bs.GHz), peak_gain=bs.dB2lin(87.8), band='Ka')
GBT_Ka_receiver     = Receiver(T_sys=186.0, BW=300*bs.MHz, peak_gain=bs.dB2lin(87.8), HPBW= 0.29*bs.arcmin, band='Ka')

TX.set_transmitter( GBT_Ka_transmitter)
RX.set_receiver(GBT_Ka_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\GBT CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\GBT\\700 W"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
import matplotlib.pyplot as plt
   
plt.close('all')