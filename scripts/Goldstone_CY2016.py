# -*- coding: utf-8 -*-
"""
Created on Mon May 31 15:47:21 2021

@author: jimurray
"""

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
import bistatic.SurfFluxBeamArea as sf

TX = obs.DSS_14
RX = obs.DSS_15
RX.receiver.SNR_min = 12.
RX.receiver.BW = TX.transmitter.BW

#actual power in CY2016
TX.transmitter.power = 440000.


intersection_altitude_km = TX.slant2orbit_alt(550.) # km
TX_azimuth   = 90. # deg
TX_elevation = 75. # deg

bs.point_ant2ant(550., TX, RX)



flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Goldstone CY2018\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Goldstone CY2016"

# fc.standard_flux_calculations(intersection_altitude_km, TX, RX, flux_file, output_directory)

start = 
bs_area = bs.bistatic_observer_area(start, stop, step, TX, RX, steps, samples, gain_pattern = airy, debug = None, renormalized = False)
sf_area = sf.observer_area(start, stop, step, ant)