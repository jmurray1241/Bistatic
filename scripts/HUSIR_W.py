# -*- coding: utf-8 -*-
"""
Created on Sat May 29 16:16:36 2021

@author: jimurray
"""

import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver
# if 'pfm' not in locals():
#     cwd = os.getcwd()
#     os.chdir("Z:\RADAR\PYTHON\Data Processing")
#     import plots_for_matney_2 as pfm
#     os.chdir(cwd)


TX = deepcopy(obs.HUSIR)
RX = deepcopy(obs.HUSIR)

W_transmitter = Transmitter(power=  1*bs.kW, pulse_length=1000*bs.us, BW=None, wavelength=bs.f2lam(96*bs.GHz), peak_gain=bs.dB2lin(89), band='Ka')
W_receiver     = Receiver(T_sys=186.0, BW=300*bs.MHz, peak_gain=bs.dB2lin(89), HPBW= 0.29*bs.arcmin, band='Ka')

TX.set_transmitter( W_transmitter)
RX.set_receiver(W_receiver)

# RX.receiver.SNR_min = 10**(5.65/10)
RX.receiver.SNR_min = 10**(11.670599913279624/10)
RX.receiver.BW = TX.transmitter.BW
RX.receiver.loss = 10**((3.9 + 6.5 - .4-1)/10)

intersection_altitude_km = 1000.  # km
TX_azimuth   = 90.  # deg
TX_elevation = 75.  # deg

RX_azimuth   = 90.  # deg
RX_elevation = 75.  # deg

TX.set_pointing(TX_azimuth, TX_elevation)
RX.set_pointing(RX_azimuth, RX_elevation)


flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\HUSIR CY2018\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\HUSIR W"
#output_directory = "G:\\results\\HUSIR CY2018"

sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              verbose = True)

