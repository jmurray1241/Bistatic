# -*- coding: utf-8 -*-
"""
Created on Wed May 16 11:17:14 2018

@author: jimurray
"""
import matplotlib.pyplot as plt
from numpy import *
import bistatic as bs
import observer2 as obs





ranges = array([760., 980., 1770., 2120., 3670.])


dis, az = bs.baseline_bearing(obs.DSS_25, obs.DSS_14)
obs.DSS_25.set_pointing(az+180,20)


alt = arange(200,1300,100, 'float')
altitude = 1000.
ranges = obs.DSS_25.alt2slant(alt)
R = arange(obs.DSS_25.alt2slant(10),obs.DSS_25.alt2slant(6200),50,'float')

area = []
elevations = arange(10,50,5,'float')
area = zeros((len(elevations), len(R)))
altitudes = zeros_like(area)
i=0



for elevation in elevations:
    
    blah = []
    obs.DSS_25.set_pointing(az+180,elevation)
    bs.point_ant2ant(obs.DSS_25.alt2slant(altitude), obs.DSS_25,obs.DSS_14)
    area[i,:] = bs.areaVrange_tx(R,obs.DSS_25, obs.DSS_14, samples = 50)
    for r in R:
        blah.append(obs.DSS_25.slant2alt(r))
    
    altitudes[i,:] = obs.nv2vec(array(blah))
    i = i+1
    


#for Range in reversed(ranges):
#    bs.point_ant2ant(Range, obs.DSS_25,obs.DSS_14)
#    
#    blah = []
#
#    for r in R:
#        blah.append(obs.DSS_25.slant2alt(r))
#    blah = array(blah)
#    #area.append(sum(bs.areaVrange_tx(R,obs.DSS_25, obs.DSS_14, samples = 100)))
#    #area[i,:] = bs.areaVrange_tx(R,obs.DSS_25, obs.DSS_14, samples = 50)
#    i = i+1
#    gain = bs.gainVrange(R,obs.DSS_25, obs.DSS_14, plot = False)
#    
#    plt.subplot(211)
#    plt.plot(R,10*log10(gain), label = 'Intersection: ' +str(round(obs.DSS_25.slant2alt(Range)))+ ' km altitude')
#    
#    plt.subplot(212)
#    plt.plot(blah,10*log10(gain), label = 'Intersection: ' +str(round(obs.DSS_25.slant2alt(Range)))+ ' km altitude')
#
#    
#plt.subplot(211)
#plt.xticks(fontsize = 20)
#plt.xlabel('DSS 25 Slant Range [km]', fontsize = 20)
#plt.yticks(fontsize = 20)
#plt.ylabel("Gain [dB]", fontsize = 20)
#plt.grid()
#plt.legend()
#plt.show()
#
#subplot(212)
#plt.xticks(fontsize = 20)
#plt.xlabel('Altitude [km]', fontsize = 20)
#plt.yticks(fontsize = 20)
#plt.ylabel("Gain [dB]", fontsize = 20)
#plt.grid()
#plt.legend()
#plt.show()