# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 15:27:01 2018

@author: jimurray
"""

import itur
import observer2 as obs
import numpy as np
from conversion import speed_of_light, GHz

frequency_GHz = speed_of_light/obs.HUSIR.wavelength/GHz
diameter = 2*obs.HUSIR.radius

p = np.logspace(-1.5, 1.5, 100)# Percetage of time the attenuation value is exceeded

p = .001

azimuths   = [90.,180.,180.]
elevations = [75., 20., 10.]

attenuations  = []

for azimuth, elevation in zip(azimuths,elevations):
    obs.HUSIR.set_pointing(azimuth, elevation)
    
    attenuations.append(2*itur.atmospheric_attenuation_slant_path(\
                        obs.HUSIR.latitude, obs.HUSIR.longitude,  \
                        frequency_GHz , obs.HUSIR.elevation, p, \
                        diameter, Ls = obs.HUSIR.alt2slant([1000.]), \
                        include_rain = False, include_scintillation = True, \
                        include_clouds = True))
    
    print attenuations[-1]
    
#for i in range(len(attenuations)):
#    print -attenuations[0]+attenuations[i]