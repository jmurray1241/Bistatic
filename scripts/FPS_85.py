# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:35:35 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.FPS_85)
RX = deepcopy(obs.AO)


# intersection_altitude_km = 1000.  # km
intersection_altitude_km = 997.5  # km
RX_azimuth   = 0.  # deg
RX_elevation = 90.  # deg


RX.set_pointing(RX_azimuth, RX_elevation)

bs.point_ant2ant(RX.orbit_alt2slant(intersection_altitude_km), RX, TX)

AO_UHF_receiver    = Receiver(T_sys=66.0, BW=   2*bs.MHz, peak_gain=bs.dB2lin(58.9),   HPBW=12.24*bs.arcmin, band='UHF')
FPS_85_transmitter = Transmitter(power=32*bs.MW, pulse_length=250*bs.us, wavelength=bs.f2lam( 442*bs.MHz), peak_gain=bs.dB2lin(48.), band='UHF')

TX.set_transmitter( FPS_85_transmitter)
RX.set_receiver(AO_UHF_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\FPS-85"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
    
import matplotlib.pyplot as plt
   
plt.close('all')