# -*- coding: utf-8 -*-
"""
Created on Mon Jun 07 20:22:56 2021

@author: jimurray
"""

import numpy as np
import pandas as pd

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.SurfFluxBeamArea as sf


TX = obs.DSS_14

p_TX = np.array([[sf.a],[0],[0]]) 

TX.set_pos_from_p(p_TX, 90., 75.)

RX = obs.DSS_15

eastward_angle_offsets = np.linspace(0, .1*np.pi/180., 500)
intersection_altitude_km = 975.
bin_width_km = 50.
intersection_range_km = TX.orbit_alt2slant(intersection_altitude_km)



AREA_KM2s_center = np.zeros(len(eastward_angle_offsets))
area_km2s_center = np.zeros(len(eastward_angle_offsets))
AREA_KM2s_left = np.zeros(len(eastward_angle_offsets))
area_km2s_left = np.zeros(len(eastward_angle_offsets))
AREA_KM2s_right = np.zeros(len(eastward_angle_offsets))
area_km2s_right = np.zeros(len(eastward_angle_offsets))
baselines = np.zeros(len(eastward_angle_offsets))
betas = np.zeros(len(eastward_angle_offsets))


for i, offset in enumerate(eastward_angle_offsets):
    psi = bs.deg2rad(TX.longitude[0]) + offset
    x = sf.a*np.cos(psi)
    y = sf.a*np.sin(psi)
    z = p_TX[2][0]
    p = np.array([[x],[y],[z]]) 
    
    RX.set_pos_from_p(p)
    
    
    bs.point_ant2ant(intersection_range_km, TX, RX)
    
#    average_p  = .5*(TX.p+RX.p)
    average_az = .5*(TX.azimuth+RX.azimuth)
    average_el = .5*(TX.elevation+RX.elevation)
    
    
    RNG1_KM, RNG2_KM, AREA_KM2_left =  sf.SURF_FLUX_BEAM_AREA(TX.latitude, average_az, average_el, \
                                                                .037, \
                                                                900 , \
                                                                950, \
                                                                0)
    
    RNG1_KM, RNG2_KM, area_km2_left = bs.BISTATIC_SURF_FLUX_BEAM_AREA(900, \
                                                                        950, \
                                                                        TX, RX, steps = 25, samples = 500/4, \
                                                                        width = .25)
    
    RNG1_KM, RNG2_KM, AREA_KM2_center =  sf.SURF_FLUX_BEAM_AREA(TX.latitude, average_az, average_el, \
                                                                .037, \
                                                                950 , \
                                                                1000 , \
                                                                0)
    
    RNG1_KM, RNG2_KM, area_km2_center = bs.BISTATIC_SURF_FLUX_BEAM_AREA(950, \
                                                                        1000, \
                                                                        TX, RX, steps = 25, samples = 500/4, \
                                                                        width = .25)
    
    RNG1_KM, RNG2_KM, AREA_KM2_right =  sf.SURF_FLUX_BEAM_AREA(TX.latitude, average_az, average_el, \
                                                                .037, \
                                                                1000 , \
                                                                1050 , \
                                                                0)
    
    RNG1_KM, RNG2_KM, area_km2_right = bs.BISTATIC_SURF_FLUX_BEAM_AREA(1000, \
                                                                        1050, \
                                                                        TX, RX, steps = 25, samples = 500/4, \
                                                                        width = .25)
               
    baseline = np.linalg.norm(TX.p - RX.p)
    
    beta = bs.rad2deg(np.arccos(np.dot(TX.b,RX.b)))

    

    AREA_KM2s_center[i] = AREA_KM2_center
    area_km2s_center[i] = area_km2_center
    AREA_KM2s_left[i] = AREA_KM2_left
    area_km2s_left[i] = area_km2_left
    AREA_KM2s_right[i] = AREA_KM2_right
    area_km2s_right[i] = area_km2_right
    baselines[i] = baseline
    betas[i] = beta
    
AREA_KM2s_all = AREA_KM2_center + AREA_KM2_left + AREA_KM2_right
area_km2s_all = area_km2_center + area_km2_left + area_km2_right


foo = {'Monostatic Area left (km^2)': AREA_KM2s_left,\
       'Bistatic Area left (km^2)': area_km2s_left,\
       'Percent Error left' : (AREA_KM2s_left-area_km2s_left)/area_km2s_left*100,\
       'Monostatic Area center (km^2)': AREA_KM2s_center,\
       'Bistatic Area center (km^2)': area_km2s_center,\
       'Percent Error center' : (AREA_KM2s_center-area_km2s_center)/area_km2s_center*100,\
       'Monostatic Area right (km^2)': AREA_KM2s_right,\
       'Bistatic Area right (km^2)': area_km2s_right,\
       'Percent Error right' : (AREA_KM2s_right-area_km2s_right)/area_km2s_right*100,\
       'Monostatic Area all (km^2)': AREA_KM2s_all,\
       'Bistatic Area all (km^2)': area_km2s_all,\
       'Percent Error all' : (AREA_KM2s_all-area_km2s_all)/area_km2s_all*100,\
       'Baseline (m)': baselines,\
       'Bistatic Angle (deg)': betas}

foo = pd.DataFrame(foo)

foo.to_excel("G:\\results\\betas_all.xlsx")