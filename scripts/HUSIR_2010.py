# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 16:34:58 2019

@author: jimurray
"""

import numpy as np
from joblib import Parallel, delayed
from glob import glob

import bistatic as bs
import observer2 as obs
from conversion import G_k2G, dB2lin, lin2dB, f2lam, lam2f,\
                                MW, kW, W,\
                                ns, us, ms, s,\
                                km, m, cm, mm,\
                                GHz, MHz, kHz, Hz,\
                                MW, kW, W,\
                                deg, arcsec, arcmin
sqkm2sqm = 1.0e6
hoursPerYear = 8760.

files = glob("./ANZ/HUSIR 2010/interp*km.DAT")
fluxes = {}

for fi in files:
    flux_key = int(fi.split('_')[-1].split('.')[0])
    fluxes[flux_key] = np.loadtxt(fi).T

R_mono = np.linspace(300,2000,400)
R_bi = np.linspace(900,1100,1000)

#HUSIR baseline
obs.HUSIR.set_pointing(90.,75.)
obs.HUSIR.receiver.BW = 1.25*MHz
obs.HUSIR.receiver.loss = obs.HUSIR.receiver.loss*dB2lin(7.8) # dB
R_int = obs.HUSIR.alt2slant(1000.)


pair = [obs.HUSIR.as_a('transmitter'),obs.HUSIR.as_a('receiver')]


fname = pair[0].name + "_" + pair[1].name + "_" + pair[0].transmitter.band + "_band.txt"
    
RCS = bs.RCSVrange([R_int], pair[0],pair[1],plot=False, atmospheric_loss = True)
size = bs.sizeVrange([R_int], pair[0],pair[1], plot=False, atmospheric_loss = True)



flux_file = "./ANZ/HUSIR 2010/FLUX_TEL.OUT"
flux_data = np.genfromtxt(flux_file, delimiter="   ", skip_header=12, usecols=(0,1,5))


R, indicies = np.unique(flux_data.T[1], return_index = True)
alts, indicies = np.unique(flux_data.T[0], return_index = True)
flux = (flux_data.T[2])[indicies]


alt_limit = np.where(np.logical_and(alts <= 1950., alts >= 400.))[0]

flux_area = bs.areaVrange_rx(R[alt_limit],pair[0],pair[1], plot = False)

flux_RCS = bs.RCSVrange(R[alt_limit],pair[0],pair[1], plot = False, atmospheric_loss = True)
flux_size_high = bs.SEM(flux_RCS, pair[0].wavelength)
flux_size_mean = bs.SEM(flux_RCS/0.6870376035296604, pair[0].wavelength)
flux_size_low  = bs.SEM(flux_RCS*4., pair[0].wavelength)

flux_total_low = []
flux_total_mean = []
flux_total_high = []
for r, SIZE_low, SIZE_high, SIZE_mean in zip(alts[alt_limit], flux_size_low, flux_size_high, flux_size_mean):
    index_low  = np.argmin(abs(fluxes[int(r)][0] - SIZE_low))
    index_mean = np.argmin(abs(fluxes[int(r)][0] - SIZE_mean))
    index_high = np.argmin(abs(fluxes[int(r)][0] - SIZE_high))
    
    flux_total_low.append(fluxes[int(r)][1][index_low])
    flux_total_mean.append(fluxes[int(r)][1][index_mean])
    flux_total_high.append(fluxes[int(r)][1][index_high])


#plt.savefig("HUSIR_area.png")

count_rate = flux_area*flux[alt_limit]*sqkm2sqm/hoursPerYear

flux_total_low = np.array(flux_total_low)
count_rate_total_low = flux_area*flux_total_low*sqkm2sqm/hoursPerYear

flux_total_high = np.array(flux_total_high)
count_rate_total_high = flux_area*flux_total_high*sqkm2sqm/hoursPerYear

flux_total_mean = np.array(flux_total_mean)
count_rate_total_mean = flux_area*flux_total_mean*sqkm2sqm/hoursPerYear

total_rate = np.sum(count_rate)
total_rate_total_low  = np.sum(count_rate_total_low)
total_rate_total_high = np.sum(count_rate_total_high)
total_rate_total_mean = np.sum(count_rate_total_mean)



#    plt.clf()
#    plt.bar(R[alt_limit],count_rate,dR,label = "Total Rate: " + str(total_rate) + " #/hr")
#    plt.title("Particle Count Rates for HUSIR Radar (> 1 cm) from 200-2000 km")
#    plt.xlabel("Altitude [km]")
#    plt.ylabel("Number of Particles per Hour")
#    plt.legend()
#    plt.grid()
#    plt.show()
#    plt.savefig("HUSIR_flux.png")

baseline_RCS  = dB2lin(-49.0143334524) # m^2
baseline_size = 5.1342086282   # mm
baseline_area = 4700.45715251  # km^2
baseline_flux = 6.22153804222  # per hour
baseline_flux_total_high = 54.54903516641725 # per hour
baseline_flux_total_low = 21.251634119733474 # per hour
baseline_flux_total_mean = 1.

with open(fname, 'w') as outfile:
    outfile.write("Transmitter: " + pair[0].name + '\n')
    outfile.write("Receiver: " + pair[1].name + '\n')
    outfile.write("Band: " + pair[0].transmitter.band + '\n')
    outfile.write("Center Frequency: " + str(lam2f(pair[0].wavelength)/MHz) + " MHz\n")
    outfile.write("TX Pulse Length: " + str(pair[0].transmitter.pulse_length/us) + " microsec\n")
    outfile.write("TX Power: " + str(pair[0].transmitter.power/kW) + " kW\n")
    outfile.write("TX Bandwidth: " + str(pair[0].transmitter.BW/kHz) + " kHz\n")
    outfile.write("TX Gain: " + str(10*np.log10(pair[0].transmitter.peak_gain)) + " dB\n")
    outfile.write("RX Bandwidth: " + str(pair[1].receiver.BW/1000000.) + " MHz\n")
    outfile.write("RX Gain: " + str(lin2dB(pair[1].receiver.peak_gain)) + " dB\n")
    outfile.write("RX Temperature: " + str(pair[1].receiver.T_sys) + " K\n")
    outfile.write("RX Minimum SNR: " + str(lin2dB(pair[1].receiver.SNR_min)) + " dB\n\n")
    outfile.write("Minimum RCS @ 1000km: " + str(lin2dB(RCS[0]))+ " dBsm\n")
    outfile.write("Minimum Size @ 1000km: " + str(size[0]/mm)+ " mm\n")
    outfile.write("Intersection Area from 300-2000km: " + str(np.sum(flux_area))+ " km^2\n")
    outfile.write("Count Rate of >1cm from 300-2000km: " + str(total_rate) + " per hour\n\n")
    outfile.write("Total Count Rate from 300-2000km:  " + str(total_rate_total_low)+ '-'+str(total_rate_total_high)+ '-'+str(total_rate_total_mean) + " per hour " + str(total_rate_total_low / baseline_flux_total_low)  + "-"+ str(total_rate_total_high / baseline_flux_total_high)   + "-"+ str(total_rate_total_mean / baseline_flux_total_mean)+ " \n \n")

    outfile.write("                                    &  Absolute                                                                 & Relative \\ \hline \n")
    outfile.write("Minimum RCS @ 1000km:               & " + str(lin2dB(RCS[0]))                                      + " dBsm     & " + str(RCS[0]/  baseline_RCS)              + " \\ \hline \n")
    outfile.write("Minimum Size @ 1000km:              & " + str(size[0]/mm)                                          + " mm       & " + str(size[0]/mm / baseline_size)         + " \\ \hline \n")
    outfile.write("Intersection Area from 300-2000km:  & " + str(np.sum(flux_area))                                        + " km$^2$   & " + str(np.sum(flux_area) / baseline_area)       + " \\ \hline \n")
    outfile.write("Count Rate of >1cm from 300-2000km: & " + str(total_rate)                                          + " per hour & " + str(total_rate / baseline_flux)         + " \\ \hline \n")
    outfile.write("Total Count Rate from 300-2000km:   & " + str(total_rate_total_low)+'-'+str(total_rate_total_high) +'-'+str(total_rate_total_mean) +" per hour & " + str(total_rate_total_low/baseline_flux_total_low)+"-"+str(total_rate_total_high/baseline_flux_total_high) +"-"+str(total_rate_total_mean/baseline_flux_total_mean)  + " \\ \hline \n")

    print pair[0].name, pair[0].transmitter.band
    print pair[1].name, pair[1].receiver.band
    print 10*np.log10(RCS), " dBsm"
    print size*100, " cm"
    print np.sum(flux_area), " km^2"

    
