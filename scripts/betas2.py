# -*- coding: utf-8 -*-
"""
Created on Tue Jun 08 10:39:37 2021

@author: jimurray
"""

import warnings
warnings.filterwarnings("ignore")

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.SurfFluxBeamArea as sf


from multiprocessing import Pool, TimeoutError
from time import time

start = time()

#%%

save_path = "C:/Users/jimurray/Python_Scripts/REPOS/Bistatic/results/area_error_v_baseline"


# Choose DSS_14 as our Transmitter
# really only keeping wavelength and dish diameter
TX = obs.DSS_14
# Reset Position to equator at zero altitude, pointed 75E
p_TX = np.array([[sf.a],[0],[0]]) 
TX.set_pos_from_p(p_TX, 90., 75.)

# Choose DSS_25 as our Receiver
# really only keeping wavelength and dish diameter
# Receiver is going to be moved longitudinally away from 
# Transmitter to increase the baseline
RX = obs.DSS_15

composite_HPBW_deg = 0.037

start_baseline = 0.
stop_baseline = 5.
baseline_step = .05
base_lines_km = np.arange(start_baseline, stop_baseline + baseline_step, baseline_step)
a_km = sf.a/1000
eastward_angle_offsets = np.arcsin(base_lines_km/2/a_km)*2
# eastward_angle_offsets = np.linspace(0, .1*np.pi/180., 500)


intersection_altitudes_km = [400., 500., 600., 700., 800., 900., 1000.]
#intersection_altitudes_km = [800., 900., 1000.]




start_alt_km = 300
stop_alt_km = 2000
bin_width_km = 50

steps = 25
samples = 125
width = .25


start_alts_km = np.arange(start_alt_km, stop_alt_km, bin_width_km)

columns  = [(str(round(i,2)) +' km') for i in base_lines_km]
indicies = [(str(i)+' km - '+ str(i+bin_width_km) + ' km') for i in start_alts_km] #+ ["Intersection Beta (deg)"]



# for ix, intersection_altitude_km in enumerate(intersection_altitudes_km):

#%%
def main(intersection_altitude_km):    
    bistatic_area  = pd.DataFrame(columns=columns, index = indicies).fillna(0.)
    surf_flux_area = pd.DataFrame(columns=columns, index = indicies).fillna(0.)
    
    print("Intersection Altitude: %i km" % intersection_altitude_km)
    
    intersection_range_km = TX.orbit_alt2slant(intersection_altitude_km)



    for i, offset in enumerate(eastward_angle_offsets):
        print("    eastward_angle_offset: %f deg" % (offset*180/np.pi))
        
        psi = offset
        x = sf.a*np.cos(psi)
        y = sf.a*np.sin(psi)
        z = p_TX[2][0]
        p = np.array([[x],[y],[z]]) 
        
        RX.set_pos_from_p(p)
        
        
        bs.point_ant2ant(intersection_range_km, TX, RX)
        
    #    average_p  = .5*(TX.p+RX.p)
        average_az = .5*(TX.azimuth+RX.azimuth)
        average_el = .5*(TX.elevation+RX.elevation)
        
        
        # baseline = np.linalg.norm(TX.p - RX.p)
        # beta = bs.rad2deg(np.arccos(np.dot(TX.b,RX.b)))
        
        # bistatic_area.at["Intersection Beta (deg)", columns[i]]  = beta
        # surf_flux_area.at["Intersection Beta (deg)", columns[i]]  = beta
        
        for idx in range(len(start_alts_km)):
            left = start_alts_km[idx]
            right = left + bin_width_km
            RNG1_KM, RNG2_KM, sf_area =  sf.SURF_FLUX_BEAM_AREA(TX.latitude, average_az, average_el, composite_HPBW_deg, left, right, 0)
            
            surf_flux_area.at[indicies[idx] , columns[i]] = sf_area 

        
        
        # Find the middle
        idx = np.where(start_alts_km == intersection_altitude_km)[0][0]
        
        bs_area = 1.
        y = 0
        while bs_area > 0.:# left side
            left  = start_alts_km[idx - 1 - y ]
            right = start_alts_km[idx - y]
            
            RNG1_KM, RNG2_KM, bs_area = bs.BISTATIC_SURF_FLUX_BEAM_AREA(left, right, TX, RX, steps = steps, samples = samples, width = width)
            
            bistatic_area.at[indicies[idx - 1 - y ] , columns[i]]  = bs_area
            
            print("        %s Left Side: Bistatic Area = %f km^2" % (indicies[idx - 1 - y ], bs_area))
            
            # If we reach the start of the array
            if idx - 1 - y == 0:
                break
            y += 1
        
        bs_area = 1.
        y = 0  
        while bs_area > 0.:# right side
            left  = start_alts_km[idx + y ]
            right = left + bin_width_km
            
            RNG1_KM, RNG2_KM, bs_area = bs.BISTATIC_SURF_FLUX_BEAM_AREA(left, right, TX, RX, steps = steps, samples = samples, width = width)
            
            bistatic_area.at[indicies[idx + y ] , columns[i]]  = bs_area
            
            print("        %s Right Side: Bistatic Area = %f km^2" % ( indicies[idx + y ], bs_area))
            
            # If we reach the end of the array
            if idx + y + 1 == len(start_alts_km):
                break
            y += 1        
    
    temp = bistatic_area.T
    temp['Total Area (km^2)'] = np.sum(bistatic_area)
    bistatic_area = temp
    bistatic_area.to_excel(os.path.join(save_path, 'bs_area_%i.xlsx'  % int(intersection_altitude_km)))
    
    temp = surf_flux_area.T
    temp['Total Area (km^2)'] = np.sum(surf_flux_area)
    surf_flux_area = temp
    surf_flux_area.to_excel(os.path.join(save_path, 'sf_area_%i.xlsx' % int(intersection_altitude_km)))

    diff = (surf_flux_area - bistatic_area)/bistatic_area *100
    diff.to_excel(os.path.join(save_path, 'diff_area_%i.xlsx' % int(intersection_altitude_km)))
    return diff


if __name__ == '__main__':
    pool = Pool() 
    
    
    # diffs = [main(i) for i in intersection_altitudes_km]
 
    multiple_results = [pool.apply_async(main, (i,)) for i in intersection_altitudes_km]
    diffs = [res.get(timeout=86400) for res in multiple_results]
    
    stop = time()
    
    print("Process took %f seconds" % (stop - start))
    
    
    #%%
    fig, ax = plt.subplots(figsize = (14.222, 8))
    
    for diff, intersection_altitude_km in zip(diffs,intersection_altitudes_km):
        
        plt.plot(base_lines_km, diff['Total Area (km^2)'], label = str(int(intersection_altitude_km))+ ' km')
        
    plt.xlabel("Baseline (km)", fontsize = 18, fontweight = 'bold')
    plt.ylabel("Percent Error", fontsize = 18, fontweight = 'bold')
    
    plt.ylim(0,1000)
    plt.xticks( fontsize = 18, fontweight = 'bold')
    plt.yticks( fontsize = 18, fontweight = 'bold')
    plt.grid()
    plt.legend(fontsize = 16)
    plt.savefig(os.path.join(save_path, 'fig.png'))
