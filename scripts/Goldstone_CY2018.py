# -*- coding: utf-8 -*-
"""
Created on Sat May 29 16:16:36 2021

@author: jimurray
"""

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc


TX = obs.DSS_14
RX = obs.DSS_25
RX.receiver.SNR_min = 12.
RX.receiver.BW = TX.transmitter.BW

#actual power in CY2018
TX.transmitter.power = 100000.


intersection_altitude_km = TX.slant2orbit_alt(800.) # km
TX_azimuth   = 90. # deg
TX_elevation = 75. # deg

bs.point_ant2ant(800., TX, RX)



flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Goldstone CY2018\\FLUX_TEL.OUT"

#output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Goldstone CY2018"

output_directory = "G:\\results\\Goldstone CY2018"

fc.standard_flux_calculations(intersection_altitude_km, TX, RX, flux_file, output_directory, area_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Goldstone CY2018\\flux_table.xlsx")
