# -*- coding: utf-8 -*-
"""
Created on Sat May 29 22:19:01 2021

@author: jimurray
"""

from time import time
import os

from joblib import Parallel, delayed

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc


intersection_altitude_km = 1000. # km
R_int = 1000.
# R_int = 997.5


flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_base_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo"


def main(i,pair):
#    if i > 2:
    output_directory = os.path.join(output_base_directory, str(i)+"_"+pair[0].name + "_" +pair[1].name)
    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)
    
    if os.path.isfile(os.path.join(output_directory, "flux_table.xlsx")):
        area_file = os.path.join(output_directory, "flux_table.xlsx")
    else:
        area_file = None
    
    fc.standard_flux_calculations(intersection_altitude_km, pair[0], pair[1], flux_file, output_directory, area_file = area_file)


if __name__ == "__main__":
    
    start_time = time()
    
    pairs = []
    
    #Arecibo UHF CW
    obs.AO.set_transmitter(obs.tx.AO_UHF)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    
    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #Arecibo UHF World Day
    obs.AO.set_transmitter(obs.tx.AO_UHF_WD)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    
    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #AO-VLBI S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
    obs.VLBI.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBI.set_receiver(obs.rx.VLBI_S)
    obs.VLBI.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBI.as_a('receiver')])
    
    #AO-VLBA S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBA)
    obs.VLBA.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBA.set_receiver(obs.rx.VLBA_S)
    obs.VLBA.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBA.as_a('receiver')])
    
    #AO-GBT S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.GBT)
    obs.GBT.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.GBT.set_receiver(obs.rx.GBT_S)
    obs.GBT.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.GBT.as_a('receiver')])
    
    #HUSIR-AO X-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.HUSIR)
    obs.HUSIR.set_pointing(az,el)
    
    obs.HUSIR.set_transmitter(obs.tx.HUSIR)
    obs.AO.set_receiver(obs.rx.AO_X)
    obs.AO.receiver.BW = obs.HUSIR.transmitter.BW
    
    pairs.append([obs.HUSIR.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #Millstone-AO L-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.Millstone)
    obs.Millstone.set_pointing(az,el)
    
    obs.Millstone.set_transmitter(obs.tx.Millstone)
    obs.AO.set_receiver(obs.rx.AO_ALFA)
    obs.AO.receiver.BW = obs.Millstone.transmitter.BW
    
    pairs.append([obs.Millstone.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #VLBI-AO L-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
    obs.VLBI.set_pointing(az,el)
    
    obs.VLBI.set_transmitter(obs.tx.COTS_L)
    obs.AO.set_receiver(obs.rx.AO_ALFA)
    obs.AO.receiver.BW = obs.VLBI.transmitter.BW
    
    pairs.append([obs.VLBI.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #FPS-85-AO 
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_85)
    obs.FPS_85.set_pointing(az,el)
    
    obs.FPS_85.set_transmitter(obs.tx.FPS_85)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    obs.AO.receiver.BW = obs.FPS_85.transmitter.BW
    
    pairs.append([obs.FPS_85.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #FPS-16-AO 
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_16)
    obs.FPS_16.set_pointing(az,el)
    
    obs.FPS_16.set_transmitter(obs.tx.FPS_16)
    obs.AO.set_receiver(obs.rx.AO_C)
    obs.AO.receiver.BW = obs.FPS_16.transmitter.BW
    
    pairs.append([obs.FPS_16.as_a('transmitter'),obs.AO.as_a('receiver')])

    
    
    
    Parallel(n_jobs=-1)(delayed(main)(i, pair) for i, pair in enumerate(pairs))


    
    end_time = time()
    print("Execution took: ", end_time - start_time, " seconds...")
