

import numpy as np
from joblib import Parallel, delayed
from glob import glob
from time import time

import bistatic as bs
import observer2 as obs
from conversion import G_k2G, dB2lin, lin2dB, f2lam, lam2f,\
                                MW, kW, W,\
                                ns, us, ms, s,\
                                km, m, cm, mm,\
                                GHz, MHz, kHz, Hz,\
                                MW, kW, W,\
                                deg, arcsec, arcmin


files = glob("./ANZ/Arecibo/interp*km.DAT")
fluxes = {}

for fi in files:
    flux_key = int(fi.split('_')[-1].split('.')[0])
    fluxes[flux_key] = np.loadtxt(fi).T

def run_pairs(i,pair):
    
    R_mono = np.linspace(300,2000,400)
    R_bi = np.linspace(900,1100,1000)
    R_int = 1000.
    
    if i < 4:
        R0 = R_mono
    else:
        R0 = R_bi
    
    fname = str(i) + "_" + pair[0].name + "_" + pair[1].name + "_" + pair[0].transmitter.band + "_band.txt"
        
    #outfile = open(fname, 'w')

    RCS = bs.RCSVrange([R_int], pair[0],pair[1],plot=False, atmospheric_loss = True)
    size = bs.sizeVrange([R_int], pair[0],pair[1], plot=False, atmospheric_loss = True)
    
#    if pair[0].name == 'Arecibo':
#        area = bs.areaVrange_tx(R0,pair[0],pair[1], plot = False)
#    else:
#        area = bs.areaVrange_rx(R0,pair[0],pair[1], plot = False)
    
    sqkm2sqm = 1.0e6
    hoursPerYear = 8760.

    if i == 0:
        flux_file = "FLUX_TEL_HUSIR.OUT"
        flux_data = np.genfromtxt(flux_file, delimiter="   ", skip_header=5, usecols=(0,5))
    else:
        flux_file = "Arecibo.OUT"
        flux_data = np.genfromtxt(flux_file, delimiter="   ", skip_header=5, usecols=(0,5))
    
    R, indicies = np.unique(flux_data.T[0], return_index = True)
    flux = (flux_data.T[1])[indicies]
    
    #dR = R[1]-R[0]
    
    alt_limit = np.where(np.logical_and(R <= 2000., R >= 300.))[0]
    
    if pair[0].name == 'Arecibo':
        flux_area = bs.areaVrange_tx(R[alt_limit],pair[0],pair[1], plot = False)
    else:
        flux_area = bs.areaVrange_rx(R[alt_limit],pair[0],pair[1], plot = False)
    
    flux_RCS = bs.RCSVrange(R[alt_limit],pair[0],pair[1], plot = False, atmospheric_loss = True)
    flux_size_high = bs.SEM(flux_RCS, pair[0].wavelength)
    flux_size_mean = bs.SEM(flux_RCS/0.6870376035296604, pair[0].wavelength)
    flux_size_low  = bs.SEM(flux_RCS*4., pair[0].wavelength)
    
    flux_total_low = []
    flux_total_mean = []
    flux_total_high = []
    for r, SIZE_low, SIZE_high, SIZE_mean in zip(R[alt_limit], flux_size_low, flux_size_high, flux_size_mean):
        index_low  = np.argmin(abs(fluxes[int(r)][0] - SIZE_low))
        index_mean = np.argmin(abs(fluxes[int(r)][0] - SIZE_mean))
        index_high = np.argmin(abs(fluxes[int(r)][0] - SIZE_high))
        
        flux_total_low.append(fluxes[int(r)][1][index_low])
        flux_total_mean.append(fluxes[int(r)][1][index_mean])
        flux_total_high.append(fluxes[int(r)][1][index_high])
    
    
    #plt.savefig("HUSIR_area.png")
    
    count_rate = flux_area*flux[alt_limit]*sqkm2sqm/hoursPerYear
    
    flux_total_low = np.array(flux_total_low)
    count_rate_total_low = flux_area*flux_total_low*sqkm2sqm/hoursPerYear
    
    flux_total_high = np.array(flux_total_high)
    count_rate_total_high = flux_area*flux_total_high*sqkm2sqm/hoursPerYear
    
    flux_total_mean = np.array(flux_total_mean)
    count_rate_total_mean = flux_area*flux_total_mean*sqkm2sqm/hoursPerYear
    
    total_rate = np.sum(count_rate)
    total_rate_total_low  = np.sum(count_rate_total_low)
    total_rate_total_high = np.sum(count_rate_total_high)
    total_rate_total_mean = np.sum(count_rate_total_mean)

    
    
#    plt.clf()
#    plt.bar(R[alt_limit],count_rate,dR,label = "Total Rate: " + str(total_rate) + " #/hr")
#    plt.title("Particle Count Rates for HUSIR Radar (> 1 cm) from 200-2000 km")
#    plt.xlabel("Altitude [km]")
#    plt.ylabel("Number of Particles per Hour")
#    plt.legend()
#    plt.grid()
#    plt.show()
#    plt.savefig("HUSIR_flux.png")
    
    baseline_RCS  = dB2lin(-49.0143334524) # m^2
    baseline_size = 5.1342086282   # mm
    baseline_area = 4700.45715251  # km^2
    baseline_flux = 6.22153804222  # per hour
    baseline_flux_total_high = 54.54903516641725 # per hour
    baseline_flux_total_low = 21.251634119733474 # per hour
    baseline_flux_total_mean = 1.
    
    with open(fname, 'w') as outfile:
        outfile.write("Transmitter: " + pair[0].name + '\n')
        outfile.write("Receiver: " + pair[1].name + '\n')
        outfile.write("Band: " + pair[0].transmitter.band + '\n')
        outfile.write("Center Frequency: " + str(lam2f(pair[0].wavelength)/MHz) + " MHz\n")
        outfile.write("TX Pulse Length: " + str(pair[0].transmitter.pulse_length/us) + " microsec\n")
        outfile.write("TX Power: " + str(pair[0].transmitter.power/kW) + " kW\n")
        outfile.write("TX Bandwidth: " + str(pair[0].transmitter.BW/kHz) + " kHz\n")
        outfile.write("TX Gain: " + str(10*np.log10(pair[0].transmitter.peak_gain)) + " dB\n")
        outfile.write("RX Bandwidth: " + str(pair[1].receiver.BW/1000000.) + " MHz\n")
        outfile.write("RX Gain: " + str(lin2dB(pair[1].receiver.peak_gain)) + " dB\n")
        outfile.write("RX Temperature: " + str(pair[1].receiver.T_sys) + " K\n")
        outfile.write("RX Minimum SNR: " + str(lin2dB(pair[1].receiver.SNR_min)) + " dB\n\n")
        outfile.write("Minimum RCS @ 1000km: " + str(lin2dB(RCS[0]))+ " dBsm\n")
        outfile.write("Minimum Size @ 1000km: " + str(size[0]/mm)+ " mm\n")
        outfile.write("Intersection Area from 300-2000km: " + str(np.sum(flux_area))+ " km^2\n")
        outfile.write("Count Rate of >1cm from 300-2000km: " + str(total_rate) + " per hour\n\n")
        outfile.write("Total Count Rate from 300-2000km:  " + str(total_rate_total_low)+ '-'+str(total_rate_total_high)+ '-'+str(total_rate_total_mean) + " per hour " + str(total_rate_total_low / baseline_flux_total_low)  + "-"+ str(total_rate_total_high / baseline_flux_total_high)   + "-"+ str(total_rate_total_mean / baseline_flux_total_mean)+ " \n \n")

        outfile.write("                                    &  Absolute                                                                 & Relative \\ \hline \n")
        outfile.write("Minimum RCS @ 1000km:               & " + str(lin2dB(RCS[0]))                                      + " dBsm     & " + str(RCS[0]/  baseline_RCS)              + " \\ \hline \n")
        outfile.write("Minimum Size @ 1000km:              & " + str(size[0]/mm)                                          + " mm       & " + str(size[0]/mm / baseline_size)         + " \\ \hline \n")
        outfile.write("Intersection Area from 300-2000km:  & " + str(np.sum(flux_area))                                        + " km$^2$   & " + str(np.sum(flux_area) / baseline_area)       + " \\ \hline \n")
        outfile.write("Count Rate of >1cm from 300-2000km: & " + str(total_rate)                                          + " per hour & " + str(total_rate / baseline_flux)         + " \\ \hline \n")
        outfile.write("Total Count Rate from 300-2000km:   & " + str(total_rate_total_low)+'-'+str(total_rate_total_high) +'-'+str(total_rate_total_mean) +" per hour & " + str(total_rate_total_low/baseline_flux_total_low)+"-"+str(total_rate_total_high/baseline_flux_total_high) +"-"+str(total_rate_total_mean/baseline_flux_total_mean)  + " \\ \hline \n")

        #outfile.write("\n\n\n")
#        allfile.write("Transmitter: " + pair[0].name + '\n')
#        allfile.write("Receiver: " + pair[1].name + '\n')
#        allfile.write("Band: " + pair[0].transmitter.band + '\n')
#        allfile.write("Minimum RCS @ 1000km: " + str(10*np.log10(RCS))+ " dBsm" + '\n')
#        allfile.write("Minimum Size @ 1000km: " + str(size*1000)+ " mm" + '\n')
#        allfile.write("Intersection Area from 300-2000km: " + str(np.sum(area))+ " km^2" + '\n')
#        allfile.write("\n\n\n")
        print pair[0].name, pair[0].transmitter.band
        print pair[1].name, pair[1].receiver.band
        print 10*np.log10(RCS), " dBsm"
        print size*100, " cm"
        print np.sum(flux_area), " km^2"
        #i=i+1
        #outfile.close()
    

if __name__ == "__main__":
    
    start_time = time()
    
    #R = np.arange(300,2010,10)
    R_mono = np.linspace(300,2000,400)
    R_bi = np.linspace(900,1100,1000)
    
    
    R_int = 1000.
    
    pairs = []
    
    
    #HUSIR baseline
    obs.HUSIR.set_pointing(90.,75.)
    pairs.append([obs.HUSIR.as_a('transmitter'),obs.HUSIR.as_a('receiver')])
    
    #Arecibo UHF CW
    obs.AO.set_transmitter(obs.tx.AO_UHF)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    
    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #Arecibo UHF World Day
    obs.AO.set_transmitter(obs.tx.AO_UHF_WD)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    
    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #AO-VLBI S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
    obs.VLBI.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBI.set_receiver(obs.rx.VLBI_S)
    obs.VLBI.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBI.as_a('receiver')])
    
    #AO-VLBA S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBA)
    obs.VLBA.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBA.set_receiver(obs.rx.VLBA_S)
    obs.VLBA.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBA.as_a('receiver')])
    
    #AO-GBT S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.GBT)
    obs.GBT.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.GBT.set_receiver(obs.rx.GBT_S)
    obs.GBT.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.GBT.as_a('receiver')])
    
    #HUSIR-AO X-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.HUSIR)
    obs.HUSIR.set_pointing(az,el)
    
    obs.HUSIR.set_transmitter(obs.tx.HUSIR)
    obs.AO.set_receiver(obs.rx.AO_X)
    obs.AO.receiver.BW = obs.HUSIR.transmitter.BW
    
    pairs.append([obs.HUSIR.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #Millstone-AO L-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.Millstone)
    obs.Millstone.set_pointing(az,el)
    
    obs.Millstone.set_transmitter(obs.tx.Millstone)
    obs.AO.set_receiver(obs.rx.AO_ALFA)
    obs.AO.receiver.BW = obs.Millstone.transmitter.BW
    
    pairs.append([obs.Millstone.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #VLBI-AO L-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
    obs.VLBI.set_pointing(az,el)
    
    obs.VLBI.set_transmitter(obs.tx.COTS_L)
    obs.AO.set_receiver(obs.rx.AO_ALFA)
    obs.AO.receiver.BW = obs.VLBI.transmitter.BW
    
    pairs.append([obs.VLBI.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #FPS-85-AO 
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_85)
    obs.FPS_85.set_pointing(az,el)
    
    obs.FPS_85.set_transmitter(obs.tx.FPS_85)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    obs.AO.receiver.BW = obs.FPS_85.transmitter.BW
    
    pairs.append([obs.FPS_85.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #FPS-16-AO 
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_16)
    obs.FPS_16.set_pointing(az,el)
    
    obs.FPS_16.set_transmitter(obs.tx.FPS_16)
    obs.AO.set_receiver(obs.rx.AO_C)
    obs.AO.receiver.BW = obs.FPS_16.transmitter.BW
    
    pairs.append([obs.FPS_16.as_a('transmitter'),obs.AO.as_a('receiver')])


    Parallel(n_jobs=-1)(delayed(run_pairs)(i, pair) for i, pair in enumerate(pairs))
#    for i, pair in enumerate(pairs):
#        run_pairs(i,pair)
    
    end_time = time()
    print "Execution took: ", end_time - start_time, " seconds..."
