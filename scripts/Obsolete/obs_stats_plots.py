# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 11:07:50 2018

@author: jimurray
"""

import numpy as np
import matplotlib.pyplot as plt
from joblib import Parallel, delayed

import bistatic as bs
import observer2 as obs
from conversion import G_k2G, dB2lin, lin2dB, f2lam, lam2f,\
                                MW, kW, W,\
                                ns, us, ms, s,\
                                km, m, cm, mm,\
                                GHz, MHz, kHz, Hz,\
                                MW, kW, W,\
                                deg, arcsec, arcmin

def run_pairs(i, pair, R0):
    
    #R0 = np.arange(300, 2020, 20, dtype = 'float')
    
    RCS = []
    size = []
    for R in R0:
        slant_range, az, el = bs.R_az_el(R, pair[0], pair[1])
        pair[1].set_pointing(az,el)
    
        size.append(bs.sizeVrange(np.array([R]), pair[0],pair[1], plot=False))
        RCS.append(np.array(bs.RCSVrange(np.array([R]), pair[0],pair[1], plot=False)))
        if i == 4 and R < 820.:
            size[-1] = np.nan
            RCS[-1] = np.nan
    
    return size, RCS


if __name__ == "__main__":

    R_mono = np.arange(300, 2020, 20, dtype = 'float')
    R_int = 1000.
    
    pairs = []
    
    #Arecibo UHF CW
    obs.AO.set_transmitter(obs.tx.AO_UHF)
    obs.AO.set_receiver(obs.rx.AO_UHF)
    
    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
    
    #HUSIR baseline
    pairs.append([obs.HUSIR.as_a('transmitter'),obs.HUSIR.as_a('receiver')])
#    
#
#    
##    #Arecibo UHF World Day
##    obs.AO.set_transmitter(obs.tx.AO_UHF_WD)
##    obs.AO.set_receiver(obs.rx.AO_UHF)
##    
##    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
##    
    #AO-VLBI S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
    obs.VLBI.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBI.set_receiver(obs.rx.VLBI_S)
    obs.VLBI.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBI.as_a('receiver')])
    
    #AO-VLBA S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBA)
    obs.VLBA.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.VLBA.set_receiver(obs.rx.VLBA_S)
    obs.VLBA.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.VLBA.as_a('receiver')])
    
    #AO-GBT S-band
    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.GBT)
    obs.GBT.set_pointing(az,el)
    
    obs.AO.set_transmitter(obs.tx.AO_S)
    obs.GBT.set_receiver(obs.rx.GBT_S)
    obs.GBT.receiver.BW = obs.AO.transmitter.BW
    
    pairs.append([obs.AO.as_a('transmitter'),obs.GBT.as_a('receiver')])
    
    
    for i, pair in enumerate(pairs):
        sizes, RCS = run_pairs(i, pair, R_mono)
        figure(1)
        plt.plot(R_mono, 1000*np.array(sizes), linewidth = 2, label = pair[0].name + '/' + pair[1].name + ": " + pair[0].transmitter.band + "-band")
        figure(2)
        plt.plot(R_mono, 10*np.log10(np.array(RCS)), linewidth = 2, label = pair[0].name + '/' + pair[1].name + ": " + pair[0].transmitter.band + "-band")
    
    figure(1)
    fig = gcf()
    fig.set_size_inches(11,8.5)
    xlabel("Altitude [km]", fontsize = 20)
    ylabel("Minimum Detectable Size [mm]", fontsize = 20)
    xticks(fontsize = 20)
    yticks(fontsize = 20)
    grid()
    minorticks_on()
    legend(fontsize = 14)
    
    figure(2)
    fig = gcf()
    fig.set_size_inches(11,8.5)
    xlabel("Altitude [km]", fontsize = 20)
    ylabel("Minimum Detectable RCS [dBsm]", fontsize = 20)
    xticks(fontsize = 20)
    yticks(fontsize = 20)
    grid()
    minorticks_on()
    legend(fontsize = 14)
    #Parallel(n_jobs=-1)(delayed(run_pairs)(i, pair) for i, pair in enumerate(pairs))