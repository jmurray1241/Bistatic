# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 14:41:43 2021

@author: jimurray
"""
import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc
from bistatic.transmitter import Transmitter
from bistatic.receiver import Receiver


TX = deepcopy(obs.FPS_16)
RX = deepcopy(obs.AO)


# intersection_altitude_km = 1000.  # km
intersection_altitude_km = 997.5  # km
RX_azimuth   = 0.  # deg
RX_elevation = 90.  # deg


RX.set_pointing(RX_azimuth, RX_elevation)

bs.point_ant2ant(RX.orbit_alt2slant(intersection_altitude_km), RX, TX)

AO_C_receiver      = Receiver(T_sys=34.5, BW=2.15*bs.GHz, peak_gain=bs.G_k2G( 6.,bs.f2lam(5480*bs.MHz)), HPBW=1.0*bs.arcmin, band='C')
FPS_16_transmitter = Transmitter(power= 3*bs.MW, pulse_length=  1*bs.us, wavelength=bs.f2lam(5480*bs.MHz), peak_gain=bs.dB2lin(52.), band = 'C')

TX.set_transmitter( FPS_16_transmitter)
RX.set_receiver(AO_C_receiver)

RX.receiver.BW = TX.transmitter.BW
RX.receiver.SNR_min = 10.
RX.receiver.loss = 9.

flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\Arecibo CY2021\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\Arecibo\\FPS-16"

if os.path.isfile(os.path.join(output_directory,"flux_table.xlsx")):    
    area_file = os.path.join(output_directory,"flux_table.xlsx")
else:
    area_file = None


sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              area_file = area_file,\
                                              verbose = True)
import matplotlib.pyplot as plt
   
plt.close('all')