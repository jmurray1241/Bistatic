# -*- coding: utf-8 -*-
"""
Created on Sat May 29 16:16:36 2021

@author: jimurray
"""

import os
from copy import deepcopy

import bistatic.bistatic as bs
import bistatic.observer2 as obs
import bistatic.flux_calculations as fc

# if 'pfm' not in locals():
#     cwd = os.getcwd()
#     os.chdir("Z:\RADAR\PYTHON\Data Processing")
#     import plots_for_matney_2 as pfm
#     os.chdir(cwd)


TX = deepcopy(obs.HUSIR)
RX = deepcopy(obs.HUSIR)
# RX.receiver.SNR_min = 10**(5.65/10)
RX.receiver.SNR_min = 10**(11.670599913279624/10)
RX.receiver.BW = TX.transmitter.BW
RX.receiver.loss = 10**((3.9 + 6.5 - .4-1)/10)

intersection_altitude_km = 1000.  # km
TX_azimuth   = 180.  # deg
TX_elevation = 10.  # deg

RX_azimuth   = 180.  # deg
RX_elevation = 10.  # deg

TX.set_pointing(TX_azimuth, TX_elevation)
RX.set_pointing(RX_azimuth, RX_elevation)


flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\HUSIR CY2018\\10S\\FLUX_TEL.OUT"

output_directory = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\HUSIR CY2018\\10S"
#output_directory = "G:\\results\\HUSIR CY2018"

sizes, counts = fc.standard_flux_calculations(intersection_altitude_km, TX, RX, \
                                              flux_file, output_directory, gain_pattern = bs.airy, \
                                              verbose = True)
                                              area_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\HUSIR CY2018\\10S\\flux_table.xlsx")
                                              #verbose = True)

#%%
# alt_limit = {'key': ' Orbit_Altitude (km)', 'lower': 400, 'upper':2000}
# fig, ax = pfm.cumulative_count_rate(pfm.CY2018_75E, ' Size_Estimate (m)', 313.2052098, limits = alt_limit)
# ax.plot(sizes, counts, 'k--')

#losses =  linspace(0,20, 21)
#for loss in losses:
#    RX.receiver.loss = 10**((3.9 + loss)/10)
#    
#    RCS, size = fc.standard_altitude_comparison(TX.orbit_alt2slant(intersection_altitude_km), TX, RX)
#    
#    print loss, size*1000