# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 18:17:40 2020

@author: Melissa Ward
"""

import sympy as sp
from sympy.solvers.inequalities import solve_poly_inequality

sp.init_printing()

N = sp.symbols('N', integer=True)
theta, mu_a, mu_c, R_0, R = sp.symbols('theta, mu_a, mu_c, R_0, R', real = True)

A = theta*sp.pi/2*(R**2 - R_0**2)
A_prime = theta*sp.pi*mu_c*R_0*(R-R_0) + theta*sp.pi*mu_c*(R-R_0)**2*((N-1)/(2*N))


foo = sp.solve(A_prime - mu_a*A, N)

print(foo)