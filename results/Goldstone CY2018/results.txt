Transmitter: DSS-14
Receiver: DSS-25
Band: X
Center Frequency: 8560.60702456 MHz
TX Pulse Length: 2340.0 microsec
TX Power: 100.0 kW
TX Bandwidth: 0.42735042735 kHz
TX Gain: 74.55 dB
RX Bandwidth: 0.00042735042735 MHz
RX Gain: 68.5 dB
RX Temperature: 21.79 K
RX Minimum SNR: 10.7918124605 dB

Minimum RCS @ 769.593801589km: -75.5537600644 dBsm
Minimum Size @ 769.593801589km: 1.98301551978 mm
Total Count Rate : 11.6725474304 per hour