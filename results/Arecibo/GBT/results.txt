Transmitter: Arecibo
Receiver: Green Bank
Band: S
Center Frequency: 2380.0 MHz
TX Pulse Length: 3000.0 microsec
TX Power: 1000.0 kW
TX Bandwidth: 0.3333333333333333 kHz
TX Gain: 73.7 dB
RX Bandwidth: 0.0003333333333333333 MHz
RX Gain: 66.4081877458251 dB
RX Temperature: 18.0 K
RX Minimum SNR: 10.0 dB

Minimum RCS @ 1000.0km: -79.49573228690784 dBsm
Minimum Size @ 1000.0km: 4.001704161133333 mm
Total Count Rate : 0.0380758916722321 per hour