Transmitter: Arecibo
Receiver: Arecibo
Band: C
Center Frequency: 5000.0 MHz
TX Pulse Length: 1638.4000000000003 microsec
TX Power: 5000.0 kW
TX Bandwidth: 0.3333333333333333 kHz
TX Gain: 82.39847378580848 dB
RX Bandwidth: 0.0003333333333333333 MHz
RX Gain: 82.39847378580848 dB
RX Temperature: 186.0 K
RX Minimum SNR: 10.0 dB

Minimum RCS @ 1000.0km: -100.77033791947882 dBsm
Minimum Size @ 1000.0km: 1.0782997887819465 mm
Total Count Rate : 3973.173551535768 per hour