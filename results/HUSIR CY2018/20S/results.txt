Transmitter: HUSIR
Receiver: HUSIR
Band: X
Center Frequency: 9993.081933333335 MHz
TX Pulse Length: 1638.4000000000003 microsec
TX Power: 250.0 kW
TX Bandwidth: 0.6103515625 kHz
TX Gain: 67.23 dB
RX Bandwidth: 0.0006103515625 MHz
RX Gain: 67.23 dB
RX Temperature: 186.0 K
RX Minimum SNR: 11.670599913279624 dB

Minimum RCS @ 1000.0km: -36.01738637555569 dBsm
Minimum Size @ 1000.0km: 14.33069054202829 mm
Total Count Rate : 2.4236937324135135 per hour