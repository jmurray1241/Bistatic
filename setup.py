from setuptools import setup
import os
import sys

cwd = os.getcwd()

if sys.version_info.major >=3:
	from pathlib import Path
	cwd = Path(cwd)

#with open("README", 'r') as f:
#    long_description = f.read()

with open(os.path.join(cwd, 'config.py'), 'w') as config:
	config.write('path = \'%s\'' % cwd)

setup(
   name='bistatic',
   version='1.0',
   description='A useful module',
   license="MIT",
#   long_description=long_description,
   author='James Murray',
   author_email='jmurray1241@gmail.com',
   packages=['bistatic'],  #same as name
   install_requires=['nvector', 'itur'], #external packages as dependencies
#   scripts=[
#            'scripts/cool',
#            'scripts/skype',
#           ]
)