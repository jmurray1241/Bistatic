# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 16:24:45 2018

@author: jimurray
"""
import subprocess
import os
import numpy as np
import time

anz_input = 'BLAH.TXT'
ordem_file = "./HUSIR 2010/FLUX_TEL.out"

x = np.array([1.01E+02,   2.44E-01,   1.13E-03,   2.16E-08,   7.14E-09 ,  4.74E-09])



def write_for_anz(arr):
    try:
        os.remove(anz_input)
    except OSError:
        pass 
    anz = open(anz_input,'w')
    for value in arr:
        anz.write(str(value) + '\n')
    anz.close()
    
def interp(altitude, arr):
    write_for_anz(arr)
    subprocess.call(['main.exe'])
    time.sleep(1)
    try:
        file_name = 'interp_'+ str(altitude) + 'km.DAT'
        os.rename('interp.DAT', file_name)
    except Exception as e:
#        raise e
#        print str(e)
#        print "File " + file_name + "exists"
        pass
    
def original(altitude, arr):
    write_for_anz(arr)
    subprocess.call(['main.exe'])
    try:
        file_name = 'original_'+ str(altitude) + 'km.DAT'
        os.rename('original.DAT', file_name)
    except Exception as e:
        print str(e)
        #pass
        #print "File " + file_name + "exists"

def generate_interp_files(ordem_file):    

    try:
        flux = np.loadtxt(ordem_file,skiprows=5)
    except:
        flux = np.loadtxt(ordem_file,skiprows=12)

        
    for altitude_bin in flux:
        altitude = altitude_bin[0]
        fluxes = altitude_bin[2:]
        interp(altitude,fluxes)
        #original(altitude,fluxes)
        
if __name__ == '__main__':
    generate_interp_files(ordem_file)
