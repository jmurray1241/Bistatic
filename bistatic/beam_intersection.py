
#from numpy import pi,tan,cos,sin,arctan2,sqrt,floor,deg2rad, arccos, linspace, abs
from numpy import *
import numpy.random as rand

#R_e=6371000.
R_e = 6378136.3 # ITRF 93
f = 298.257
#Arecibo coords 18 deg 21'13.7"N 66 deg 45'18.8"W
AO={'lat':18.353805555555557,'long':-66.75522222222222}
#GBT coords 38 deg 25' 59.236"N 79 deg 50'23.406"W
GBT={'lat':38.433121111111106,'long':-79.839835}


class Pointing():

    def __init__(self, azimuth, elevation):
        self.azimuth = azimuth
        self.elevation = elevation
    
    def print_(self):
        return self.azimuth, self.elevation

class Observer():
    
    def __init__(self, latitude, longitude, HPBW, azimuth = 0, elevation = 90., altitude = 0, name = ''):
        self.latitude  = latitude
        self.longitude = longitude
        self.altitude  = altitude
        self.HPBW      = HPBW
        self.azimuth   = azimuth
        self.elevation = elevation
        self.name      = name
    
        
    def nice_coordinates(self):
        lat_direction='N'
        long_direction='E'
    
        if self.latitude < 0:
            lat_direction='S'
        if self.longitude < 0:
            long_direction='W'

        lat_degree=floor(abs(self.latitude))
        lat_minute=floor((abs(self.latitude)-lat_degree)*60)
        lat_sec=(abs(self.latitude)-lat_degree-lat_minute/60)*3600

        long_degree=floor(abs(self.longitude))
        long_minute=floor((abs(self.longitude)-long_degree)*60)
        long_sec=(abs(self.longitude)-long_degree-long_minute/60)*3600

        latitude=str(int(lat_degree))+"d "+str(int(lat_minute))+"m "+"{0:.2f}".format(lat_sec)+'s '+lat_direction
        longitude=str(int(long_degree))+"d "+str(int(long_minute))+"m "+"{0:.2f}".format(long_sec)+'s '+long_direction
    
        return latitude + ', ' +longitude
    

Arecibo    = Observer(18.353805555555557,-66.75522222222222, 2./60, name = 'Arecibo')
Green_Bank = Observer(38.433121111111106,-79.839835, 12*2.38/60, name = 'Green Bank')
VLBA       = Observer(17.775657778,-64.58363056, 19.44/60, name = 'VLBA St. Croix')
VLBI       = Observer(18.348237, -66.751539 ,43./60, name = 'VLBI Ref') #taken from Google Maps
# Taken from "DSN Station Locations and Uncertainties"
DSS_14     = Observer(35.425901438888886, 243.18634590081558, .03, altitude = 1002.114, name = 'DSS-14')
DSS_15     = Observer(35.42185386111111 , 243.11280693055554, .06, altitude =  973.945, name = 'DSS-15')

def rad(degrees):
    return degrees*pi/180

def deg(radians):
    return radians*180/pi

def sigfig(x,n):
        '''
        Round x to n significant figures
        '''
        if n <= 0:
            print("N must be >= 1")
            print("N is being set to 1")
            n = 1
        if int(x):
            return round(x,n-(int(log10(abs(x)))+1))
        if not int(x):
            return round(x,n-(int(log10(abs(x)))))

def nice_coordinates(coords):
    lat_direction='N'
    long_direction='E'
    
    if coords['lat'] < 0:
        coords['lat']*=-1
        lat_direction='S'
    if coords['long'] < 0:
        coords['long']*=-1
        long_direction='W'

    lat_degree=floor(coords['lat'])
    lat_minute=floor((coords['lat']-lat_degree)*60)
    lat_sec=(coords['lat']-lat_degree-lat_minute/60)*3600

    long_degree=floor(coords['long'])
    long_minute=floor((coords['long']-long_degree)*60)
    long_sec=(coords['long']-long_degree-long_minute/60)*3600

    latitude=str(int(lat_degree))+"d "+str(int(lat_minute))+"m "+"{0:.2f}".format(lat_sec)+'s '+lat_direction
    longitude=str(int(long_degree))+"d "+str(int(long_minute))+"m "+"{0:.2f}".format(long_sec)+'s '+long_direction
    
    return latitude + ', ' +longitude

def ymax_flat(elevation_B,elevation_A=90,HPBW_A=.037,HPBW_B=.352,d=238000):
    HPBW_A=rad(HPBW_A)
    HPBW_B=rad(HPBW_B)
    alpha=rad(elevation_A)
    beta=pi-rad(elevation_B)
    
    y=d*tan(alpha+HPBW_A/2)/(1-tan(alpha+HPBW_A/2)/tan(beta-HPBW_B/2))
    return y

def ymin_flat(elevation_B,elevation_A=90,HPBW_A=.037,HPBW_B=.352,d=238000):
    HPBW_A=rad(HPBW_A)
    HPBW_B=rad(HPBW_B)
    
    alpha=rad(elevation_A)
    beta=pi-rad(elevation_B)

    y=d*tan(alpha-HPBW_A/2)/(1-tan(alpha-HPBW_A/2)/tan(beta+HPBW_B/2))
    return y

def alt_range_flat(elevation_B,elevation_A=90,HPBW_A=.037,HPBW_B=.352,d=238000):
    alt_r=ymax_flat(elevation_B,elevation_A,HPBW_A,HPBW_B,d)-ymin_flat(elevation_B,elevation_A,HPBW_A,HPBW_B,d)
    return alt_r

def haversine(lat1,lat2,long1,long2):
    return sin(.5*rad(lat2-lat1))**2+cos(rad(lat1))*cos(rad(lat2))*sin(.5*rad(long2-long1))**2

def angle_between(lat1,lat2,long1,long2):
    return 2*arctan2(sqrt(haversine(lat1,lat2,long1,long2)),sqrt(1-haversine(lat1,lat2,long1,long2)))

def intersection_alt(elevA,elevB,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    theta=angle_between(latA,latB,longA,longB)
    alpha=rad(elevA)
    beta=pi-theta-rad(elevB)
    
    x_0=R_e*(cos(theta)-sin(theta)*tan(beta)-1)/(tan(alpha)-tan(beta))
    y_0=tan(alpha)*x_0+R_e

    altitude=sqrt(x_0**2+y_0**2)-R_e

    return altitude

def fp(x,elevA,elevB,HPBW_A=2./60,HPBW_B=12*2.38/60,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    return R_e + tan(rad(elevA+HPBW_A/2))*x

def fm(x,elevA,elevB,HPBW_A=2./60,HPBW_B=12*2.38/60,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    return R_e + tan(rad(elevA-HPBW_A/2))*x

def gp(x,elevA,elevB,theta=23.08,HPBW_A=2./60,HPBW_B=12*2.38/60,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    return R_e*cos(rad(theta))+tan(rad(180.-theta-elevB+HPBW_B/2))*(x-R_e*sin(rad(theta)))

def gm(x,elevA,elevB,theta=23.08,HPBW_A=2./60,HPBW_B=12*2.38/60,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    return R_e*cos(rad(theta))+tan(rad(180.-theta-elevB-HPBW_B/2))*(x-R_e*sin(rad(theta)))
    


def intersection_area(elevA,elevB,HPBW_A=2./60,HPBW_B=12*2.38/60,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    
    theta = angle_between(latA,latB,longA,longB)
    print(deg(theta))
    elevA = rad(elevA)
    elevB = rad(elevB)
    HPBW_A = rad(HPBW_A)
    HPBW_B = rad(HPBW_B)

    beta = pi-theta-elevB

    xpp = R_e*(1-cos(theta)+sin(theta)*tan(beta+HPBW_B/2))/(tan(beta+HPBW_B/2)-tan(elevA+HPBW_A/2))
    ypp = R_e + xpp*tan(elevA+HPBW_A/2)

    xpm = R_e*(1-cos(theta)+sin(theta)*tan(beta-HPBW_B/2))/(tan(beta-HPBW_B/2)-tan(elevA+HPBW_A/2))
    ypm = R_e + xpm*tan(elevA+HPBW_A/2)

    xmp = R_e*(1-cos(theta)+sin(theta)*tan(beta+HPBW_B/2))/(tan(beta+HPBW_B/2)-tan(elevA-HPBW_A/2))
    ymp = R_e + xmp*tan(elevA-HPBW_A/2)

    xmm = R_e*(1-cos(theta)+sin(theta)*tan(beta-HPBW_B/2))/(tan(beta-HPBW_B/2)-tan(elevA-HPBW_A/2))
    ymm = R_e + xmm*tan(elevA-HPBW_A/2)

    a = sqrt((xpp-xmp)**2+(ypp-ymp)**2)
    b = sqrt((xmp-xmm)**2+(ymp-ymm)**2)
    c = sqrt((xpp-xpm)**2+(ypp-ypm)**2)
    d = sqrt((xpm-xmm)**2+(ypm-ymm)**2)
    e = sqrt((xpp-xmm)**2+(ypp-ymm)**2)

    p1 = (a+b+e)/2
    p2 = (c+d+e)/2
    area1 = sqrt(p1*(p1-a)*(p1-b)*(p1-e))
    area2 = sqrt(p2*(p2-c)*(p2-d)*(p2-e))

    area = area1+area2
    
    print("plot(%s,%s,'r*')" % (xpp,ypp))
    print("plot(%s,%s,'r*')" % (xpm,ypm))
    print("plot(%s,%s,'r*')" % (xmp,ymp))
    print("plot(%s,%s,'r*')" % (xmm,ymm))
    
    plot(xpp,ypp,'c*')
    plot(xpm,ypm,'c*')
    plot(xmp,ymp,'c*')
    plot(xmm,ymm,'c*')
    
    

    return area

def intersection_range(elevA,elevB,latA=AO['lat'],longA=AO['long'],latB=GBT['lat'],longB=GBT['long']):
    theta=angle_between(latA,latB,longA,longB)
    alpha=rad(elevA)
    beta=pi-theta-rad(elevB)
    
    x_0=R_e*(cos(theta)-sin(theta)*tan(beta)-1)/(tan(alpha)-tan(beta))
    y_0=tan(alpha)*x_0+R_e

    R_A=sqrt(x_0**2+(y_0-R_e)**2)
    R_B=sqrt((x_0-R_e*sin(theta))**2+(y_0-R_e*cos(theta))**2)
    return R_A,R_B

def coords_of_intersection(elevA,elevB,coordsA=AO,coordsB=GBT):
    theta=angle_between(coordsA['lat'],coordsB['lat'],coordsA['long'],coordsB['long'])
    alpha=rad(elevA)
    beta=pi-theta-rad(elevB)
    
    x_0=R_e*(cos(theta)-sin(theta)*tan(beta)-1)/(tan(alpha)-tan(beta))
    y_0=tan(alpha)*x_0+R_e

    theta_0=pi/2-arctan2(y_0,x_0)
    f=theta_0/theta

    a=sin((1-f)*theta_0)/sin(theta_0)
    b=sin(f*theta_0)/sin(theta_0)

    x=a*cos(rad(coordsA['lat']))*cos(rad(coordsA['long']))+b*cos(rad(coordsB['lat']))*cos(rad(coordsB['long']))
    y=a*cos(rad(coordsA['lat']))*sin(rad(coordsA['long']))+b*cos(rad(coordsB['lat']))*sin(rad(coordsB['long']))
    z=a*sin(rad(coordsA['lat']))+b*sin(rad(coordsB['lat']))

    lat_0=arctan2(z,sqrt(x**2+y**2))
    long_0=arctan2(y,x)

    coords_0={'lat':deg(lat_0),'long':deg(long_0)}
    
    return coords_0

lat_AO=18.6
zen_AO=15.
GM=3.986*10**14

def vdop(h,inclination, zen=zen_AO, lam_0=lat_AO):

    inclination=deg2rad(inclination)
    zen=deg2rad(zen)
    lam_0=deg2rad(lam_0)
    r=sqrt(R_e**2*(sin(lam_0)**2+cos(lam_0)**2*cos(zen))-(h**2-2*R_e*h))
    r-=R_e*(sin(lam_0)**2+cos(lam_0)**2*cos(zen))
	
    longitude=arccos(r*cos(lam_0)*sin(zen)/(R_e*cos(lam_0)+r*cos(lam_0)*cos(zen)))

    lam=pi/2-arccos((R_e+r)*sin(lam_0)/sqrt(R_e**2+r**2+2*R_e*r*(sin(lam_0)**2+cos(lam_0)**2*cos(zen))))

    theta=arccos(cos(inclination)/cos(lam))

    cos_a=sin(theta)*cos(longitude)*cos(lam_0)*cos(zen)
    cos_a+=sin(theta)*sin(longitude)*cos(lam_0)*sin(zen)
    cos_a+=cos(theta)*sin(lam_0)

    v=sqrt(GM/(h+R_e))*cos_a

    return v

def max_intersect_alt(obs1 , obs2):        
    
    theta = angle_between(obs1.latitude,obs2.latitude,obs1.longitude,obs2.longitude)
    #print deg(theta)
    #print "Theta + elevation: " + str(deg(theta) + obs1.elevation) + "degrees"
    
    elevA = rad(obs1.elevation)
    #print obs1.name + " elevation is: " + str(obs1.elevation)
    #print obs2.name + " elevation is: " + str(obs2.elevation)

    elevB = rad(obs2.elevation)
    HPBW_A = rad(obs1.HPBW)
    HPBW_B = rad(obs2.HPBW)

    beta = pi-theta-elevB
    print("Beta: " + str(deg(beta)))
    

    xpp = R_e*(1-cos(theta)+sin(theta)*tan(beta+HPBW_B/2))/(tan(beta+HPBW_B/2)-tan(elevA+HPBW_A/2))
    ypp = R_e + xpp*tan(elevA+HPBW_A/2)

    xpm = R_e*(1-cos(theta)+sin(theta)*tan(beta-HPBW_B/2))/(tan(beta-HPBW_B/2)-tan(elevA+HPBW_A/2))
    ypm = R_e + xpm*tan(elevA+HPBW_A/2)

    xmp = R_e*(1-cos(theta)+sin(theta)*tan(beta+HPBW_B/2))/(tan(beta+HPBW_B/2)-tan(elevA-HPBW_A/2))
    ymp = R_e + xmp*tan(elevA-HPBW_A/2)

    xmm = R_e*(1-cos(theta)+sin(theta)*tan(beta-HPBW_B/2))/(tan(beta-HPBW_B/2)-tan(elevA-HPBW_A/2))
    ymm = R_e + xmm*tan(elevA-HPBW_A/2)

    
    #print "plot(%s,%s,'r*')" % (xpp,ypp)
    #print "plot(%s,%s,'r*')" % (xpm,ypm)
    #print "plot(%s,%s,'r*')" % (xmp,ymp)
    #print "plot(%s,%s,'r*')" % (xmm,ymm)
    
    plot(xpp,ypp,'r*')
    plot(xpm,ypm,'r*')
    plot(xmp,ymp,'r*')
    plot(xmm,ymm,'r*')
    
    #print "Max Altitude is: " + str((max([ypp,ypm,ymp,ymm])-R_e)/1000) + " km"
    #print ypp,ypm,ymp,ymm
    
    
    return max(array([ypp,ypm,ymp,ymm]) - R_e)

def intersection_volume_flat(obs1, obs2, samples = 1000000, alt_max = 2000000.):        
    

    elevA = rad(obs1.elevation)
    elevB = rad(obs2.elevation)
    HPBW_A = rad(obs1.HPBW)
    HPBW_B = rad(obs2.HPBW)


    
    elev = elevB#rad(90.-90)
    beta = theta - pi/2 + elev
    
    #theta = 5*pi/180
    #HPBW_A = pi/10
    #HPBW_B = pi/20
    
    #alt_max = 4000000 #2000 km

    alt_max = max_intersect_alt(obs1,obs2)*1.25
    if alt_max < 0:
        alt_max = 2000000.

    X = alt_max*tan(HPBW_A/2)
    Y = alt_max*tan(HPBW_A/2)
    Z = R_e + alt_max
    
    
    x = (rand.rand(samples)-.5)*2*X#*tan(rad(2./60))
    y = (rand.rand(samples)-.5)*2*Y#*tan(rad(2./60))
    z = (rand.rand(samples))*Z
    
    total_v = 2*X*2*Y*Z
    #print total_v

    beam1  = x**2 + ((y-R_e*sin(theta))*cos(beta)-((z-R_e*cos(theta))*sin(beta)))**2 <= (((y-R_e*sin(theta))*sin(beta)+(z-R_e*cos(theta))*cos(beta))*tan(HPBW_B/2))**2#*tan(HPBW_A))**2
    beam2 = x**2 + y**2<=((z-R_e)*tan(HPBW_A/2))**2
    half_cone = z >= R_e#*cos(theta)
    
    beam1 = ((beam1.astype('int') + half_cone.astype('int'))/2).astype('bool')
    beam2 = ((beam2.astype('int') + half_cone.astype('int'))/2).astype('bool')
    intersection  = ((beam1.astype('int') + beam2.astype('int'))/2).astype('bool')
    #print sum(beam1)
    #print sum(beam2)
    

    clf()
    plot(y[beam1],z[beam1],'b*')
    plot(y[beam2],z[beam2],'c*')
    plot(y[intersection],z[intersection],'g*')
    max_intersect_alt(obs1,obs2)
    xlim(-X,X)
    ylim(R_e,Z)
    
    
    yticks(linspace(sigfig(R_e,2),sigfig(Z,2),9),linspace(0,sigfig(Z,2)/1000,9))    
    ylabel("Altitude (km)")
    xlabel("\"X\"-tent (meters)")
    grid()
    theta = linspace(-pi/2,pi/2,100000)
    plot(R_e*sin(theta),R_e*cos(theta))

    intersect_v = float(sum(intersection))/samples * total_v
    
    print("Volume of Intersection: " + str((intersect_v/1000.**3)) + " km^3")

    return intersect_v
    return x,y,z,intersection

def intersection_volume(obs1, obs2, samples = 1000000, alt_max = 2000000.):        
    
    theta = angle_between(obs1.latitude,obs2.latitude,obs1.longitude,obs2.longitude)
    print( "Geocentric Sepatation: " + str(deg(theta)) + " degrees")
    elevA = rad(obs1.elevation)
    elevB = rad(obs2.elevation)
    HPBW_A = rad(obs1.HPBW)
    HPBW_B = rad(obs2.HPBW)


    
    elev = elevB#rad(90.-90)
    beta = theta - pi/2 + elev
    
    #theta = 5*pi/180
    #HPBW_A = pi/10
    #HPBW_B = pi/20
    
    #alt_max = 4000000 #2000 km

    alt_max = max_intersect_alt(obs1,obs2)*1.25
    if alt_max < 0:
        alt_max = 2000000.

    X = alt_max*tan(HPBW_A/2)
    Y = alt_max*tan(HPBW_A/2)
    Z = R_e + alt_max
    
    
    x = (rand.rand(samples)-.5)*2*X#*tan(rad(2./60))
    y = (rand.rand(samples)-.5)*2*Y#*tan(rad(2./60))
    z = (rand.rand(samples))*Z
    
    total_v = 2*X*2*Y*Z
    #print total_v

    beam1  = x**2 + ((y-R_e*sin(theta))*cos(beta)-((z-R_e*cos(theta))*sin(beta)))**2 <= (((y-R_e*sin(theta))*sin(beta)+(z-R_e*cos(theta))*cos(beta))*tan(HPBW_B/2))**2#*tan(HPBW_A))**2
    beam2 = x**2 + y**2<=((z-R_e)*tan(HPBW_A/2))**2
    half_cone = z >= R_e#*cos(theta)
    
    beam1 = ((beam1.astype('int') + half_cone.astype('int'))/2).astype('bool')
    beam2 = ((beam2.astype('int') + half_cone.astype('int'))/2).astype('bool')
    intersection  = ((beam1.astype('int') + beam2.astype('int'))/2).astype('bool')
    #print sum(beam1)
    #print sum(beam2)
    

    clf()
    plot(y[beam1],z[beam1],'b*')
    plot(y[beam2],z[beam2],'c*')
    plot(y[intersection],z[intersection],'g*')
    max_intersect_alt(obs1,obs2)
    xlim(-X,X)
    ylim(R_e,Z)
    
    
    yticks(linspace(sigfig(R_e,2),sigfig(Z,2),9),linspace(0,sigfig(Z,2)/1000,9))    
    ylabel("Altitude (km)")
    xlabel("\"X\"-tent (meters)")
    grid()
    theta = linspace(-pi/2,pi/2,100000)
    plot(R_e*sin(theta),R_e*cos(theta))

    intersect_v = float(sum(intersection))/samples * total_v
    
    print("Volume of Intersection: " + str((intersect_v/1000.**3)) + " km^3")

    return intersect_v
    return x,y,z,intersection

Green_Bank.elevation = 62.2
VLBA.elevation = 30.
#intersection_volume(obs1 = DSS_14, obs2 = DSS_15)
intersection_volume(Arecibo, VLBI)
'''
blah = zeros(10)
for i in range(len(blah)):
    blah [i] = intersection_volume(90.,15,samples = 100000)/1000**3

#plot(blah)
print "Mean: %s" % str(mean(blah))
print "Std : %s" % str(std(blah))
'''
