# -*- coding: utf-8 -*-
"""
Created on Tue May 4 16:20:49 2018

@author: jimurray
"""

from .conversion import G_k2G, dB2lin, f2lam

from .constants import MW, kW, W,\
                      ns, us, ms, s,\
                      km, m, cm, mm,\
                      GHz, MHz, kHz, Hz,\
                      MW, kW, W,\
                      deg, arcsec, arcmin


class Receiver:
    def __init__(self, T_sys, BW, peak_gain, HPBW, loss=1, SNR_min=10., name='', band = ''):
        self.T_sys = T_sys
        self.BW = BW
        self.SNR_min = SNR_min
        self.peak_gain = peak_gain
        self.loss = loss
        self.HPBW = HPBW
        self.name = name
        self.band = band

Goldstone = Receiver(T_sys=21.79, BW=1.25*MHz, peak_gain=dB2lin(68.5), HPBW=0.06*deg, loss=dB2lin(3.9), band='X')


HUSIR = Receiver(T_sys=186., BW=1.25*MHz, peak_gain=dB2lin(67.23), HPBW=.058*deg, loss=dB2lin(3.9), band='X')
HAX   = Receiver(T_sys=161., BW=1.25*MHz, peak_gain=dB2lin(63.64), HPBW= 0.1*deg, loss=dB2lin(4.5), band='Ku')

AO_S    = Receiver(T_sys=20.0, BW=  10*MHz, peak_gain=dB2lin(73.7),   HPBW=  1.9*arcmin, band='S')
AO_UHF  = Receiver(T_sys=66.0, BW=   2*MHz, peak_gain=dB2lin(58.9),   HPBW=12.24*arcmin, band='UHF')
AO_X    = Receiver(T_sys=30.0, BW= 2.4*GHz, peak_gain=G_k2G(4.,3*cm), HPBW= 0.57*arcmin, band='X')
AO_C    = Receiver(T_sys=34.5, BW=2.15*GHz, peak_gain=G_k2G( 6.,f2lam(5480*MHz)), HPBW=1.0*arcmin, band='C')
AO_ALFA = Receiver(T_sys=30.0, BW= 300*MHz, peak_gain=G_k2G(10.,f2lam(1215*MHz)), HPBW=3.8*arcmin, band='L')

NGAT_S = Receiver(T_sys=25.0, BW= 10*MHz, peak_gain=G_k2G(20.,f2lam(2380*MHz)), HPBW=1.9*arcmin, band='S')
NGAT_C = Receiver(T_sys=25.0, BW= 10*MHz, peak_gain=G_k2G(18.,f2lam(5000*MHz)), HPBW=1.0*arcmin, band='C')

#Check bandwidth on these
VLBI_S = Receiver(T_sys=107.0, BW= 10*MHz, peak_gain=dB2lin(47.2), HPBW=0.665*deg, band='S')
VLBA_S = Receiver(T_sys= 40.0, BW=200*MHz, peak_gain=dB2lin(52.7), HPBW=0.352*deg, band='S')

GBT_X  = Receiver(T_sys=27.0, BW=500*MHz, peak_gain=G_k2G(2,3*cm), HPBW=1.4*arcmin, band='X')
#GBT_C = Receiver(T_sys=18., BW=3.8*10.**9, peak_gain=G_k2G(2,.03))
GBT_S  = Receiver(T_sys=18.0, BW=300*MHz, peak_gain=G_k2G(2.0,f2lam(2380*MHz)), HPBW= 5.8*arcmin, band='S')
GBT_L  = Receiver(T_sys=20.0, BW=300*MHz, peak_gain=G_k2G(2.0,f2lam(1295*MHz)), HPBW= 9.0*arcmin, band='L')
GBT_Ka = Receiver(T_sys=30.0, BW=300*MHz, peak_gain=G_k2G(1.8,f2lam(  30*GHz)), HPBW=22.6*arcsec, band='Ka')

GBT_Ka = Receiver(T_sys=30.0, BW=10*MHz, peak_gain=G_k2G(1.8,f2lam(  35*GHz)), HPBW=22.6*arcsec, band='Ka')
