# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:36:16 2020

@author: jimurray
"""

import numpy as np
import pandas as pd

DTR = np.pi/180.

a = 6378137.0000 # meters
b = 6356752.3142 # meters


def observer_area(start, stop, step, ant):
    
    start_alts = np.arange(start, stop, step, dtype = 'float')
    stop_alts  = start_alts + step
    start_ranges0 = []
    stop_ranges0  = []
    areas = []
    
    for alt in start_alts:
        rng1 , rng2, area = SURF_FLUX_BEAM_AREA(ant.latitude, ant.azimuth, \
                                                ant.elevation, ant.HPBW, \
                                                alt, alt + step, ant.altitude)
        start_ranges0.append(rng1)
        stop_ranges0.append(rng2)
        areas.append(area)
        
    start_ranges0 = np.array(start_ranges0)
    start_ranges1 = start_ranges0
    stop_ranges0  = np.array(stop_ranges0)
    stop_ranges1  = stop_ranges0

        
    names = ['ALT1_KM', 'ALT2_KM', 'RNG1_KM_0', 'RNG2_KM_0','RNG1_KM_1', 'RNG2_KM_1', 'AREA_KM^2']

    areas = pd.DataFrame(np.array([start_alts, stop_alts, \
                        start_ranges0, stop_ranges0,\
                        start_ranges1, stop_ranges1, areas]).T, \
                        columns = names)
    return areas

def SURF_FLUX_BEAM_AREA(SENSOR_LAT_DEG, SENSOR_AZ_DEG, SENSOR_EL_DEG, \
                        SENSOR_FULL_BEAM_WIDTH_DEG, \
                        ALT1_KM, ALT2_KM, HEIGHT):
    
    RTEL_KM = LLH2EFG_Meeus(SENSOR_LAT_DEG, HEIGHT)
    
    LOOK_N = calc_LOOK_N(SENSOR_LAT_DEG, SENSOR_AZ_DEG, SENSOR_EL_DEG)
    
    RNG1_KM = RNG_KM_FROM_ALT_KM(ALT1_KM,LOOK_N,RTEL_KM)
    RNG2_KM = RNG_KM_FROM_ALT_KM(ALT2_KM,LOOK_N,RTEL_KM)

    RNGDOTR = calc_RNGDOTR(RNG1_KM, RNG2_KM, RTEL_KM, LOOK_N)
    
    AREA_KM2 = (SENSOR_FULL_BEAM_WIDTH_DEG * DTR)  \
                * (np.pi / 2.0)                    \
                * (RNG2_KM**2 - RNG1_KM**2)        \
                * RNGDOTR  # ! CORRECTION FOR VERTICAL ANGLE
               
    return RNG1_KM, RNG2_KM, AREA_KM2

def LLH2EFG_Meeus(SENSOR_LAT_DEG, HEIGHT, b = b, a = a):

    RTEL_KM = np.zeros(3)
    
    U_RAD = np.arctan(b/a * np.tan(SENSOR_LAT_DEG * DTR))
    RTEL_KM[0] = np.cos(U_RAD) 
    RTEL_KM[0] = RTEL_KM[0] + HEIGHT / a  * np.cos(SENSOR_LAT_DEG * DTR)
    RTEL_KM[0] = a * RTEL_KM[0]
    
    RTEL_KM[1] = 0.0
    
    RTEL_KM[2] = np.sin(U_RAD) * b/a
    RTEL_KM[2] = RTEL_KM[2] + HEIGHT / a * np.sin(SENSOR_LAT_DEG * DTR)
    RTEL_KM[2] = a * RTEL_KM[2]

    return RTEL_KM/1000.



def calc_LOOK_N(SENSOR_LAT_DEG, SENSOR_AZ_DEG, SENSOR_EL_DEG):
    
    LOOK_N = np.zeros(3)
    NORTH = np.zeros(3)
    EAST = np.zeros(3)
    UP = np.zeros(3)
    
    CAZ_TEL = np.cos(SENSOR_AZ_DEG * DTR)
    SAZ_TEL = np.sin(SENSOR_AZ_DEG * DTR)

    CEL_TEL = np.cos(SENSOR_EL_DEG * DTR)
    SEL_TEL = np.sin(SENSOR_EL_DEG * DTR)

    NORTH[0] = -np.sin(SENSOR_LAT_DEG * DTR)
    NORTH[1] = 0.0
    NORTH[2] = np.cos(SENSOR_LAT_DEG * DTR)

    EAST[0] = 0.0
    EAST[1] = 1.0
    EAST[2] = 0.0

    UP[0] = EAST[1] * NORTH[2] - EAST[2] * NORTH[1]
    UP[1] = EAST[2] * NORTH[0] - EAST[0] * NORTH[2]
    UP[2] = EAST[0] * NORTH[1] - EAST[1] * NORTH[0]

    LOOK_N[0] = CAZ_TEL * CEL_TEL * NORTH[0] \
              + SAZ_TEL * CEL_TEL * EAST[0]  \
              + SEL_TEL * UP[0]
    LOOK_N[1] = CAZ_TEL * CEL_TEL * NORTH[1] \
              + SAZ_TEL * CEL_TEL * EAST[1]  \
              + SEL_TEL * UP[1]
    LOOK_N[2] = CAZ_TEL * CEL_TEL * NORTH[2] \
              + SAZ_TEL * CEL_TEL * EAST[2]  \
              + SEL_TEL * UP[2]
              
    return LOOK_N

def calc_RNGDOTR(RNG1_KM, RNG2_KM, RTEL_KM, LOOK_N):
    
    R_KM = RTEL_KM + 0.5 * (RNG1_KM + RNG2_KM) * LOOK_N
    
    RAD_KM = np.linalg.norm(R_KM)
    
    RNGDOTR =  LOOK_N.dot(R_KM) / RAD_KM
    
    return RNGDOTR

def RNG_KM_FROM_ALT_KM(ALT_KM,LOOK_N,RTEL_KM):
#    c  Convert an altitude to a sensor range for a given sensor
#    c  location and pointing angle

    RNG1 = 0.0    
    ALT1 = np.linalg.norm(RTEL_KM + RNG1 * LOOK_N) - a/1000.

    RNG3 = 2.0 * ALT_KM + 1.0
    ALT3 = np.linalg.norm(RTEL_KM + RNG3 * LOOK_N) - a/1000.

    while (ALT3 < ALT_KM):# THEN  ! not yet bracketed
        RNG3 = 2.0 * RNG3
        ALT3 = np.linalg.norm(RTEL_KM + RNG3 * LOOK_N) - a/1000.

#   C  Answer bracketed, begin iterations

    while ((RNG3 - RNG1) > 1.0e-6):
        RNG2 = 0.5 * (RNG1 + RNG3)
        ALT2 = np.linalg.norm(RTEL_KM + RNG2 * LOOK_N) - a/1000.

        if (ALT2 < ALT_KM):# THEN
            ALT1 = ALT2
            RNG1 = RNG2
        elif (ALT2 > ALT_KM):# THEN
            ALT3 = ALT2
            RNG3 = RNG2
        else:
            #!  Exact match!
            return RNG2
        
    return RNG2
    