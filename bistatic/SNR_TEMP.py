# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 18:08:59 2021

@author: maward2
"""

import numpy as np

from .SEM import SEM_inv

from .PD import marcumsq

def SNR(size):
    
    tau = 1.6384*10**-3
    BW_TX = 1./tau
    P_t = 250. * 10**3
    G_t = 10**6.723
    G_r = 10**6.723
    wavelength = .03
    R_t = 775000.
    R_r = 775000.
    k_b = 1.38*10**-23
    T_sys = 186.
    BW_RX = 1250000.
    L_r = 10**.39
    
    RCS = SEM_inv(size, wavelength)

    SNR = RCS*tau*BW_TX*P_t*G_t*G_r*wavelength**2/\
          ((4*np.pi)**3*R_t**2*R_r**2*k_b*T_sys*BW_RX*L_r)

    return SNR


def cumflux_with_Pd(sizes, fluxes, SNR_tresh_dB = 10.):
    
    f = -np.diff(fluxes)
    
    SNR_avg_dB = 10*np.log10(SNR(sizes))[1:]
    
    cumflux = np.cumsum((marcumsq(SNR_avg_dB, SNR_tresh_dB)*f)[::-1])[::-1]
    
    return cumflux