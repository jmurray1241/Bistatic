# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 16:20:49 2018

@author: jimurray
"""
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from time import time

from scipy.spatial import ConvexHull

import itur
#import sobol_seq

from . import observer2 as obs
from .gain_pattern import airy, gaussian, obscured_airy, airy_cos
from .SEM import SEM, SEM_inv
from .points import lin_grid

from .error import optimal_steps, optimal_samples, optimal_steps2
from .plots import gain_plot, plot_gainVrange, plot_RCSVrange, plot_sizeVrange, plot_areas, area_debug

from .conversion import G_k2G, dB2lin, lin2dB, f2lam, deg2rad, rad2deg

from .constants import MW, kW, W,\
                      ns, us, ms, s,\
                      km, m, cm, mm,\
                      GHz, MHz, kHz, Hz,\
                      MW, kW, W,\
                      deg, arcsec, arcmin,\
                      speed_of_light, k_B
                                

#####################
#    To Do:
#   -Use astroconst
#   -Increase font sizes for plots
#   -Add doc



# c = 299792458.0 # meters per sec
# k_B = 1.38064852*10**-23
# #f = 8560. # MHz
# #lam = 300./f

# lam_x = .03512 # m
# lam_s = .13 # m

# R_e = 6378136.3 # ITRF 93
# f = 298.257# Earth Flattening


def R_az_el(R, ant0, ant1, supress_output = False):
    """Return the range, azimuth, and elevation from ant1 to the intersection point
    Parameters:
        R: km, range from TX to intersection point
        ant0: Observer object
        ant1: Observer object 
    Returns:
        slant_range: km, range from ant1 to intersection point
        az: degrees,  azimuth from ant1 to intersection point 
        el: degrees,  elevation from ant1 to intersection point 
    """
    b = R*km*ant0.b + ant0.p - ant1.p
    b_east = np.dot(b,ant1.k_east)
    b_north = np.dot(b,ant1.k_north)
    b_perp = np.dot(b,ant1.n)
    az = np.arctan2(b_east,b_north)
    el = np.pi/2-np.arctan2(np.sqrt(b_north**2+b_east**2),b_perp)

    slant_range = np.linalg.norm(b)/1000.
    az = rad2deg(az)
    el = rad2deg(el)

    if not supress_output:
        print(ant1.name + " slant range: " + str(round(slant_range)) + " km")
        print("Azimuth: " + str(az) + " degrees")
        print("Elevation: " +str(el) + " degrees")

    return slant_range, az, el

def baseline_bearing(ant0, ant1):
    """ """
    baseline, bearing_at_0, bearing_at_1 = ant0.point.distance_and_azimuth(ant1.point)
    print("Bearing: " + str(rad2deg(bearing_at_0)) + " deg")
    print("Baseline: " + str(baseline/km) + " km")
    return baseline, rad2deg(bearing_at_0)

def point_ant2ant(R, ant0, ant1):
    slant_range, azimuth, elevation = R_az_el(R, ant0, ant1)
    ant1.set_pointing(azimuth, elevation)
    return 0

def gainVrange(R, TX, RX, samples = 50, plot=True, along='tx', width = .1, gain_pattern = airy, atmospheric_loss = False, supress_output = False):    
    #RX.wavelength = TX.wavelength
#    gain = []
    gain = np.zeros_like(R)
    for i,r in enumerate(R):
        if along == 'tx':
            x,y,G = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, along=along, gain_pattern = gain_pattern)
            r_tx = r
            r_rx = RX.alt2slant([TX.slant2alt([r])])
        elif along == 'rx':
            x,y,G = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, along=along, gain_pattern = gain_pattern)
            r_rx = r
            r_tx = RX.alt2slant([TX.slant2alt([r])])
        else:
            print("Argument \'along\' must be either \'tx\' or \'rx\'...")
            return 0
        gain[i] = np.max(G)
        # print("Gain:")
        # print(G)
        # print("Max Gain:" + str(gain[i]))
        if atmospheric_loss:
            p = .001
            loss_dB_TX = itur.atmospheric_attenuation_slant_path(TX.latitude, TX.longitude,\
                        speed_of_light/TX.wavelength/GHz, TX.elevation, p, 2*TX.radius,\
                        Ls = r_tx, include_rain = False)
            loss_dB_RX = itur.atmospheric_attenuation_slant_path(RX.latitude, RX.longitude,\
                        speed_of_light/TX.wavelength/GHz, RX.elevation, p, 2*RX.radius,\
                        Ls = r_rx, include_rain = False)
            # itur function returns astropy Quantity object
            loss_TX = dB2lin(loss_dB_TX.value)
            loss_RX = dB2lin(loss_dB_RX.value)
            
            # gain[i] = gain[i]*loss_TX*loss_RX
            gain[i] = gain[i]/loss_TX/loss_RX

        # print("Max Gain:" + str(gain[i]))

        if not supress_output:
            print("Range: " + str(r) + " km")
        
#    gain = np.array(gain)
    if plot:
        plot_gainVrange(R, gain, TX.name, RX.name, along)
        
    return gain



def RCSVrange(R, TX, RX, samples = 50, plot=True, along ='tx', width = .1, atmospheric_loss = False, gain_pattern = airy, supress_output = False):
    if not (along == 'tx' or along == 'rx'):
        print("Argument \'along\' must be either \'tx\' or \'rx\'...")
        return 0

    # if TX.sensor == 'transmitter' and RX.sensor == 'receiver':
    #     TX = TX
    #     RX = RX
    # elif RX.sensor == 'transmitter' and TX.sensor == 'receiver':
    #     TX = RX
    #     RX = TX
    # else:
    #     print "Must have one transmitter and one receiver..."
    #     return 0

    if not (RX.receiver.band == TX.transmitter.band):
        print("The transmitter and receiver must be in the same band...")
        return 0

    RX.wavelength = TX.wavelength


    RCS = []
    for r in R:
        if along == 'tx':
            x,y,G_R = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
            r_tx = r
            r_rx = RX.alt2slant([TX.slant2alt([r])])
        elif along == 'rx':
            x,y,G_R = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
            r_rx = r
            r_tx = RX.alt2slant([TX.slant2alt([r])])
            
        RCS.append(np.min(RCS_min(G_R,TX.transmitter,RX.receiver)))
        
        if atmospheric_loss:
            p = .001
            loss_dB_TX = itur.atmospheric_attenuation_slant_path(TX.latitude, TX.longitude,\
                        speed_of_light/TX.wavelength/GHz, TX.elevation, p, 2*TX.radius,\
                        Ls = r_tx, include_rain = False)
            loss_dB_RX = itur.atmospheric_attenuation_slant_path(RX.latitude, RX.longitude,\
                        speed_of_light/TX.wavelength/GHz, RX.elevation, p, 2*RX.radius,\
                        Ls = r_rx, include_rain = False)
            # itur function returns astropy Quantity object
            loss_TX = dB2lin(loss_dB_TX.value)
            loss_RX = dB2lin(loss_dB_RX.value)
            
            # RCS[-1] = RCS[-1]*loss_TX*loss_RX
            RCS[-1] = RCS[-1]/loss_TX/loss_RX
            
        if not supress_output:
            print("Range: " + str(r) + " km")    
            
    RCS = np.array(RCS)
    if plot:
        plot_RCSVrange(R, RCS, TX.name, RX.name, along, TX.transmitter.band)

    return RCS

def sizeVrange(R, TX, RX, samples = 50, plot=True, along='tx', width = .1, atmospheric_loss = False, gain_pattern = airy, supress_output = False):
    
    size = SEM(RCSVrange(R, TX, RX, samples = samples, plot = plot, \
                         along = along, width=width, atmospheric_loss = atmospheric_loss, gain_pattern = gain_pattern, supress_output = supress_output),\
                        TX.wavelength)        
    if plot:
        plot_sizeVrange(R, size, TX.name, RX.name, along, TX.transmitter.band)

    return size




def powerVrange(R, TX, RX, samples = 50, plot=False, along ='tx', width = .1, size = .005, gain_pattern = airy):
    if not (along == 'tx' or along == 'rx'):
        print("Argument \'along\' must be either \'tx\' or \'rx\'...")
        return 0

    if not (RX.receiver.band == TX.transmitter.band):
        print("The transmitter and receiver must be in the same band...")
        return 0

    #RX.wavelength = TX.wavelength

    power = []
    for r in R:
        if along == 'tx':
            x,y,G_R = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
        elif along == 'rx':
            x,y,G_R = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
        
        power.append(np.min(power_min(G_R,TX.transmitter,RX.receiver, size=size)))
        print("Range: " + str(r) + " km")
    power = np.array(power)
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        plt.title(TX.name+"/"+RX.name+" Minimum Transmit Power v. "+TX.name+" slant range")
        ax.plot(R,power/kW)
        plt.xlabel(TX.name + " Slant Range [km]")
        plt.ylabel("Minimum Power [kW]")
        plt.grid()
        plt.show()

    return power

def powerVsize(sizes, TX, RX, samples = 50, plot=False, along ='tx', width = .1, R = np.array([1000.]), gain_pattern = airy):
    if not (along == 'tx' or along == 'rx'):
        print("Argument \'along\' must be either \'tx\' or \'rx\'...")
        return 0

    if not (RX.receiver.band == TX.transmitter.band):
        print("The transmitter and receiver must be in the same band...")
        return 0

    #RX.wavelength = TX.wavelength

    power = []
    for size in sizes:
        if along == 'tx':
            x,y,G_R = g_wgs(R, TX, RX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
        elif along == 'rx':
            x,y,G_R = g_wgs(R, RX, TX, samples=samples, plot=False, width=width, ranges=True, gain_pattern = gain_pattern)
        
        power.append(np.min(power_min(G_R,TX.transmitter,RX.receiver, size=size)))
        print("Size: " + str(size) + " m")
    power = np.array(power)
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        plt.title(TX.name+"/"+RX.name+" Minimum Transmit Power v. "+TX.name+" Size")
        ax.plot(sizes,power/kW)
        plt.xlabel("Size [m]")
        plt.ylabel("Minimum Power [kW]")
        plt.grid()
        plt.show()

    return power



def areaVrange_tx(R, TX, RX ,samples=200, plot=True, thresh = 1./4, param = 'gain', width = .15, gain_pattern=airy):
    """
    param = 'gain', 'RCS', or 'size'

    If param is 'gain', the wavelength will default to that of TX.
    If param is 'RCS' or 'size', the observer object must be passed 
    using the .as_a(sensor) command, i.e. TX.as('transmitter'). 
    """
    if not (param == 'RCS' or param == 'size' or param == 'gain'):
        print("Invalid parameter type...")
        return 0

    if param == 'RCS' or param == 'size':
        if TX.sensor == 'transmitter' and RX.sensor == 'receiver':
            TX = TX
            RX = RX
        elif RX.sensor == 'transmitter' and TX.sensor == 'receiver':
            TX = RX
            RX = TX
        else:
            print("Must have one transmitter and one receiver...")
            return 0

        if not (RX.receiver.band == TX.transmitter.band):
            print("The transmitter and receiver must be in the same band...")
            return 0

    else:
        TX = TX
        RX = RX

    RX.wavelength = TX.wavelength
    

    C = []
    Rmin = R.min()
    delR = R.max()-R.min()
    start = time()
    for r in R:
        if param == 'gain':
            x,y,G = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, gain_pattern=gain_pattern)
            C.append(circum(x,y,G,thresh,param))
        elif param == 'RCS':
            x,y,G_R = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,RCS_min(G_R,TX.transmitter,RX.receiver),thresh,param))
        elif param == 'size':
            x,y,G_R = g_wgs(r, TX, RX, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,SEM(RCS_min(G_R,TX.transmitter,RX.receiver),TX.wavelength),thresh,param))
            
        print("Range: " + str(round(r)) + " km" + "    Percent: "+str(round(np.abs(Rmin - r)/delR*100)) + "%")
        
    dR = np.abs(R[1] - R[0])
    C = np.array(C)

    area = C*dR
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        ax.bar(R,area,dR)
        plt.xlabel(TX.name + " Slant Range [km]")
        plt.ylabel("Composite Beam Area [km^2]")
        #Formula for the pointing range in the title is wrong
        plt.title(TX.name+"/"+RX.name+" Bistatic Beam Area")#: Pointing " +str(round(R[np.argmax(C)]))+" km, Az: " + str(TX.azimuth)+ " El: " +str(TX.elevation))
        plt.grid()
        xmax = np.max(R[np.where( (C > 0.))[0]])
        xmin = np.min(R[np.where( (C > 0.))[0]])
        plt.xlim(xmin,xmax)
        plt.show()
    stop = time()
    
    print("Execution: " + str(stop - start) + " s")
    
    return area   

#This function needs to be updated
def areaVrange_rx(R, TX, RX ,samples=200, plot=True, thresh = 1./4, param = 'gain', width = .15, gain_pattern=airy):
    """
    param = 'gain', 'RCS', or 'size'

    If param is 'gain', the wavelength will default to that of TX.
    If param is 'RCS' or 'size', the observer object must be passed 
    using the .as_a(sensor) command, i.e. TX.as('transmitter'). 
    """
    if not (param == 'RCS' or param == 'size' or param == 'gain'):
        print("Invalid parameter type...")
        return 0

    if param == 'RCS' or param == 'size':
        if TX.sensor == 'transmitter' and RX.sensor == 'receiver':
            TX = TX
            RX = RX
        elif RX.sensor == 'transmitter' and TX.sensor == 'receiver':
            TX = RX
            RX = TX
        else:
            print("Must have one transmitter and one receiver...")
            return 0

        if not (RX.receiver.band == TX.transmitter.band):
            print("The transmitter and receiver must be in the same band...")
            return 0

    else:
        TX = TX
        RX = RX

    RX.wavelength = TX.wavelength

    C = []
    Rmin = R.min()
    delR = R.max()-R.min()
    start = time()
    for r in R:
        if param == 'gain':
            x,y,G = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, gain_pattern=gain_pattern)
            C.append(circum(x,y,G,thresh,param))
        elif param == 'RCS':
            x,y,G_R = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,RCS_min(G_R,TX.transmitter,RX.receiver),thresh,param))
        elif param == 'size':
            x,y,G_R = g_wgs(r, RX, TX, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,SEM(RCS_min(G_R,TX.transmitter,RX.receiver),TX.wavelength),thresh,param))
            
        print("Range: " + str(round(r)) + " km" + "    Percent: "+str(round(np.abs(Rmin - r)/delR*100)) + "%")
        
    dR = np.abs(R[1] - R[0])
    C = np.array(C)

    area = C*dR
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        ax.bar(R,area,dR)
        plt.xlabel(RX.name + " Slant Range [km]")
        plt.ylabel("Composite Beam Area [km^2]")
        #Formula for the pointing range in the title is wrong
        plt.title(RX.name+"/"+TX.name+" Bistatic Beam Area")#: Pointing " +str(round(R[np.argmax(C)]))+" km, Az: " + str(TX.azimuth)+ " El: " +str(TX.elevation))
        plt.grid()
        xmax = np.max(R[np.where( (C > 0.))[0]])
        xmin = np.min(R[np.where( (C > 0.))[0]])
        plt.xlim(xmin,xmax)
        plt.show()
    stop = time()
    
    print("Execution: " + str(stop - start) + " s")
    
    return area

def areaValt(H, TX, RX ,samples=200, plot=True, thresh = 1./4, param = 'gain', width = 1, gain_pattern=airy, supress_output = False):
    """
    param = 'gain', 'RCS', or 'size'

    If param is 'gain', the wavelength will default to that of TX.
    If param is 'RCS' or 'size', the observer object must be passed 
    using the .as_a(sensor) command, i.e. TX.as('transmitter'). 
    """
    if not (param == 'RCS' or param == 'size' or param == 'gain'):
        print("Invalid parameter type...")
        return 0

    if param == 'RCS' or param == 'size':
        if TX.sensor == 'transmitter' and RX.sensor == 'receiver':
            TX = TX
            RX = RX
        elif RX.sensor == 'transmitter' and TX.sensor == 'receiver':
            TX = RX
            RX = TX
        else:
            print("Must have one transmitter and one receiver...")
            return 0

        if not (RX.receiver.band == TX.transmitter.band):
            print("The transmitter and receiver must be in the same band...")
            return 0

    else:
        TX = TX
        RX = RX

    RX.wavelength = TX.wavelength
    
    if TX.radius > RX.radius:
        ant0 = TX
        ant1 = RX
    else:
        ant1 = TX
        ant0 = RX

    C = []
    Hmin = H.min()
    delH = H.max()-H.min()
    start = time()
    for h in H:
        if param == 'gain':
            x,y,G = g_wgs_orbit_alt(h, ant0, ant1, samples=samples, plot=False, width=width, gain_pattern=gain_pattern)
            C.append(circum(x,y,G,thresh,param))
        elif param == 'RCS':
            x,y,G_R = g_wgs_orbit_alt(h, ant0, ant1, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,RCS_min(G_R,TX.transmitter,RX.receiver),thresh,param))
        elif param == 'size':
            x,y,G_R = g_wgs_orbit_alt(h, ant0, ant1, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,SEM(RCS_min(G_R,TX.transmitter,RX.receiver),TX.wavelength),thresh,param))
        if not supress_output:    
            print("Orbit Altitude: " + str(round(h)) + " km" + "    Percent: "+str(round(float(np.abs(Hmin - h))/delH*100)) + "%")    
        
    dH = np.abs(H[1] - H[0])
    C = np.array(C)

    area = C*dH
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        ax.bar(H,area,dH)
        plt.xlabel(TX.name + " Slant Range [km]")
        plt.ylabel("Composite Beam Area [km^2]")
        #Formula for the pointing range in the title is wrong
        plt.title(TX.name+"/"+RX.name+" Bistatic Beam Area")#: Pointing " +str(round(R[np.argmax(C)]))+" km, Az: " + str(TX.azimuth)+ " El: " +str(TX.elevation))
        plt.grid()
        xmax = np.max(H[np.where( (C > 0.))[0]])
        xmin = np.min(H[np.where( (C > 0.))[0]])
        plt.xlim(xmin,xmax)
        plt.show()
    stop = time()
    
    if not supress_output:
        print("Execution: " + str(stop - start) + " s")
    
    return area 

  
def areaValt_vector(H, TX, RX ,samples=200, plot=True, thresh = 1./4, param = 'gain', width = 1, gain_pattern=airy, supress_output = False):
    """
    param = 'gain', 'RCS', or 'size'

    If param is 'gain', the wavelength will default to that of TX.
    If param is 'RCS' or 'size', the observer object must be passed 
    using the .as_a(sensor) command, i.e. TX.as('transmitter'). 
    """
    if not (param == 'RCS' or param == 'size' or param == 'gain'):
        print("Invalid parameter type...")
        return 0

    if param == 'RCS' or param == 'size':
        if TX.sensor == 'transmitter' and RX.sensor == 'receiver':
            TX = TX
            RX = RX
        elif RX.sensor == 'transmitter' and TX.sensor == 'receiver':
            TX = RX
            RX = TX
        else:
            print("Must have one transmitter and one receiver...")
            return 0

        if not (RX.receiver.band == TX.transmitter.band):
            print("The transmitter and receiver must be in the same band...")
            return 0

    else:
        TX = TX
        RX = RX

    RX.wavelength = TX.wavelength
    
    if TX.radius > RX.radius:
        ant0 = TX
        ant1 = RX
    else:
        ant1 = TX
        ant0 = RX

    C = []
    Hmin = H.min()
    delH = H.max()-H.min()
    start = time()
    for h in H:
        if param == 'gain':
            x,y,G = g_wgs_orbit_alt_vector(h, ant0, ant1, samples=samples, plot=False, width=width, gain_pattern=gain_pattern)
            C.append(circum(x,y,G,thresh,param))
        elif param == 'RCS':
            x,y,G_R = g_wgs_orbit_alt_vector(h, ant0, ant1, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,RCS_min(G_R,TX.transmitter,RX.receiver),thresh,param))
        elif param == 'size':
            x,y,G_R = g_wgs_orbit_alt_vector(h, ant0, ant1, samples=samples, plot=False, width=width, ranges=True, gain_pattern=gain_pattern)
            C.append(circum(x,y,SEM(RCS_min(G_R,TX.transmitter,RX.receiver),TX.wavelength),thresh,param))
        if not supress_output:    
            print("Orbit Altitude: " + str(round(h)) + " km" + "    Percent: "+str(round(np.abs(Hmin - h)/delH*100)) + "%")
        
    dH = np.abs(H[1] - H[0])
    C = np.array(C)

    area = C*dH
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        ax.bar(H,area,dH)
        plt.xlabel(TX.name + " Slant Range [km]")
        plt.ylabel("Composite Beam Area [km^2]")
        #Formula for the pointing range in the title is wrong
        plt.title(TX.name+"/"+RX.name+" Bistatic Beam Area")#: Pointing " +str(round(R[np.argmax(C)]))+" km, Az: " + str(TX.azimuth)+ " El: " +str(TX.elevation))
        plt.grid()
        xmax = np.max(H[np.where( (C > 0.))[0]])
        xmin = np.min(H[np.where( (C > 0.))[0]])
        plt.xlim(xmin,xmax)
        plt.show()
    stop = time()
    if not supress_output:
        print("Execution: " + str(stop - start) + " s")
    
    return area 


def areaValt_bounded_error(start, stop, step, TX, RX, mu_a = .999, mu_c = .9996, gain_pattern=airy, supress_output = False, plot = True, debug = None):
    """

    """
    RX.wavelength = TX.wavelength
    
    if TX.radius > RX.radius:
        ant0 = TX
        ant1 = RX
    else:
        ant1 = TX
        ant0 = RX

    start_time = time()
        
    H_start = np.arange(start, stop, step, dtype = 'float')
    H_stop  = H_start + step
    
    areas = []
    if debug:
        with open(os.path.join(debug, "debug.csv"),"w") as DEBUG:
            DEBUG.write('h,step,h_sub,N,samples,C_sub\n')
    for h in H_start:
        N = optimal_steps(h, h + step, mu_a, mu_c)
        print(N)
        samples = optimal_samples(mu_c)
        #H_sub = np.arange(h,h+step, float(step)/N)
        H_sub = np.linspace(h,h+step, N, endpoint = False)

        C_sub = []
        for h_sub in H_sub:
            x,y,G = g_wgs_orbit_alt(h_sub, ant0, ant1, samples=samples, plot=False, gain_pattern=gain_pattern)            
            
            C_sub.append(circum(x,y,G,.25))
            if debug:
                
                with open(os.path.join(debug, "debug.csv"),"a") as DEBUG:
                    DEBUG.write('%s,%s,%s,%s,%s,%s\n' % (str(h),str(step),str(h_sub),str(N),str(samples),str(C_sub[-1])))
                area_debug(x,y,G, h_sub, debug)    
            
        
        areas.append(np.sum(np.array(C_sub))*float(step)/N)
            
        if not supress_output:    
            print("Orbit Altitude: " + str(round(h)) + " km")

        
    start_ranges0 = ant0.orbit_alt2slant(H_start)
    start_ranges1 = ant1.orbit_alt2slant(H_start)
    
    stop_ranges0 = ant0.orbit_alt2slant(H_stop)
    stop_ranges1 = ant1.orbit_alt2slant(H_stop)
    
    names = ['ALT1_KM', 'ALT2_KM', 'RNG1_KM_0', 'RNG2_KM_0','RNG1_KM_1', 'RNG2_KM_1', 'AREA_KM^2']


    areas = pd.DataFrame(np.array([H_start, H_stop, \
                   start_ranges0, stop_ranges0,\
                   start_ranges1, stop_ranges1, areas]).T, columns = names)

    if plot:
        plot_areas(areas, TX.name, RX.name)
    stop_time = time()
    if not supress_output:
        print("Execution: " + str(stop_time - start_time) + " s")
    return areas  

def areaValt_bounded_error_average(start, stop, step, TX, RX, mu_a = .999, mu_c = .9996, gain_pattern=airy, supress_output = False, plot = True, debug = None, samples = None):
    """

    """
    RX.wavelength = TX.wavelength
    
    if TX.radius > RX.radius:
        ant0 = TX
        ant1 = RX
    else:
        ant1 = TX
        ant0 = RX
    ant0 = TX
    ant1 = RX
    start_time = time()
        
    H_start = np.arange(start, stop, step, dtype = 'float')
    H_stop  = H_start + step
    
    areas = []
    if debug:
        with open(os.path.join(debug, "debug.csv"),"w") as DEBUG:
            DEBUG.write('h,step,h_sub,N,samples,C_sub\n')
    for h in H_start:
        N = optimal_steps(h, h + step, mu_a, mu_c)
        print(N)
        if samples == None:
            samples = optimal_samples(mu_c)
        #H_sub = np.arange(h,h+step, float(step)/N)
        H_sub = np.linspace(h,h+step, N+1, endpoint = True)

        C_sub = []
        for h_sub in H_sub:
            x,y,G = g_wgs_orbit_alt(h_sub, ant0, ant1, samples=samples, plot=False, gain_pattern=gain_pattern)            
            C_sub.append(circum(x,y,G,.25))
            if debug:
                with open(os.path.join(debug, "debug.csv"),"a") as DEBUG:
                    DEBUG.write('%s,%s,%s,%s,%s,%s\n' % (str(h),str(step),str(h_sub),str(N),str(samples),str(C_sub[-1])))
                area_debug(x,y,G, h_sub, debug)

                    
        for i in range(len(C_sub)-1):
            C_sub[i] = (C_sub[i] + C_sub[i+1]) / 2.  
            
        areas.append(np.sum(np.array(C_sub[:-1]))*float(step)/N)
            
        if not supress_output:    
            print("Orbit Altitude: " + str(round(h)) + " km")

        
    start_ranges0 = ant0.orbit_alt2slant(H_start)
    start_ranges1 = ant1.orbit_alt2slant(H_start)
    
    stop_ranges0 = ant0.orbit_alt2slant(H_stop)
    stop_ranges1 = ant1.orbit_alt2slant(H_stop)
    
    names = ['ALT1_KM', 'ALT2_KM', 'RNG1_KM_0', 'RNG2_KM_0','RNG1_KM_1', 'RNG2_KM_1', 'AREA_KM^2']


    areas = pd.DataFrame(np.array([H_start, H_stop, \
                   start_ranges0, stop_ranges0,\
                   start_ranges1, stop_ranges1, areas]).T, columns = names)

    if plot:
        plot_areas(areas, TX.name, RX.name)
    stop_time = time()
    if not supress_output:
        print("Execution: " + str(stop_time - start_time) + " s")
    return areas  



def BISTATIC_SURF_FLUX_BEAM_AREA(ALT1_KM, ALT2_KM, TX, RX, steps, samples, gain_pattern=airy, debug = None, width = 1, renormalized = False):
    """

    """
    RX.wavelength = TX.wavelength
    
    # If the transmitter is larger, travel along the transmitter
    # else, travel along the receiver
    if TX.radius > RX.radius:
        ant0 = TX
        ant1 = RX
    else:
        ant1 = TX
        ant0 = RX
    
    # Open Debug File        
    if debug:
        with open(os.path.join(debug, "debug.csv"),"w") as DEBUG:
            DEBUG.write('RNG1_KM,RNG2_KM,steps,samples,R,C_sub\n')
    
    # Get Range from Altitude
    RNG1_KM = ant0.orbit_alt2slant(ALT1_KM)
    RNG2_KM = ant0.orbit_alt2slant(ALT2_KM)
    
    # Calculate Correction used in Mark's Code
    R_KM = ant0.p/km + 0.5 * (RNG1_KM + RNG2_KM) * ant0.b
    RAD_KM = np.linalg.norm(R_KM)
    RNGDOTR =  ant0.b.dot(R_KM) / RAD_KM


    # Break up this integration into the number of steps to be performed
    # plus an extra step to perform the forward and backwards averaging
    R_sub = np.linspace(RNG1_KM, RNG2_KM, steps+1, endpoint = True)
    C_sub = []
    for R in R_sub:
        x,y,G = g_wgs(R, ant0, ant1, samples=samples, plot=False, gain_pattern=gain_pattern, width = width)
        if renormalized:
            G = G/np.max(G)            
        C_sub.append(circum(x,y,G,.25))
        
        if debug:
            with open(os.path.join(debug, "debug.csv"),"a") as DEBUG:
                DEBUG.write('%s,%s,%s,%s,%s,%s\n' % (str(RNG1_KM),str(RNG2_KM),str(steps),str(samples),str(R),str(C_sub[-1])))
            area_debug(x,y,G, ant0.slant2orbit_alt(R), debug)

    # Average the forward and backwards cylinder approximations
    for i in range(len(C_sub)-1):
        C_sub[i] = (C_sub[i] + C_sub[i+1]) / 2.  

        
    AREA_KM2 = np.sum(np.array(C_sub[:-1]))     \
               * float(RNG2_KM - RNG1_KM)/steps \
               * RNGDOTR
    
    return RNG1_KM, RNG2_KM, AREA_KM2 
 
    
def bistatic_observer_area(start, stop, step, TX, RX, steps, samples, gain_pattern = airy, debug = None, renormalized = False):
    
    start_alts = np.arange(start, stop, step, dtype = 'float')
    stop_alts  = start_alts + step
    start_ranges0 = []
    stop_ranges0  = []
    areas = []
    
    for alt in start_alts:
        print("Altitude: " + str(alt))
        rng1 , rng2, area = BISTATIC_SURF_FLUX_BEAM_AREA(alt, alt + step, TX, RX, steps, samples, gain_pattern=gain_pattern, debug = debug, renormalized = renormalized)
        start_ranges0.append(rng1)
        stop_ranges0.append(rng2)
        areas.append(area)
        
    start_ranges0 = np.array(start_ranges0)
    start_ranges1 = start_ranges0
    stop_ranges0  = np.array(stop_ranges0)
    stop_ranges1  = stop_ranges0

        
    names = ['ALT1_KM', 'ALT2_KM', 'RNG1_KM_0', 'RNG2_KM_0','RNG1_KM_1', 'RNG2_KM_1', 'AREA_KM^2']

    areas = pd.DataFrame(np.array([start_alts, stop_alts, \
                        start_ranges0, stop_ranges0,\
                        start_ranges1, stop_ranges1, areas]).T, \
                        columns = names)
    return areas


def g_wgs(R, ant0, ant1, gain_pattern=airy, samples=200, width=1, plot=True, ranges=False, along='tx', distance = 'range'):
    """
    Accepts Observer objects ant0 and ant1 and range R (km) measured from ant0 
    """
    
    if distance == 'orbit_altitude':
        R = ant0.orbit_alt2slant(R)

    R = R*km
    
    ############################################################
    #
    #   Generate points in the y-z Earth cantered plane for transformation
    #
    ############################################################
    #psi is chosen such that 3 beam nulls of the larger antenna are shown when width = 1
    if ant0.radius > ant1.radius:
        psi = np.arcsin(3.69*ant0.wavelength/(2*ant0.radius))
    else:
        psi = np.arcsin(3.69*ant1.wavelength/(2*ant1.radius))

    xmax = width*R*np.tan(psi)
    if samples == 'dynamic':
        samples = xmax/10.
    
    # print(samples)
    
    xprime = np.linspace(-xmax,xmax,samples)
    yprime = np.linspace(-xmax,xmax,samples)
    zprime = R*np.ones_like(xprime)
        
    ant0.theta = np.zeros([len(xprime),len(xprime)])
    ant1.theta = np.zeros([len(xprime),len(xprime)])
    
    ant0.costheta = np.zeros([len(xprime),len(xprime)])
    ant1.costheta = np.zeros([len(xprime),len(xprime)]) 
    
    ant0.ranges = np.zeros_like(ant0.theta)
    ant1.ranges = np.zeros_like(ant1.theta)
    
    for i in range(len(xprime)):
        for j in range(len(xprime)):
            
            if distance == 'orbit_altitude':
                rotation = obs.Rot_from_to(obs.z_hat, obs.unit(ant0.p + ant0.b*R))
            else:
                rotation = ant0.Rot
                
            x,y,z = np.dot(rotation, np.array([xprime[i],yprime[j],0])) + ant0.p + ant0.b*R
            r0 = np.array([x,y,z])
        
            ant0.r = r0 - ant0.p
            ant1.r = r0 - ant1.p            
            
            # ant0.theta[i,j] = np.arccos(np.dot(ant0.r,ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r)))
            # ant1.theta[i,j] = np.arccos(np.dot(ant1.r,ant1.b)/(np.linalg.norm(ant1.b)*np.linalg.norm(ant1.r)))
            ant0.theta[i,j] = np.arccos(np.clip(np.dot(ant0.r,ant0.b)/(np.linalg.norm(ant0.r)),-1,1))
            ant1.theta[i,j] = np.arccos(np.clip(np.dot(ant1.r,ant1.b)/(np.linalg.norm(ant1.r)),-1,1))
            # if np.isnan(ant0.theta[i,j]):
            #     print(np.linalg.norm(ant0.b))
            #     print(np.linalg.norm(ant0.r))
            #     print(np.dot(ant0.r,ant0.b))
            #     print((np.dot(ant0.r,ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r))))                
            #     print(np.arccos(np.dot(ant0.r,ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r))))
            #     print("Ant 0 Theta: " + str(ant0.r) + " " + str(ant0.b) + " " + str(ant0.theta[i,j]))
            # if np.isnan(ant1.theta[i,j]):
            #     print("Ant 1 Theta: " + str(ant1.r) + " " + str(ant1.b) + " " + str(ant1.theta[i,j]))

            
            ant0.costheta[i,j] = np.clip(ant0.r.dot(ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r)),-1,1)
            ant1.costheta[i,j] = np.clip(ant1.r.dot(ant1.b)/(np.linalg.norm(ant1.b)*np.linalg.norm(ant1.r)),-1,1)
            
            ant0.ranges[i,j] = np.sqrt(ant0.r.dot(ant0.r))
            ant1.ranges[i,j] = np.sqrt(ant1.r.dot(ant1.r))
    
    if along == 'tx':
        wavelength = ant0.wavelength
    elif along == 'rx':
        wavelength = ant1.wavelength

    if "_cos" in gain_pattern.__name__:
        ant0.G = gain_pattern(ant0.costheta,ant0.radius, lam = wavelength)
        ant1.G = gain_pattern(ant1.costheta,ant1.radius, lam = wavelength)
    else:
        ant0.G = gain_pattern(rad2deg(ant0.theta),ant0.radius, lam = wavelength)
        ant1.G = gain_pattern(rad2deg(ant1.theta),ant1.radius, lam = wavelength)
    prod = ant0.G*ant1.G
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        plt.suptitle(ant0.name+"/"+ant1.name+" Gain Pattern at "+str(round(R/km))+" km")
        gain_plot(xprime,yprime,prod)
    if ranges:
        return xprime,yprime,prod/(ant0.ranges*ant1.ranges)**2
    return xprime,yprime,prod     

def g_wgs_orbit_alt(alt, ant0, ant1, gain_pattern=airy, samples=200, width=1, plot=True, ranges=False, along='tx'):
    """
    Accepts Observer objects ant0 and ant1 and orbit altitude h (km) which then centers on the boresight of ant0
    """    
    
    R = ant0.orbit_alt2slant(alt)
    R = R*km
    
    ############################################################
    #
    #   Generate points in the y-z Earth cantered plane for transformation
    #
    ############################################################
    #psi is chosen such that 3 beam nulls of the larger antenna are shown when width = 1
    if ant0.radius > ant1.radius:
        xmax = ant0.orbit_alt2slant(alt)*km*ant0.wavelength/(2*ant0.radius)*width
    else:
        xmax = ant1.orbit_alt2slant(alt)*km*ant1.wavelength/(2*ant1.radius)*width

#    xmax = width*R*np.tan(psi)
    if samples == 'dynamic':
        samples = xmax/10.
        samples = np.max([10,samples])
        print(samples)
    
    xprime = np.linspace(-xmax,xmax,samples)
    yprime = np.linspace(-xmax,xmax,samples)
    zprime = R*np.ones_like(xprime)
        
    ant0.theta = np.zeros([len(xprime),len(xprime)])
    ant1.theta = np.zeros([len(xprime),len(xprime)])
    
    ant0.ranges = np.zeros_like(ant0.theta)
    ant1.ranges = np.zeros_like(ant1.theta)
    
    for i in range(len(xprime)):
        for j in range(len(xprime)):
            rotation = obs.Rot_from_to(obs.z_hat, obs.unit(ant0.p + ant0.b*R))
            x,y,z = np.dot(rotation, np.array([xprime[i],yprime[j],0])) + ant0.p + ant0.b*R
            r0 = np.array([x,y,z])
        
            ant0.r = r0 - ant0.p
            ant1.r = r0 - ant1.p   
            
            ant0.theta[i,j] = np.arccos(ant0.r.dot(ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r)))
            ant1.theta[i,j] = np.arccos(ant1.r.dot(ant1.b)/(np.linalg.norm(ant1.b)*np.linalg.norm(ant1.r)))
            
            ant0.ranges[i,j] = np.sqrt(ant0.r.dot(ant0.r))
            ant1.ranges[i,j] = np.sqrt(ant1.r.dot(ant1.r))
    
    if along == 'tx':
        wavelength = ant0.wavelength
    elif along == 'rx':
        wavelength = ant1.wavelength

    ant0.G = gain_pattern(rad2deg(ant0.theta),ant0.radius, lam = wavelength)
    ant1.G = gain_pattern(rad2deg(ant1.theta),ant1.radius, lam = wavelength)
    prod = ant0.G*ant1.G
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        plt.suptitle(ant0.name+"/"+ant1.name+" Gain Pattern at "+str(round(alt))+" km")
        gain_plot(xprime,yprime,prod)
    if ranges:
        return xprime,yprime,prod/(ant0.ranges*ant1.ranges)**2
    return xprime,yprime,prod

def g_wgs_orbit_alt_vector(alt, ant0, ant1, gain_pattern=airy, samples=200, width=1, plot=True, ranges=False, along='tx'):
    """
    Accepts Observer objects ant0 and ant1 and orbit altitude h (km) which then centers on the boresight of ant0
    """    
    # Range from ant0
    R = ant0.orbit_alt2slant(alt)
    R = R*km
    
    ############################################################
    #
    #   Generate points in the y-z Earth centered plane for transformation
    #
    ############################################################
    # xmax corresponds to the 3 dB beam width of the larger antenna 
    if ant0.radius > ant1.radius:
        xmax = ant0.orbit_alt2slant(alt)*km*ant0.wavelength/(2*ant0.radius)*width
    else:
        xmax = ant1.orbit_alt2slant(alt)*km*ant1.wavelength/(2*ant1.radius)*width

    
#    XYpairs = points(xmax, xmax, samples)
    
    xprime = np.linspace(-xmax,xmax,samples)
    yprime = np.linspace(-xmax,xmax,samples)
    
    XY = np.meshgrid(xprime, yprime)
    XYpairs = np.dstack(XY).reshape(-1, 2)
    
    rprime = np.vstack([XYpairs.T[0], XYpairs.T[1], np.zeros(samples**2)]).T
    
    rotation = obs.Rot_from_to(obs.z_hat, obs.unit(ant0.p + ant0.b*R))
    
#    Rz = np.array([[np.cos(phi), -np.sin(phi), 0],[np.sin(phi), np.cos(phi), 0],[0,0,1]])
#    rotation = rotation.dot(Rz)
    r = np.tensordot(rotation, rprime.T, axes = 1).T 

    ant0.r = r + ant0.b*R
    ant1.r = r + ant1.b*R
    
    ant0.theta = np.arccos(ant0.r.dot(ant0.b)/(np.linalg.norm(ant0.b)*np.linalg.norm(ant0.r, axis = 1)))
    ant1.theta = np.arccos(ant1.r.dot(ant1.b)/(np.linalg.norm(ant1.b)*np.linalg.norm(ant1.r, axis = 1)))
    ant0.ranges = np.linalg.norm(ant0.r,axis=1)
    ant1.ranges = np.linalg.norm(ant1.r,axis=1)
    
    if along == 'tx':
        wavelength = ant0.wavelength
    elif along == 'rx':
        wavelength = ant1.wavelength

    ant0.G = gain_pattern(rad2deg(ant0.theta),ant0.radius, lam = wavelength)
    ant1.G = gain_pattern(rad2deg(ant1.theta),ant1.radius, lam = wavelength)
    prod = ant0.G*ant1.G
    prod = prod.reshape(samples,samples)
    if plot:
        fig, ax = plt.subplots(figsize=(11,8.5))
        plt.suptitle(ant0.name+"/"+ant1.name+" Gain Pattern at "+str(round(alt))+" km")
        gain_plot(xprime, yprime, prod)
    if ranges:
        return xprime,yprime,prod/(ant0.ranges*ant1.ranges)**2

    return xprime, yprime, prod    


def circum(x,y,G,thresh=1./4, param = 'gain'):
    if param == 'gain':
        i,j = np.where( G >= thresh)
    elif param == 'RCS' or param == 'size':
        i,j = np.where( G <= thresh)
    if len(i) < 10:
        return 0
    foox = np.array([x[i]])    
    fooy = np.array([y[j]])
    points = np.concatenate([foox,fooy]).T
    try:
        hull = ConvexHull(points)
    except:
        return 0
    #Area of a 2D hull is the perimeter
    return hull.area/km

def circum_points(x,y,G,thresh=1./4, param = 'gain'):
    if param == 'gain':
        i,j = np.where( G >= thresh)
    elif param == 'RCS' or param == 'size':
        i,j = np.where( G <= thresh)
    if len(i) < 10:
        return 0
    foox = np.array([x[i]])    
    fooy = np.array([y[j]])
    points = np.concatenate([foox,fooy]).T
    try:
        hull = ConvexHull(points)
    except:
        return 0
    #Area of a 2D hull is the perimeter
    return hull

#def RCS_min(G_over_R, TX, RX):
#    RCS = RX.SNR_min*k_B*RX.T_sys*RX.BW/(TX.power*TX.pulse_length*TX.BW*TX.wavelength**2)/(G_over_R*RX.peak_gain*TX.peak_gain)*RX.loss
#    return RCS

def RCS_min(G_over_R, TX, RX):
    RCS = RX.SNR_min*k_B*RX.T_sys*RX.BW/(TX.power*TX.pulse_length*TX.wavelength**2)/(G_over_R*RX.peak_gain*TX.peak_gain)*RX.loss
    return RCS

def power_min(G_over_R, TX, RX, size = .005):
    RCS = SEM_inv(size, TX.wavelength)
    power = RX.SNR_min*k_B*RX.T_sys*RX.BW/(RCS*TX.pulse_length*TX.BW*TX.wavelength**2)/(G_over_R*RX.peak_gain*TX.peak_gain)*RX.loss
    return power     
