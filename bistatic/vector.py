# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 19:23:49 2020

@author: Melissa Ward
"""

import numpy as np

I = np.array([[1.,0.,0.],[0.,1.,0.],[0.,0.,1.]])
x_hat = np.array([1.,0.,0.])
y_hat = np.array([0.,1.,0.])
z_hat = np.array([0.,0.,1.])

def vec2nv(vector):
    """Returns a vector in the nvector format"""
    return np.array([[vector[0]],[vector[1]],[vector[2]]])

def nv2vec(vector):
    """Returns a regular vector from an nvector"""
    return vector.T[0,:]

def unit(vector):
    """Returns a unit vector of the input vector"""
    return vector/np.linalg.norm(vector)

def angle_between(a, b):
    """Returns the angle between two vectors a and b"""
    return np.arccos(a.dot(b)/(np.linalg.norm(a)*np.linalg.norm(b)))

def Rot_from_to(a,b):
    """Returns a rotation matrix to rotate vector a to vector b"""
    v = np.cross(a,b)
    v_x = np.array([[0.,-v[2],v[1]],[v[2],0.,-v[0]],[-v[1],v[0],0.]])
    Rot = I + v_x + v_x.dot(v_x)*(1-a.dot(b))/(v.dot(v.T))
    #print "Rotation Matrix: "
    #print Rot
    return Rot
