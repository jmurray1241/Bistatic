# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 19:38:28 2020

@author: Melissa Ward
"""

from numpy import pi

#TODO: Use ODPO R_e
R_e = 6378136.3 # ITRF 93
f = 298.257# Earth Flattening

k_B = 1.38064852*10**-23 # boltzmann's constant
A_e_per_G_k = 2761. # [m^2]
speed_of_light = 2.99792458e08 # [m/s]
ns = 10.**-9 # [s/ns]
us = 10.**-6 # [s/us]
ms = 10.**-3 # [s/ms]
s  = 1.
GHz = 10.**9 # [Hz/GHz]
MHz = 10.**6 # [Hz/MHz]
kHz = 10.**3 # [Hz/kHz]
Hz  = 1.
MW = 10.**6 # [W/MW]
kW = 10.**3 # [W/kW]
W  = 1.
km = 10.**3 # [m/km]
m  = 1.
cm = 10.**-2 # [m/cm]
mm = 10.**-3 # [m/mm]
deg = pi/180 # [deg/rad]
arcmin = deg/60 # [arcmin/rad]
arcsec = arcmin/60 # [arcsec/rad]