# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 18:43:03 2020

@author: jimurray
"""

import numpy as np
#import sobol_seq

def lin_grid(xmax, ymax, samples):
    xprime = np.linspace(-xmax,xmax,samples)
    yprime = np.linspace(-ymax,ymax,samples)
    
    XY = np.meshgrid(xprime, yprime)
    XYpairs = np.dstack(XY).reshape(-1, 2)
    

    return XYpairs

def sobol(xmax, ymax, samples):
    xprime = 2*xmax*(sobol_seq.i4_sobol_generate(1, samples).T[0]-.5)
    yprime = 2*ymax*(sobol_seq.i4_sobol_generate(1, samples).T[0]-.5)
    
    XY = np.meshgrid(xprime, yprime)
    XYpairs = np.dstack(XY).reshape(-1, 2)
#    XYpairs = 2*xmax*(sobol_seq.i4_sobol_generate(2, samples**2)-.5)
#    XYpairs = np.array(sorted(XYpairs, key=lambda x: (x[0], -x[1])))
    return XYpairs

def uniform(xmax, ymax, samples):
    
    xprime = 2*xmax*(np.random.rand(samples)-.5)
    yprime = 2*ymax*(np.random.rand(samples)-.5)

    
    XY = np.meshgrid(xprime, yprime)
    XYpairs = np.dstack(XY).reshape(-1, 2)
    
#    XYpairs = 2*xmax*(np.random.rand(samples**2,2)-.5)
#    XYpairs = np.array(sorted(XYpairs, key=lambda x: (x[0], -x[1])))

    return XYpairs