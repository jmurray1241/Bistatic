# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 18:40:28 2021

@author: jimurray
"""



import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from copy import deepcopy

#import bistatic.bistatic as bs
#import bistatic.observer2 as obs
#import bistatic.PD as PD
#from bistatic.conversion import G_k2G, dB2lin, lin2dB, f2lam, lam2f,\
#                                MW, kW, W,\
#                                ns, us, ms, s,\
#                                km, m, cm, mm,\
#                                GHz, MHz, kHz, Hz,\
#                                MW, kW, W,\
#                                deg, arcsec, arcmin
from . import bistatic as bs
from . import observer2 as obs
from . import PD as PD
from .conversion import lin2dB, lam2f
from .constants import MW, kW, W,\
                      ns, us, ms, s,\
                      km, m, cm, mm,\
                      GHz, MHz, kHz, Hz,\
                      MW, kW, W,\
                      deg, arcsec, arcmin
                                
from scipy.interpolate import PchipInterpolator as PCHIP
from scipy.stats import ncx2

fiducial_sizes_m = np.array([10.**-5., 10.**-4, 10.**-3, 10.**-2, 10.**-1, 1.])
sqkm2sqm = 1.0e6
hoursPerYear = 8760.

#%%

TX = obs.HUSIR
RX = obs.HUSIR
RX.receiver.SNR_min = 10**(5.65/10)

intersection_altitude_km = 1000. # km
intersection_range_km = TX.orbit_alt2slant(intersection_altitude_km)
TX_azimuth   = 90. # deg
TX_elevation = 75. # deg



DEBUG = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\DEBUG"
flux_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\data\\ORDEM3_1\\HUSIR CY2018\\FLUX_TEL.OUT"


def standard_altitude_comparison(intersection_range_km, TX, RX, gain_pattern = bs.airy):
    # Standard altitude comparison
    # RX_temp = deepcopy(RX)
    # bs.point_ant2ant(intersection_range_km, TX, RX_temp)
    
    RCS = bs.RCSVrange([intersection_range_km], TX, RX, gain_pattern = gain_pattern, plot=False, atmospheric_loss = True)[0]
    size = bs.sizeVrange([intersection_range_km], TX, RX, gain_pattern = gain_pattern, plot=False, atmospheric_loss = True)[0]
    
    print("Minimum Detectable RCS: " + str(RCS) + " m^2 (" + str(10*np.log10(RCS))+" dBsm)")
    print("Minimum Detectable Size: " + str(size*1000) + " mm")
    return RCS, size


def calculate_count_rate_flux_table(flux_file, TX, RX, gain_pattern = bs.airy, area_file = None, debug_path = None, verbose = False):


    print("Reading Flux Table...")
    flux_data_m2_yr = np.genfromtxt(flux_file, delimiter="   ", skip_header=12)
    
    if area_file != None:
        print("Loading Areas from File...")
        areas = pd.read_excel(area_file)
            
    start_alts_km, indicies = np.unique(flux_data_m2_yr.T[0], return_index = True)
    alt_step_km = start_alts_km[1] - start_alts_km[0]
    
    start_alts_km = start_alts_km[:-1]

    # For each altitude bin calculate the following
    # - Area in bin (bistatic area)
    # - Minimum detectable size at mid-bin (sizeVrange)
    # - Flux at the minimum size (PCHIP)
    # - Count rate of min size and higher (flux*area)
    columns = ['Start_Altitude (km)', 'Stop_Altitude (km)', 'Area (km^2)', \
                               ">10um Flux (#/m^2/yr)", ">100um Flux (#/m^2/yr)", \
                               ">1mm Flux (#/m^2/yr)", ">1cm Flux (#/m^2/yr)", \
                               ">10cm Flux (#/m^2/yr)",">1m Flux (#/m^2/yr)", \
                               "Mid-Bin Limiting Size (mm)", \
                               "Flux to Limiting Size (#/m^2/yr)", \
                               "Count Rate to Limiting Size (#/hr)"]
    
    df = pd.DataFrame()
    count = np.zeros(1000, dtype='float')            
    for i, start_alt_km in enumerate(start_alts_km):
        print("start_alt_km: " + str(start_alt_km))
        stop_alt_km = start_alt_km + alt_step_km
        
        if area_file != None:
            area_km2 = areas.iloc[i]['Area (km^2)']
            print("    Area read from file...")
        else:
            #Calculate Area
            print("    Calculating Area...")
            # Run a quick check to see if an altitude in this bin is in the main beam
            # If not save time by just assigning zero to area
            main_beam_check = (bs.gainVrange(np.linspace(TX.orbit_alt2slant(start_alt_km), TX.orbit_alt2slant(stop_alt_km)), TX, RX, plot = False, supress_output=True) > 0.1).any()
#            main_beam_check = (bs.gainVrange(np.linspace(TX.orbit_alt2slant(start_alt_km), TX.orbit_alt2slant(stop_alt_km)), TX, RX, plot = False, supress_output=True) > 0.25).any()
            if main_beam_check:
                print("        Altitude in main beam, calculating area")
                if TX.name == RX.name: # If monostatic, do each altitude bin in one step
                    print("            Monostatic run detected, steps set to 1")
                    steps = 1
                else:
                    print("            Bistatic run detected, steps set to 25")
                    steps = 25
                RNG1_KM, RNG2_KM, area_km2 = bs.BISTATIC_SURF_FLUX_BEAM_AREA(start_alt_km, stop_alt_km, TX, RX, gain_pattern = gain_pattern, steps = steps, samples = 250, width = .25, debug = debug_path)
            else:
                print("        Altitude not in main beam, area set to zero")
                area_km2 = 0.
            print("    area_km2: " + str(area_km2))
    
        #Assign fluxes to variables and instantiate PCHIP interpolator
        print("    Assigning fluxes to variables and instantiating PCHIP interpolator...")
        
        flux_10um_m2_yr, flux_100um_m2_yr, flux_1mm_m2_yr, \
        flux_1cm_m2_yr,  flux_10cm_m2_yr,  flux_1m_m2_yr = flux_data_m2_yr[2*i-1][2:]
        
        PCHIP_interpolator = PCHIP(np.log10(fiducial_sizes_m), np.log10(flux_data_m2_yr[2*i-1][2:]))
        
        
        #Calculate Minimum Size at mid-Bin
        print("    Calculating limiting size at mid-bin...")
        mid_bin_alt_km = start_alt_km + alt_step_km/2
        min_size_mm = bs.sizeVrange([TX.orbit_alt2slant(mid_bin_alt_km)], TX, RX, gain_pattern = gain_pattern, plot = False, supress_output=True, atmospheric_loss = True)[0]*1000
        
        
        interp_sizes = np.logspace(-5, 0, 1000)
        interp_flux = 10**PCHIP_interpolator(np.log10(interp_sizes))        
        interp_RCS = bs.SEM_inv(interp_sizes, TX.wavelength)
        differential_flux = -(np.gradient(interp_flux))
        
        # if verbose:
        #     print(differential_flux)
        #     input("Press any key to continue")
        Rt = TX.orbit_alt2slant(mid_bin_alt_km)*1000.
        Rr = RX.orbit_alt2slant(mid_bin_alt_km)*1000.
        # print(mid_bin_alt_km)
        power_pattern_product = bs.gainVrange([Rt/1000.], TX, RX, gain_pattern = gain_pattern, plot=False, atmospheric_loss = True, samples = 75)[0]
        #power_pattern_product = bs.gainVrange([Rt/1000.], TX, RX, gain_pattern = gain_pattern, plot=False, atmospheric_loss = False, samples = 75)[0]
        
        if verbose:
            print(power_pattern_product)
            # input("Press any key to continue")

        
        Gt = TX.transmitter.peak_gain
        Gr = RX.receiver.peak_gain
        SNR = PD.SNR(interp_RCS, TX.transmitter.power, Gt, Gr, \
                     TX.transmitter.wavelength, Rt, Rr, RX.receiver.T_sys, TX.transmitter.BW, \
                     RX.receiver.loss, power_pattern_product)#, TX.transmitter.pulse_length)
        # if verbose:
        #     print(SNR)
        #     input("Press any key to continue")

        #probability_of_detection = PD.marcumsq(10*np.log10(SNR), 10*np.log10(RX.receiver.SNR_min))
        #probability_of_detection = PD.pd(10*np.log10(SNR), 10*np.log10(RX.receiver.SNR_min))
        probability_of_detection = ncx2.cdf(SNR, df = 4, nc = RX.receiver.SNR_min)
        #probability_of_detection = roc_pd(4.5*10**-5, snr = SNR, N = 1, stype = 'Real')
        # if verbose:
        #     print(probability_of_detection)
        #     input("Press any key to continue")

        
        flux_w_PD = differential_flux*probability_of_detection
        
        interp_flux_trap = np.zeros_like(interp_flux)
        for j in range(len(interp_flux)):
            interp_flux_trap[j] = np.trapz(flux_w_PD[j:]) + interp_flux[-1]

        # if verbose:
        #     print(interp_flux_trap)
        #     input("Press any key to continue")      
    
        PCHIP_interpolator_w_PD = PCHIP(np.log10(interp_sizes), np.log10(interp_flux_trap))
        
        #Calculate flux to limiting size
        print("    Calculating limiting flux...")
        #flux_to_limiting_size_m2_yr = 10**PCHIP_interpolator(np.log10(min_size_mm/1000))
        flux_to_limiting_size_m2_yr = 10**PCHIP_interpolator_w_PD(np.log10(min_size_mm/1000))
        
        if i >39:
            count += area_km2*10**6/8760.*interp_flux_trap
    
        if debug_path != None:
            fig, ax = plt.subplots()
            ax.loglog(fiducial_sizes_m, flux_data_m2_yr[2*i-1][2:], 'ko')
            ax.loglog(interp_sizes, interp_flux, label = 'ORDEM')
            ax.loglog(interp_sizes, interp_flux_trap, label = 'w/ Detection Probability')
            ax.loglog(min_size_mm/1000, flux_to_limiting_size_m2_yr, 'r*')
            plt.ylim(10**-10,10**4)
            plt.xlabel("Size (m)")
            plt.ylabel(r"Flux (#/m$^2$/yr)")
            plt.grid(which = 'both')
            plt.title("Altitude Bin: " + str(start_alt_km) + " km - " + str(stop_alt_km) + " km")
            plt.savefig(os.path.join(debug_path, "flux interpolations " + str(start_alt_km) + "km_" + str(stop_alt_km) + "km.png"))
    
        #Calculate count rate to limiting size
        print("    Calculating limiting count rate...")
        count_rate_to_limiting_size_hr = area_km2*sqkm2sqm * flux_to_limiting_size_m2_yr/hoursPerYear
        
        print("    Appending Row to table...")
        row = np.array([[start_alt_km, stop_alt_km, area_km2, flux_10um_m2_yr, flux_100um_m2_yr, \
               flux_1mm_m2_yr, flux_1cm_m2_yr,  flux_10cm_m2_yr, flux_1m_m2_yr, min_size_mm,\
               flux_to_limiting_size_m2_yr, count_rate_to_limiting_size_hr]])
        
        df = pd.concat([df,pd.DataFrame(data = row, columns = columns)])
        
    return df, interp_sizes, count

def standard_flux_calculations(intersection_altitude_km, TX, RX, flux_file, output_directory, gain_pattern = bs.airy, area_file = None, verbose = False):
    
    intersection_range_km = TX.orbit_alt2slant(intersection_altitude_km)

    RCS, size =  standard_altitude_comparison(intersection_range_km, TX, RX)
    
    
    with open(os.path.join(output_directory, "results.txt"),'w') as outfile:
        outfile.write("Transmitter: " + TX.name + '\n')
        outfile.write("Receiver: " + RX.name + '\n')
        outfile.write("Band: " + TX.transmitter.band + '\n')
        outfile.write("Center Frequency: " + str(lam2f(TX.wavelength)/MHz) + " MHz\n")
        outfile.write("TX Pulse Length: " + str(TX.transmitter.pulse_length/us) + " microsec\n")
        outfile.write("TX Power: " + str(TX.transmitter.power/kW) + " kW\n")
        outfile.write("TX Bandwidth: " + str(TX.transmitter.BW/kHz) + " kHz\n")
        outfile.write("TX Gain: " + str(10*np.log10(TX.transmitter.peak_gain)) + " dB\n")
        outfile.write("RX Bandwidth: " + str(RX.receiver.BW/1000000.) + " MHz\n")
        outfile.write("RX Gain: " + str(lin2dB(RX.receiver.peak_gain)) + " dB\n")
        outfile.write("RX Temperature: " + str(RX.receiver.T_sys) + " K\n")
        outfile.write("RX Minimum SNR: " + str(lin2dB(RX.receiver.SNR_min)) + " dB\n\n")
        outfile.write("Minimum RCS @ "+str(intersection_altitude_km)+"km: " + str(lin2dB(RCS))+ " dBsm\n")
        outfile.write("Minimum Size @ "+str(intersection_altitude_km)+"km: " + str(size/mm)+ " mm\n")
    
    
    
    
    
    
    flux_table, interp_sizes, cumulative_count = calculate_count_rate_flux_table( \
                                                 flux_file, TX, RX, gain_pattern = gain_pattern, \
                                                 area_file = area_file, debug_path = output_directory,\
                                                 verbose = verbose)


    flux_table.to_excel(os.path.join(output_directory, "flux_table.xlsx"))
    
    with open(os.path.join(output_directory, "count_rate_v_size.csv"),'w') as out:
        out.write("Size (m),Count Rate (#/hr)\n")
        for i in range(len(cumulative_count)):
            out.write(str(interp_sizes[i])+","+str(cumulative_count[i])+"\n")

    fig, ax = plt.subplots()
    
    plt.loglog(interp_sizes, cumulative_count, 'k--', label = "Count Rate : " + str(max(cumulative_count)) + "#/hr" )
    plt.xlabel("Size (m)")
    plt.ylabel("Cumulative Count Rate (#/hour)")
    plt.grid(which = 'both')
    plt.legend()
    plt.savefig(os.path.join(output_directory, "cumulative_count_rate.png"))
    
    with open(os.path.join(output_directory, "results.txt"),'a') as outfile:
        outfile.write("Total Count Rate : " + str(max(cumulative_count)) + " per hour" )


    return interp_sizes, cumulative_count



import numpy as np
from scipy.special import erfc, erfcinv, gammainc


def log_factorial(n):
    """
    Compute the factorial of 'n' using logarithms to avoid overflow

    :param int n:
        Integer number

    :return:
        log(n!)
    :rtype: float
    """

    n = n+9.0
    n2 = n**2
    return (n-1)*np.log(n)-n+np.log(np.sqrt(2*np.pi*n)) + \
        ((1-(1/30+(1/105)/n2)/n2)/12)/n - \
        np.log((n-1)*(n-2)*(n-3)*(n-4)*(n-5)*(n-6)*(n-7)*(n-8))


def threshold(pfa, N):
    """
    Threshold ratio

    :param float pfa:
        Probability of false alarm
    :param int N:
        Number of pulses for integration

    :return:
        Threshod ratio
    :rtype: float

    *Reference*

    Mahafza, Bassem R. Radar systems analysis and design using MATLAB.
    Chapman and Hall/CRC, 2005.
    """

    eps = 0.00000001
    delta = 10000.
    nfa = N * np.log(2)/pfa
    sqrtpfa = np.sqrt(-np.log10(pfa))
    sqrtnp = np.sqrt(N)
    thred0 = N-sqrtnp+2.3*sqrtpfa*(sqrtpfa+sqrtnp-1.0)
    thred = thred0
    while (delta >= thred0):
        igf = gammainc(N, thred0)
        deno = np.exp((N-1) * np.log(thred0+eps) - thred0 - log_factorial(N-1))
        thred = thred0+((0.5**(N/nfa)-igf)/(deno+eps))

        delta = np.abs(thred-thred0)*10000.0
        thred0 = thred

    return thred


def roc_pd(pfa, snr, N=1, stype='Coherent'):
    """
    Calculate probability of detection (Pd) in receiver operating
    characteristic (ROC)

    :param pfa:
        Probability of false alarm (Pfa)
    :type pfa: float or numpy.1darray
    :param snr:
        Signal to noise ratio in decibel (dB)
    :type snr: float or numpy.1darray
    :param int N:
        Number of pulses for integration (default is 1)
    :param str stype:
        Signal type (default is ``Coherent``)

        - ``Coherent``: Non-fluctuating coherent
        - ``Real``: Non-fluctuating real signal
        - ``Swerling 0``: Non-coherent Swerling 0, Non-fluctuating non-coherent
        - ``Swerling 1``: Non-coherent Swerling 1
        - ``Swerling 2``: Non-coherent Swerling 2
        - ``Swerling 3``: Non-coherent Swerling 3
        - ``Swerling 4``: Non-coherent Swerling 4
        - ``Swerling 5``: Non-coherent Swerling 5, Non-fluctuating non-coherent

    :return: probability of detection (Pd).
        if both ``pfa`` and ``snr`` are floats, ``pd`` is a float
        if ``pfa`` or ``snr`` is a 1-D array, ``pd`` is a 1-D array
        if both ``pfa`` and ``snr`` are 1-D arrays, ``pd`` is a 2-D array
    :rtype: float or 1-D array or 2-D array

    *Reference*

    Mahafza, Bassem R. Radar systems analysis and design using MATLAB.
    Chapman and Hall/CRC, 2005.
    """
    snr_db = snr
    snr = 10.0**(snr_db/10.)

    size_pfa = np.size(pfa)
    size_snr = np.size(snr)

    pd = np.zeros((size_pfa, size_snr))

    it_pfa = np.nditer(pfa, flags=['f_index'])
    while not it_pfa.finished:
        thred = threshold(it_pfa[0], N)

        if stype == 'Swerling 1':
            if (N == 1):
                pd[it_pfa.index, :] = np.exp(-thred/(1+snr))
            else:
                temp_sw1 = 1+1/(N*snr)
                igf1 = gammainc(N-1, thred)
                igf2 = gammainc(N-1, thred/temp_sw1)
                pd[it_pfa.index, :] = 1-igf1 + \
                    (temp_sw1**(N-1))*igf2*np.exp(-thred/(1+N*snr))
        elif stype == 'Swerling 2':
            if (N <= 50):
                pd[it_pfa.index, :] = 1-gammainc(N, (thred/(1+snr)))
            else:
                V = (thred-N*(snr+1))/(np.sqrt(N)*(snr+1))
                Vsqr = V**2
                val1 = np.exp(-Vsqr/2)/np.sqrt(2*np.pi)
                val2 = -1/np.sqrt(9*N)*(Vsqr-1)+0.25*V * \
                    (3-Vsqr)/N-V*(V**4-10*Vsqr+15)/(18*N)
                pd[it_pfa.index, :] = 0.5*erfc(V/np.sqrt(2))-val1*val2
        elif stype == 'Swerling 3':
            temp_1 = thred/(1+0.5*N*snr)
            ko = np.exp(-temp_1)*(1+2/(N*snr))**(N-2) * \
                (1+temp_1-2*(N-2)/(N*snr))
            if (N <= 2):
                pd[it_pfa.index, :] = ko
            else:
                if log_factorial(N-2.) <= 500:
                    temp4 = thred**(N-1)*np.exp(-thred) / \
                        (temp_1*np.exp(log_factorial(N-2.)))
                else:
                    temp4 = 0
                pd[it_pfa.index, :] = temp4+1-gammainc(N-1, thred) + \
                    ko*gammainc(N-1, thred/(1+2/(N*snr)))
            if np.size(pd[it_pfa.index, :]) == 1:
                if pd[it_pfa.index, :] > 1:
                    pd[it_pfa.index, :] = 1
            else:
                neg_idx = np.where(pd[it_pfa.index, :] > 1)
                pd[it_pfa.index, :][neg_idx[0]] = 1
        elif stype == 'Swerling 4':
            beta = 1+snr/2
            if (N >= 50):
                omegabar = np.sqrt(N*(2*beta**2-1))
                c3 = (2*beta**3-1)/(3*(2*beta**2-1)*omegabar)
                c4 = (2*beta**4-1)/(4*N*(2*beta**2-1)**2)
                c6 = c3**2/2
                V = (thred-N*(1+snr))/omegabar
                Vsqr = V**2
                val1 = np.exp(-Vsqr/2)/np.sqrt(2*np.pi)
                val2 = c3*(V**2-1)+c4*V*(3-V**2)-c6*V*(V**4-10*V**2+15)
                pd[it_pfa.index, :] = 0.5*erfc(V/np.sqrt(2))-val1*val2
            else:
                gamma0 = gammainc(N, thred/beta)
                a1 = (thred/beta)**N / \
                    (np.exp(log_factorial(N))*np.exp(thred/beta))
                sum_var = gamma0
                for i in range(1, N+1, 1):
                    temp_sw4 = 1
                    if (i == 1):
                        ai = a1
                    else:
                        ai = (thred/beta)*a1/(N+i-1)
                    a1 = ai
                    gammai = gamma0-ai
                    gamma0 = gammai
                    a1 = ai

                    for ii in range(1, i+1, 1):
                        temp_sw4 = temp_sw4*(N+1-ii)

                    term = (snr/2)**i*gammai*temp_sw4/np.exp(log_factorial(i))
                    sum_var = sum_var+term
                pd[it_pfa.index, :] = 1-sum_var/beta**N
            if np.size(pd[it_pfa.index, :]) == 1:
                if pd[it_pfa.index, :] < 0:
                    pd[it_pfa.index, :] = 0
            else:
                neg_idx = np.where(pd[it_pfa.index, :] < 0)
                pd[it_pfa.index, :][neg_idx[0]] = 0
        elif stype == 'Swerling 5' or stype == 'Swerling 0':
            temp_1 = 2*snr+1
            omegabar = np.sqrt(N*temp_1)
            c3 = -(snr+1/3)/(np.sqrt(N)*temp_1**1.5)
            c4 = (snr+0.25)/(N*temp_1**2.)
            c6 = c3*c3/2
            V = (thred-N*(1+snr))/omegabar
            Vsqr = V**2
            val1 = np.exp(-Vsqr/2)/np.sqrt(2*np.pi)
            val2 = c3*(V**2-1)+c4*V*(3-V**2) - \
                c6*V*(V**4-10*V**2+15)
            q = 0.5*erfc(V/np.sqrt(2))
            pd[it_pfa.index, :] = q-val1*val2
        elif stype == 'Coherent':
            snr = snr*N
            pd[it_pfa.index, :] = erfc(erfcinv(2*it_pfa[0])-np.sqrt(snr))/2
        elif stype == 'Real':
            snr = snr*N/2
            pd[it_pfa.index, :] = erfc(erfcinv(2*it_pfa[0])-np.sqrt(snr))/2
        else:
            return None

        it_pfa.iternext()

    if size_pfa == 1 and size_snr == 1:
        return pd[0, 0]
    elif size_pfa == 1 and size_snr > 1:
        return pd[0, :]
    elif size_pfa > 1 and size_snr == 1:
        return pd[:, 0]
    else:
        return pd


def roc_snr(pfa, pd, N=1, stype='Coherent'):
    """
    Calculate the minimal SNR for certain probability of
    detection (Pd) and probability of false alarm (Pfa) in
    receiver operating characteristic (ROC) with Secant method

    :param pfa:
        Probability of false alarm (Pfa)
    :type pfa: float or numpy.1darray
    :param pd:
         Probability of detection (Pd)
    :type pd: float or numpy.1darray
    :param int N:
        Number of pulses for integration (default is 1)
    :param str stype:
        Signal type (default is ``Coherent``)

        - ``Coherent`` : Non-fluctuating coherent
        - ``Real`` : Non-fluctuating real signal
        - ``Swerling 0`` : Non-fluctuating non-coherent
        - ``Swerling 1`` : Non-coherent Swerling 1
        - ``Swerling 2`` : Non-coherent Swerling 2
        - ``Swerling 3`` : Non-coherent Swerling 3
        - ``Swerling 4`` : Non-coherent Swerling 4
        - ``Swerling 5`` : Same as ``Swerling 0``

    :return: Minimal signal to noise ratio in decibel (dB)
        if both ``pfa`` and ``pd`` are floats, ``SNR`` is a float
        if ``pfa`` or ``pd`` is a 1-D array, ``SNR`` is a 1-D array
        if both ``pfa`` and ``pd`` are 1-D arrays, ``SNR`` is a 2-D array
    :rtype: float or 1-D array or 2-D array

    *Reference*

    Secant method:

        The x intercept of the secant line on the the Nth interval

        .. math:: m_n = a_n - f(a_n)*(b_n - a_n)/(f(b_n) - f(a_n))

        The initial interval [a_0,b_0] is given by [a,b]. If f(m_n) == 0
        for some intercept m_n then the function returns this solution.
        If all signs of values f(a_n), f(b_n) and f(m_n) are the same at any
        iterations, the secant method fails and return None.
    """

    def fun(pfa, pd, snr):
        return roc_pd(pfa, snr, N, stype)-pd

    max_iter = 1000
    snra = 40
    snrb = -30

    size_pd = np.size(pd)
    size_pfa = np.size(pfa)
    snr = np.zeros((size_pfa, size_pd))

    it_pfa = np.nditer(pfa, flags=['f_index'])
    while not it_pfa.finished:

        it_pd = np.nditer(pd, flags=['f_index'])
        while not it_pd.finished:
            if fun(it_pfa[0], it_pd[0], snra) *\
                    fun(it_pfa[0], it_pd[0], snrb) >= 0:
                # print("Initializing Secant method fails.")
                return None
            a_n = snra
            b_n = snrb
            for n in range(1, max_iter+1):
                m_n = a_n-fun(it_pfa[0], it_pd[0], a_n)*(b_n-a_n) / \
                    (fun(it_pfa[0], it_pd[0], b_n) -
                     fun(it_pfa[0], it_pd[0], a_n))
                f_m_n = fun(it_pfa[0], it_pd[0], m_n)
                if f_m_n == 0:
                    # print("Found exact solution.")
                    snr[it_pfa.index, it_pd.index] = m_n
                    break
                elif np.abs(f_m_n) < 0.00001:
                    # print("Reach threshold.")
                    snr[it_pfa.index, it_pd.index] = m_n
                    break
                elif fun(it_pfa[0], it_pd[0], a_n)*f_m_n < 0:
                    a_n = a_n
                    b_n = m_n
                elif fun(it_pfa[0], it_pd[0], b_n)*f_m_n < 0:
                    a_n = m_n
                    b_n = b_n
                else:
                    # print("Secant method fails.")
                    # return None
                    snr[it_pfa.index, it_pd.index] = float('nan')
                    break
            # return a_n-fun(a_n)*(b_n-a_n)/(fun(b_n)-fun(a_n))
            snr[it_pfa.index, it_pd.index] = a_n - \
                fun(it_pfa[0], it_pd[0], a_n)*(b_n-a_n) / \
                (fun(it_pfa[0], it_pd[0], b_n)-fun(it_pfa[0], it_pd[0], a_n))
            it_pd.iternext()
        it_pfa.iternext()

    if size_pfa == 1 and size_pd == 1:
        return snr[0, 0]
    elif size_pfa == 1 and size_pd > 1:
        return snr[0, :]
    elif size_pfa > 1 and size_pd == 1:
        return snr[:, 0]
    else:
        return snr



#if __name__ == "__main__":
#
#    foo, count = calculate_count_rate_flux_table(flux_file, TX, RX, area_file = "C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\HUSIR_CY2018_flux_table.xlsx", debug_path = DEBUG)
#foo.to_excel("C:\\Users\\jimurray\\Python_Scripts\\REPOS\\Bistatic\\results\\HUSIR_CY2018_flux_table.xlsx")

#%%
#    fname = str(i) + "_" + pair[0].name + "_" + pair[1].name + "_" + pair[0].transmitter.band + "_band.txt"
#        
#    #outfile = open(fname, 'w')
#
#    RCS = bs.RCSVrange([R_int], pair[0],pair[1],plot=False, atmospheric_loss = True)
#    size = bs.sizeVrange([R_int], pair[0],pair[1], plot=False, atmospheric_loss = True)
#    
##    if pair[0].name == 'Arecibo':
##        area = bs.areaVrange_tx(R0,pair[0],pair[1], plot = False)
##    else:
##        area = bs.areaVrange_rx(R0,pair[0],pair[1], plot = False)
#    
#    sqkm2sqm = 1.0e6
#    hoursPerYear = 8760.
#
#    if i == 0:
#        flux_file = "FLUX_TEL_HUSIR.OUT"
#        flux_data = np.genfromtxt(flux_file, delimiter="   ", skip_header=5, usecols=(0,5))
#    else:
#        flux_file = "Arecibo.OUT"
#        flux_data = np.genfromtxt(flux_file, delimiter="   ", skip_header=5, usecols=(0,5))
#    
#    R, indicies = np.unique(flux_data.T[0], return_index = True)
#    flux = (flux_data.T[1])[indicies]
#    
#    #dR = R[1]-R[0]
#    
#    alt_limit = np.where(np.logical_and(R <= 2000., R >= 300.))[0]
#    
#    if pair[0].name == 'Arecibo':
#        flux_area = bs.areaVrange_tx(R[alt_limit],pair[0],pair[1], plot = False)
#    else:
#        flux_area = bs.areaVrange_rx(R[alt_limit],pair[0],pair[1], plot = False)
#    
#    flux_RCS = bs.RCSVrange(R[alt_limit],pair[0],pair[1], plot = False, atmospheric_loss = True)
#    flux_size_high = bs.SEM(flux_RCS, pair[0].wavelength)
#    flux_size_mean = bs.SEM(flux_RCS/0.6870376035296604, pair[0].wavelength)
#    flux_size_low  = bs.SEM(flux_RCS*4., pair[0].wavelength)
#    
#    flux_total_low = []
#    flux_total_mean = []
#    flux_total_high = []
#    for r, SIZE_low, SIZE_high, SIZE_mean in zip(R[alt_limit], flux_size_low, flux_size_high, flux_size_mean):
#        index_low  = np.argmin(abs(fluxes[int(r)][0] - SIZE_low))
#        index_mean = np.argmin(abs(fluxes[int(r)][0] - SIZE_mean))
#        index_high = np.argmin(abs(fluxes[int(r)][0] - SIZE_high))
#        
#        flux_total_low.append(fluxes[int(r)][1][index_low])
#        flux_total_mean.append(fluxes[int(r)][1][index_mean])
#        flux_total_high.append(fluxes[int(r)][1][index_high])
#    
#    
#    #plt.savefig("HUSIR_area.png")
#    
#    count_rate = flux_area*flux[alt_limit]*sqkm2sqm/hoursPerYear
#    
#    flux_total_low = np.array(flux_total_low)
#    count_rate_total_low = flux_area*flux_total_low*sqkm2sqm/hoursPerYear
#    
#    flux_total_high = np.array(flux_total_high)
#    count_rate_total_high = flux_area*flux_total_high*sqkm2sqm/hoursPerYear
#    
#    flux_total_mean = np.array(flux_total_mean)
#    count_rate_total_mean = flux_area*flux_total_mean*sqkm2sqm/hoursPerYear
#    
#    total_rate = np.sum(count_rate)
#    total_rate_total_low  = np.sum(count_rate_total_low)
#    total_rate_total_high = np.sum(count_rate_total_high)
#    total_rate_total_mean = np.sum(count_rate_total_mean)
#
#    
#    
##    plt.clf()
##    plt.bar(R[alt_limit],count_rate,dR,label = "Total Rate: " + str(total_rate) + " #/hr")
##    plt.title("Particle Count Rates for HUSIR Radar (> 1 cm) from 200-2000 km")
##    plt.xlabel("Altitude [km]")
##    plt.ylabel("Number of Particles per Hour")
##    plt.legend()
##    plt.grid()
##    plt.show()
##    plt.savefig("HUSIR_flux.png")
#    
#    baseline_RCS  = dB2lin(-49.0143334524) # m^2
#    baseline_size = 5.1342086282   # mm
#    baseline_area = 4700.45715251  # km^2
#    baseline_flux = 6.22153804222  # per hour
#    baseline_flux_total_high = 54.54903516641725 # per hour
#    baseline_flux_total_low = 21.251634119733474 # per hour
#    baseline_flux_total_mean = 1.
#    
#    with open(fname, 'w') as outfile:
#        outfile.write("Transmitter: " + pair[0].name + '\n')
#        outfile.write("Receiver: " + pair[1].name + '\n')
#        outfile.write("Band: " + pair[0].transmitter.band + '\n')
#        outfile.write("Center Frequency: " + str(lam2f(pair[0].wavelength)/MHz) + " MHz\n")
#        outfile.write("TX Pulse Length: " + str(pair[0].transmitter.pulse_length/us) + " microsec\n")
#        outfile.write("TX Power: " + str(pair[0].transmitter.power/kW) + " kW\n")
#        outfile.write("TX Bandwidth: " + str(pair[0].transmitter.BW/kHz) + " kHz\n")
#        outfile.write("TX Gain: " + str(10*np.log10(pair[0].transmitter.peak_gain)) + " dB\n")
#        outfile.write("RX Bandwidth: " + str(pair[1].receiver.BW/1000000.) + " MHz\n")
#        outfile.write("RX Gain: " + str(lin2dB(pair[1].receiver.peak_gain)) + " dB\n")
#        outfile.write("RX Temperature: " + str(pair[1].receiver.T_sys) + " K\n")
#        outfile.write("RX Minimum SNR: " + str(lin2dB(pair[1].receiver.SNR_min)) + " dB\n\n")
#        outfile.write("Minimum RCS @ 1000km: " + str(lin2dB(RCS[0]))+ " dBsm\n")
#        outfile.write("Minimum Size @ 1000km: " + str(size[0]/mm)+ " mm\n")
#        outfile.write("Intersection Area from 300-2000km: " + str(np.sum(flux_area))+ " km^2\n")
#        outfile.write("Count Rate of >1cm from 300-2000km: " + str(total_rate) + " per hour\n\n")
#        outfile.write("Total Count Rate from 300-2000km:  " + str(total_rate_total_low)+ '-'+str(total_rate_total_high)+ '-'+str(total_rate_total_mean) + " per hour " + str(total_rate_total_low / baseline_flux_total_low)  + "-"+ str(total_rate_total_high / baseline_flux_total_high)   + "-"+ str(total_rate_total_mean / baseline_flux_total_mean)+ " \n \n")
#
#        outfile.write("                                    &  Absolute                                                                 & Relative \\ \hline \n")
#        outfile.write("Minimum RCS @ 1000km:               & " + str(lin2dB(RCS[0]))                                      + " dBsm     & " + str(RCS[0]/  baseline_RCS)              + " \\ \hline \n")
#        outfile.write("Minimum Size @ 1000km:              & " + str(size[0]/mm)                                          + " mm       & " + str(size[0]/mm / baseline_size)         + " \\ \hline \n")
#        outfile.write("Intersection Area from 300-2000km:  & " + str(np.sum(flux_area))                                        + " km$^2$   & " + str(np.sum(flux_area) / baseline_area)       + " \\ \hline \n")
#        outfile.write("Count Rate of >1cm from 300-2000km: & " + str(total_rate)                                          + " per hour & " + str(total_rate / baseline_flux)         + " \\ \hline \n")
#        outfile.write("Total Count Rate from 300-2000km:   & " + str(total_rate_total_low)+'-'+str(total_rate_total_high) +'-'+str(total_rate_total_mean) +" per hour & " + str(total_rate_total_low/baseline_flux_total_low)+"-"+str(total_rate_total_high/baseline_flux_total_high) +"-"+str(total_rate_total_mean/baseline_flux_total_mean)  + " \\ \hline \n")
#
#        #outfile.write("\n\n\n")
##        allfile.write("Transmitter: " + pair[0].name + '\n')
##        allfile.write("Receiver: " + pair[1].name + '\n')
##        allfile.write("Band: " + pair[0].transmitter.band + '\n')
##        allfile.write("Minimum RCS @ 1000km: " + str(10*np.log10(RCS))+ " dBsm" + '\n')
##        allfile.write("Minimum Size @ 1000km: " + str(size*1000)+ " mm" + '\n')
##        allfile.write("Intersection Area from 300-2000km: " + str(np.sum(area))+ " km^2" + '\n')
##        allfile.write("\n\n\n")
#        print pair[0].name, pair[0].transmitter.band
#        print pair[1].name, pair[1].receiver.band
#        print 10*np.log10(RCS), " dBsm"
#        print size*100, " cm"
#        print np.sum(flux_area), " km^2"
#        #i=i+1
#        #outfile.close()
#    
#
#if __name__ == "__main__":
#    
#    start_time = time()
#    
#    #R = np.arange(300,2010,10)
#    R_mono = np.linspace(300,2000,400)
#    R_bi = np.linspace(900,1100,1000)
#    
#    
#    R_int = 1000.
#    
#    pairs = []
#    
#    
#    #HUSIR baseline
#    obs.HUSIR.set_pointing(90.,75.)
#    pairs.append([obs.HUSIR.as_a('transmitter'),obs.HUSIR.as_a('receiver')])
#    
#    #Arecibo UHF CW
#    obs.AO.set_transmitter(obs.tx.AO_UHF)
#    obs.AO.set_receiver(obs.rx.AO_UHF)
#    
#    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #Arecibo UHF World Day
#    obs.AO.set_transmitter(obs.tx.AO_UHF_WD)
#    obs.AO.set_receiver(obs.rx.AO_UHF)
#    
#    pairs.append([obs.AO.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #AO-VLBI S-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
#    obs.VLBI.set_pointing(az,el)
#    
#    obs.AO.set_transmitter(obs.tx.AO_S)
#    obs.VLBI.set_receiver(obs.rx.VLBI_S)
#    obs.VLBI.receiver.BW = obs.AO.transmitter.BW
#    
#    pairs.append([obs.AO.as_a('transmitter'),obs.VLBI.as_a('receiver')])
#    
#    #AO-VLBA S-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBA)
#    obs.VLBA.set_pointing(az,el)
#    
#    obs.AO.set_transmitter(obs.tx.AO_S)
#    obs.VLBA.set_receiver(obs.rx.VLBA_S)
#    obs.VLBA.receiver.BW = obs.AO.transmitter.BW
#    
#    pairs.append([obs.AO.as_a('transmitter'),obs.VLBA.as_a('receiver')])
#    
#    #AO-GBT S-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.GBT)
#    obs.GBT.set_pointing(az,el)
#    
#    obs.AO.set_transmitter(obs.tx.AO_S)
#    obs.GBT.set_receiver(obs.rx.GBT_S)
#    obs.GBT.receiver.BW = obs.AO.transmitter.BW
#    
#    pairs.append([obs.AO.as_a('transmitter'),obs.GBT.as_a('receiver')])
#    
#    #HUSIR-AO X-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.HUSIR)
#    obs.HUSIR.set_pointing(az,el)
#    
#    obs.HUSIR.set_transmitter(obs.tx.HUSIR)
#    obs.AO.set_receiver(obs.rx.AO_X)
#    obs.AO.receiver.BW = obs.HUSIR.transmitter.BW
#    
#    pairs.append([obs.HUSIR.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #Millstone-AO L-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.Millstone)
#    obs.Millstone.set_pointing(az,el)
#    
#    obs.Millstone.set_transmitter(obs.tx.Millstone)
#    obs.AO.set_receiver(obs.rx.AO_ALFA)
#    obs.AO.receiver.BW = obs.Millstone.transmitter.BW
#    
#    pairs.append([obs.Millstone.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #VLBI-AO L-band
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.VLBI)
#    obs.VLBI.set_pointing(az,el)
#    
#    obs.VLBI.set_transmitter(obs.tx.COTS_L)
#    obs.AO.set_receiver(obs.rx.AO_ALFA)
#    obs.AO.receiver.BW = obs.VLBI.transmitter.BW
#    
#    pairs.append([obs.VLBI.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #FPS-85-AO 
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_85)
#    obs.FPS_85.set_pointing(az,el)
#    
#    obs.FPS_85.set_transmitter(obs.tx.FPS_85)
#    obs.AO.set_receiver(obs.rx.AO_UHF)
#    obs.AO.receiver.BW = obs.FPS_85.transmitter.BW
#    
#    pairs.append([obs.FPS_85.as_a('transmitter'),obs.AO.as_a('receiver')])
#    
#    #FPS-16-AO 
#    slant_range, az, el = bs.R_az_el(R_int, obs.AO, obs.FPS_16)
#    obs.FPS_16.set_pointing(az,el)
#    
#    obs.FPS_16.set_transmitter(obs.tx.FPS_16)
#    obs.AO.set_receiver(obs.rx.AO_C)
#    obs.AO.receiver.BW = obs.FPS_16.transmitter.BW
#    
#    pairs.append([obs.FPS_16.as_a('transmitter'),obs.AO.as_a('receiver')])
#
#
#    Parallel(n_jobs=-1)(delayed(run_pairs)(i, pair) for i, pair in enumerate(pairs))
##    for i, pair in enumerate(pairs):
##        run_pairs(i,pair)
#    
#    end_time = time()
#    print "Execution took: ", end_time - start_time, " seconds..."
