# -*- coding: utf-8 -*-
"""
Created on Tue May 4 16:20:49 2018

@author: jimurray
"""

from .conversion import G_k2G, dB2lin, f2lam

from .constants import MW, kW, W,\
                      ns, us, ms, s,\
                      km, m, cm, mm,\
                      GHz, MHz, kHz, Hz,\
                      MW, kW, W,\
                      deg, arcsec, arcmin

class Transmitter:
    def __init__(self, power, pulse_length, wavelength, peak_gain, BW = None, name = '', band=''):
        self.power = power
        self.pulse_length = pulse_length
        self.wavelength = wavelength
        #self.TBP = TBP
        #self.BW = TBP/pulse_length
        self.peak_gain = peak_gain
        self.name = name
        self.band = band
        if BW == None:
            self.BW = 1./pulse_length
        else:
            	self.BW = BW

Goldstone = Transmitter(power=440.*kW, pulse_length=2.34*ms, wavelength=3.502*cm, peak_gain=dB2lin(74.55), band='X')

HUSIR = Transmitter(power=250*kW, pulse_length=1.6384*ms, wavelength=3.0*cm, peak_gain=dB2lin(67.23), band='X')
HAX   = Transmitter(power= 50*kW, pulse_length=1.6384*ms, wavelength=1.8*cm, peak_gain=dB2lin(63.64), band='Ku')

#AO_UHF    = Transmitter(power=2.5*MW, pulse_length=2.1*ms, BW= 2*MHz, wavelength=f2lam( 430*MHz), peak_gain=dB2lin(58.9), band='UHF')
#AO_UHF_WD = Transmitter(power=2.5*MW, pulse_length=440*us, BW= 1*MHz, wavelength=f2lam( 430*MHz), peak_gain=dB2lin(58.9), band='UHF')
#AO_S      = Transmitter(power=  1*MW, pulse_length=  3*ms, BW=10*MHz, wavelength=f2lam(2380*MHz), peak_gain=dB2lin(73.7), band='S')
#AO_S_CW   = Transmitter(power=  1*MW, pulse_length=150*ms, BW=10*MHz, wavelength=f2lam(2380*MHz), peak_gain=dB2lin(73.7), band='S')
#COTS_L    = Transmitter(power= 10*kW, pulse_length= 25*us, BW=None,   wavelength=f2lam(1215*MHz), peak_gain=dB2lin(35.3), band='L')
#
#NGAT_S = Transmitter(power=  8*MW, pulse_length=1.6384*ms, BW=None, wavelength=f2lam(2380*MHz), peak_gain=G_k2G(20.,f2lam(2380*MHz)), band='S')
#NGAT_C = Transmitter(power=  5*MW, pulse_length=1.6384*ms, BW=None, wavelength=f2lam(5000*MHz), peak_gain=G_k2G(18.,f2lam(5000*MHz)), band='C')
#
#
#VLBI_L    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(1375*MHz), peak_gain=G_k2G(.024,f2lam(1375*MHz)), band='L')
#VLBI_S    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(2380*MHz), peak_gain=G_k2G(.024,f2lam(2380*MHz)), band='S')
#VLBI_C    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(5480*MHz), peak_gain=G_k2G(.024,f2lam(5480*MHz)), band='C')
#VLBI_X    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(10.0*GHz), peak_gain=G_k2G(.024,f2lam(10.0*GHz)), band='X')
#
#VLBA_L    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(1375*MHz), peak_gain=G_k2G(.087,f2lam(1375*MHz)), band='L')
#VLBA_S    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(2380*MHz), peak_gain=G_k2G(.087,f2lam(2380*MHz)), band='S')
#VLBA_C    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(5480*MHz), peak_gain=G_k2G(.087,f2lam(5480*MHz)), band='C')
#VLBA_X    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(10.0*GHz), peak_gain=G_k2G(.087,f2lam(10.0*GHz)), band='X')
#
##Altair = Transmitter(power=5.*10**6, pulse_length=80.*10**-6, wavelength=300./442, peak_gain=)
#FPS_85 = Transmitter(power=32*MW, pulse_length=250*us, wavelength=f2lam( 442*MHz), peak_gain=dB2lin(48.), band='UHF')
#FPS_16 = Transmitter(power= 3*MW, pulse_length=  1*us, wavelength=f2lam(5480*MHz), peak_gain=dB2lin(52.), band = 'C')
##SBX
##MCR
#
##assuming 100% aperture efficiency
#Millstone = Transmitter(power=3*MW, pulse_length=1*ms, wavelength=f2lam(1295*MHz), peak_gain=dB2lin(50.), band='L')
#
#GBT_Ka = Transmitter(power = 45*kW, pulse_length=1*ms, wavelength=f2lam(30*GHz), peak_gain=G_k2G(1.8,f2lam(30*GHz)), band='Ka')
#
#GBT_Ka = Transmitter(power = 500*kW, pulse_length=1.6384*ms, wavelength=f2lam(35*GHz), peak_gain=G_k2G(1.8,f2lam(35*GHz)), band='Ka')

AO_UHF    = Transmitter(power=2.5*MW, pulse_length=2.1*ms, BW=None, wavelength=f2lam( 430*MHz), peak_gain=dB2lin(58.9), band='UHF')
AO_UHF_WD = Transmitter(power=2.5*MW, pulse_length=440*us, BW=None, wavelength=f2lam( 430*MHz), peak_gain=dB2lin(58.9), band='UHF')
AO_S      = Transmitter(power=  1*MW, pulse_length=  3*ms, BW=None, wavelength=f2lam(2380*MHz), peak_gain=dB2lin(73.7), band='S')
AO_S_CW   = Transmitter(power=  1*MW, pulse_length=150*ms, BW=None, wavelength=f2lam(2380*MHz), peak_gain=dB2lin(73.7), band='S')
COTS_L    = Transmitter(power= 10*kW, pulse_length= 25*us, BW=None, wavelength=f2lam(1215*MHz), peak_gain=dB2lin(35.3), band='L')

NGAT_S = Transmitter(power=  8*MW, pulse_length=1.6384*ms, BW=None, wavelength=f2lam(2380*MHz), peak_gain=G_k2G(20.,f2lam(2380*MHz)), band='S')
NGAT_C = Transmitter(power=  5*MW, pulse_length=1.6384*ms, BW=None, wavelength=f2lam(5000*MHz), peak_gain=G_k2G(18.,f2lam(5000*MHz)), band='C')


VLBI_L    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(1375*MHz), peak_gain=G_k2G(.024,f2lam(1375*MHz)), band='L')
VLBI_S    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(2380*MHz), peak_gain=G_k2G(.024,f2lam(2380*MHz)), band='S')
VLBI_C    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(5480*MHz), peak_gain=G_k2G(.024,f2lam(5480*MHz)), band='C')
VLBI_X    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(10.0*GHz), peak_gain=G_k2G(.024,f2lam(10.0*GHz)), band='X')

VLBA_L    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(1375*MHz), peak_gain=G_k2G(.087,f2lam(1375*MHz)), band='L')
VLBA_S    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(2380*MHz), peak_gain=G_k2G(.087,f2lam(2380*MHz)), band='S')
VLBA_C    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(5480*MHz), peak_gain=G_k2G(.087,f2lam(5480*MHz)), band='C')
VLBA_X    = Transmitter(power= 0*kW, pulse_length= 1*ms, BW=None,   wavelength=f2lam(10.0*GHz), peak_gain=G_k2G(.087,f2lam(10.0*GHz)), band='X')

#Altair = Transmitter(power=5.*10**6, pulse_length=80.*10**-6, wavelength=300./442, peak_gain=)
FPS_85 = Transmitter(power=32*MW, pulse_length=250*us, wavelength=f2lam( 442*MHz), peak_gain=dB2lin(48.), band='UHF')
FPS_16 = Transmitter(power= 3*MW, pulse_length=  1*us, wavelength=f2lam(5480*MHz), peak_gain=dB2lin(52.), band = 'C')
#SBX
#MCR

#assuming 100% aperture efficiency
Millstone = Transmitter(power=3*MW, pulse_length=1*ms, wavelength=f2lam(1295*MHz), peak_gain=dB2lin(50.), band='L')

GBT_Ka = Transmitter(power = 45*kW, pulse_length=1*ms, wavelength=f2lam(30*GHz), peak_gain=G_k2G(1.8,f2lam(30*GHz)), band='Ka')

GBT_Ka = Transmitter(power = 500*kW, pulse_length=1.6384*ms, wavelength=f2lam(35*GHz), peak_gain=G_k2G(1.8,f2lam(35*GHz)), band='Ka')