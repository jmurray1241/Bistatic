# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:07:35 2020

@author: jimurray
"""
import os
# from . import config
import numpy as np
import pandas as pd

def optimal_samples(error_c):
    error = pd.read_excel(os.path.join(config.path,"results/error_analysis/areaVsamples.xlsx"))
    samples = error['Samples'][error['75E'] >= error_c].iloc[0]
#    a = -13.83182199   
#    b = 1.66633599
#    samples = int(np.ceil((-1./a*(1-error_c))**(-1./b)))
    return samples
#%%
def optimal_steps(R_0, R_N, error_a = .999, error_c = .9995):
    
    N = int(np.ceil(error_c/(error_c-error_a)*((R_N - R_0)/(R_N + R_0))))
    
    return N

def optimal_steps2(R_0, R_N, error_a = .999, error_c = .9995):
    
    N = int(np.ceil(abs((error_c/(error_c + error_a - 2))*((R_N - R_0)/(R_N + R_0)))))
    
    return N
