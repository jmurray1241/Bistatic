# -*- coding: utf-8 -*-
"""
Created on Tue May 08 09:55:33 2018

@author: jimurray
"""

import numpy as np
from scipy.special import j1
from .conversion import deg2rad

def airy(theta, radius, lam, peak_gain=1.):
    """Calculate the gain of a circular aperture"""
    
    if theta == 0:
        return 1.

    theta = deg2rad(theta)

    k = 2*np.pi/lam
    x = k*radius*np.sin(theta)
    gain = peak_gain*(2*j1(x)/(x))**2
    if np.isnan(gain):
        print("theta: " + str(theta))
    return gain

airy = np.vectorize(airy)
airy.__name__ = "airy"

def airy_cos(costheta, radius, lam, peak_gain=1.):
    """Calculate the gain of a circular aperture without using arccos"""
        
    sintheta = np.sqrt(1-costheta**2)
    
    k = 2*np.pi/lam
    x = k*radius*sintheta
    if x == 0:
        return 1.

    return peak_gain*(2*j1(x)/(x))**2

airy_cos = np.vectorize(airy_cos)
airy_cos.__name__ = "airy_cos"


def gaussian(theta, radius, lam, peak_gain=1., fit='volume'):
	"""Calculate the gain of a circular aperture using a gaussian profile"""
	if fit == 'volume':
		sigma = 0.45*lam/(2*radius)
	elif fit == 'best':
		sigma = 0.42*lam/(2*radius)
	else:
		print("\'fit\' must be \'volume\' or \'best\'...")
		return 0

	theta = deg2rad(theta)

	return peak_gain*np.exp(-np.tan(theta)**2/(2*sigma**2))

def obscured_airy(theta, radius, lam, obscuration_ratio = .1):
    """Calculate the gain of an obscured circular aperture"""
    
    theta = deg2rad(theta)

    k = 2*np.pi/lam
    x = k*radius*np.sin(theta)
    
    e = obscuration_ratio
    
    gain = (1-e**2)**-2*(2*j1(x)/(x) - 2*e*j1(e*x)/(x))**2

    return gain