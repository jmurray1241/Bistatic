# -*- coding: utf-8 -*-
"""
Created on Tue May 4 16:20:49 2018

@author: jimurray
"""
import numpy as np
from copy import deepcopy
import nvector as nv

from . import transmitter as tx
from . import receiver as rx

from .conversion import deg2rad, rad2deg
from .constants import km, cm, speed_of_light, MHz, R_e, f
from .vector import I, x_hat, y_hat, z_hat, vec2nv, nv2vec, unit, angle_between, Rot_from_to


#c = 299792458.0 # meters per sec
lam_x = 3.0*cm # m
lam_x_DSS = speed_of_light/8560./MHz
lam_x_DSS_13 = .04193 # m
lam_s = 13.0*cm # m
lam_l = speed_of_light/1295./MHz
lam_ku = 1.8*cm



wgs84 = nv.FrameE(name='WGS84')

class Observer:
    def __init__(self, HPBW, radius, name = '', wavelength = lam_x):
        #self.set_location(latitude,longitude, altitude)
        #self.set_pointing(azimuth, elevation)
        self.HPBW = HPBW
        self.wavelength = wavelength
        self.radius = radius
        self.name = name
        self.sensor = None

    def set_pos_from_latlon(self, latitude, longitude, altitude, azimuth = 0., elevation = 90.):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.n = nv2vec(nv.lat_lon2n_E(deg2rad(latitude), deg2rad(longitude)))
        self.p = nv2vec(nv.n_EB_E2p_EB_E(vec2nv(self.n), -altitude))
        self.k_east = unit(np.cross(z_hat,self.n))
        self.k_north = np.cross(self.n,self.k_east)
        self.point = wgs84.GeoPoint(latitude=latitude, longitude=longitude, degrees=True)
        self.set_pointing(azimuth, elevation)

    def set_pos_from_p(self, p, azimuth=0., elevation=90.):
        self.p = nv2vec(p)
        n, depth = nv.p_EB_E2n_EB_E(p,a=R_e,f=1./f)
        self.altitude = -depth
        #n, self.altitude = nv.p_EB_E2n_EB_E(p)
        self.n = nv2vec(n)
        lat,lon = nv.n_E2lat_lon(n)
        self.latitude = rad2deg(lat)
        self.longitude = rad2deg(lon)
        self.k_east = unit(np.cross(z_hat,self.n))
        self.k_north = np.cross(self.n,self.k_east)
        self.point = wgs84.GeoPoint(latitude=self.latitude, longitude=self.longitude, degrees=True)
        self.set_pointing(azimuth, elevation)

    def set_pointing(self, azimuth, elevation):
        self.azimuth = azimuth
        self.elevation = elevation
        self._set_b()
        self._set_Rot()
        self._set_altitude_interp()
  
    def _set_b(self):
        zag = deg2rad(90.0 - self.elevation)
        az = deg2rad(self.azimuth)
        
        n = np.cos(zag)
        k_north = np.sin(zag)*np.cos(az)
        k_east  = np.sin(zag)*np.sin(az)
        
        self.b = n*self.n + k_east*self.k_east + k_north*self.k_north
        # Verify that b is normalized since rounding can make it not normalized
        self.b /= np.linalg.norm(self.b)

    
    def _set_Rot(self):
        """Sets the observer rotation matrix"""
        self.Rot = Rot_from_to(z_hat,self.b)
        
    def slant2alt(self, slant):
        """Returns perpendicular distance from Earth ellipsoid in km"""
        n, depth = nv.p_EB_E2n_EB_E(vec2nv(self.p + self.b*slant*km))
        #print "Altitude: " + str(-depth/1000) + " km"
        return -depth/km

    def slant2alt_interp(self, slant):
        """Returns Slant Range from altitude for the set pointing"""
        return np.interp(slant, self._R, self._alt)

    def alt2slant(self, alt):
        """Returns Slant Range from altitude for the set pointing"""
        return np.interp(alt, self._alt, self._R)      
    
    def orbit_alt2slant(self, alt):
        """Returns Slant Range from orbit altitude for the set pointing"""
        p = np.linalg.norm(self.p)
#        theta = np.arccos(self.p.dot(self.b)/p)
        cos_theta = self.p.dot(self.b)/p
        alt = alt*km
#        slant = np.sqrt((alt + R_e)**2 + p**2*(np.cos(theta)**2 - 1)) - p*np.cos(theta)
        slant = np.sqrt((alt + R_e)**2 + p**2*(cos_theta**2 - 1)) - p*cos_theta
        slant = slant/km
        return slant
    
    def slant2orbit_alt(self, slant):
        """Returns orbit altitude from slant range for the set pointing"""
        slant = slant*km
        alt = np.linalg.norm(self.p + self.b*slant) - R_e
        alt = alt/km
        return alt
    
    def set_receiver(self, receiver):
        """Sets the observer receiver"""
        self.receiver = receiver
#        self.HPBW = receiver.HPBW
    
    def set_transmitter(self, transmitter):
        """Sets the observer transmitter"""
        self.transmitter = transmitter
        self.wavelength = transmitter.wavelength

    def as_a(self, sensor):
        blah = deepcopy(self)
        if sensor == 'transmitter' or sensor == 'receiver':
            blah.sensor = sensor
            return blah
        else:
            print("Not a valid sensor type...")
            return 0
        
    def _set_altitude_interp(self):
        self._R = np.arange(0,50050,50,'float')
        blah = []
        for r in self._R:
            blah.append(self.slant2alt(r))
        self._alt = nv2vec(np.array(blah))
        


#    Antenna     X-axis, m    Y-axis, m   Z-axis, m
#    DSS 14    −2353621.251 −4641341.542 3677052.370
#    DSS 15    −2353538.790 −4641649.507 3676670.043
#    DSS 25    −2355022.066 −4646953.636 3669040.895
#    DSS 24    –2354906.711 –4646840.095 3669242.325
#    Station    Latitude       Longitude      Height, m
#    DSS 14  35 25 33.24518  243 6 37.66967   1002.114
#    DSS 15  35 25 18.67390  243 6 46.10495   0973.945
#    DSS 25  35 20 15.40450  243 7 28.69836   0960.862

#   Table 1: Geographic Locations of VLBA Stations		
#   Location         Latitude	   Longitude    Elevation
#   Saint Croix, VI	17:45:23.68	 64:35:01.07	-15	
#   Green Bank, WV	38:25:59.24	 79:50:23.41	824
#   Arecibo, PR	    18:20:36.60	 66:45:11.10	451	



#p_14 = np.array([[-2353621.251], [-4641341.542], [3677052.370]])
#p_15 = np.array([[-2353538.790], [-4641649.507], [3676670.043]])
#p_25 = np.array([[-2355022.066], [-4646953.636], [3669040.895]])
#p_24 = np.array([[-2354906.711], [-4646840.095], [3669242.325]])
#
#DSS_14 = Observer(HPBW=0.03, radius=70./2, name='DSS-14', wavelength=lam_x_DSS)
#DSS_14.set_pos_from_p(p_14,azimuth=90., elevation=75.)
#
#DSS_15 = Observer(HPBW=0.06, radius=34./2, name='DSS-15', wavelength=lam_x_DSS)
#DSS_15.set_pos_from_p(p_15, azimuth=89.805, elevation=75.024)
#
#DSS_25 = Observer(HPBW=0.06, radius=34./2,  name='DSS-25', wavelength=lam_x_DSS)
#DSS_25.set_pos_from_p(p_25, azimuth=87.646, elevation=75.077)
#
#DSS_24 = Observer(HPBW=0.06, radius=34./2,  name='DSS-24', wavelength=lam_x_DSS)
#DSS_24.set_pos_from_p(p_24, azimuth=87.646, elevation=75.077)
        
p_14 = np.array([[-2353621.251], [-4641341.542], [3677052.370]])
p_15 = np.array([[-2353538.790], [-4641649.507], [3676670.043]])
p_24 = np.array([[-2354906.495], [-4646840.128], [3669242.317]])
p_25 = np.array([[-2355022.066], [-4646953.636], [3669040.895]])
p_26 = np.array([[-2354890.967], [-4647166.925], [3668872.212]])
p_13 = np.array([[-2351112.491], [-4655530.714], [3660912.787]])
p_28 = np.array([[-2350101.849], [-4656673.447], [3660103.577]])

DSS_14 = Observer(HPBW=0.030, radius=70./2, name='DSS-14', wavelength = lam_x_DSS)
DSS_14.set_pos_from_p(p_14,azimuth=90., elevation=75.)
DSS_14.set_transmitter(tx.Goldstone)

DSS_15 = Observer(HPBW=0.060, radius=34./2, name='DSS-15', wavelength = lam_x_DSS)
DSS_15.set_pos_from_p(p_15, azimuth=89.805, elevation=75.024)
DSS_15.set_receiver(rx.Goldstone)

DSS_24 = Observer(HPBW=0.06, radius=34./2,  name='DSS-24', wavelength = lam_x_DSS)
DSS_24.set_pos_from_p(p_24, azimuth=87.646, elevation=75.077)
DSS_24.set_receiver(rx.Goldstone)

DSS_25 = Observer(HPBW=0.06, radius=34./2,  name='DSS-25', wavelength = lam_x_DSS)
DSS_25.set_pos_from_p(p_25, azimuth=87.646, elevation=75.077)
DSS_25.set_receiver(rx.Goldstone)

DSS_26 = Observer(HPBW=0.06, radius=34./2,  name='DSS-26', wavelength = lam_x_DSS)
DSS_26.set_pos_from_p(p_26, azimuth=87.646, elevation=75.077) #valid when range = 1100 km
DSS_26.set_receiver(rx.Goldstone)

DSS_13 = Observer(HPBW=0.06, radius=34./2,  name='DSS-13', wavelength = lam_x_DSS_13)
DSS_13.set_pos_from_p(p_13, azimuth=90., elevation=75.)

DSS_28 = Observer(HPBW=0.06, radius=34./2,  name='DSS-28', wavelength = lam_x_DSS_13)
DSS_28.set_pos_from_p(p_28, azimuth=90., elevation=75.)



TIRA = Observer(HPBW=0.5, radius=47./2,  name='TIRA')
TIRA.set_pos_from_latlon(latitude = 50.6166, longitude = 7.1296, altitude=0., \
                        azimuth = 90, elevation=75.)

Effelsburg = Observer(HPBW=0.16, radius=100./2,  name='Effelsburg')
Effelsburg.set_pos_from_latlon(latitude = 50.5247, longitude = 6.8828, altitude=319., \
                        azimuth = 90, elevation=75.)

p_63 = np.array([[4849092.647], [-360180.569], [4115109.113]]) 
DSS_63 = Observer(HPBW=0.03, radius=70./2,  name='DSS-63')
DSS_63.set_pos_from_p(p_63, azimuth=90., elevation=75.)




# Azimuth and Elevations are chosen to intersect zenith staring Arecibo at 800km except GBT which is 1000km
AO = Observer(HPBW=2./60, radius=305./2, name='Arecibo', wavelength=lam_s)
AO.set_pos_from_latlon(latitude=18.3435, longitude=-66.7530833333,  altitude=451., azimuth=0.0, elevation=90.0)
AO.set_transmitter(tx.AO_S)
AO.set_receiver(rx.AO_ALFA)

GBT = Observer(HPBW=12*2.38/60, radius=100./2, name='Green Bank',wavelength=lam_s)
GBT.set_pos_from_latlon(latitude=38.4331222222, longitude=-79.8398361111, altitude=824., \
                        azimuth=-50.339635112494335, elevation=8.2228530213332718)
GBT.set_receiver(rx.GBT_X)

VLBA = Observer(HPBW=19.44/60, radius=25./2, name='St. Croix', wavelength=lam_s)
VLBA.set_pos_from_latlon(latitude=17.7565777778, longitude=-64.5836305556, altitude=-15., \
                         azimuth=172.0720274637541, elevation=71.338692934465101)
VLBA.set_receiver(rx.VLBA_S)

VLBI = Observer(HPBW=.665, radius=12.0/2, name='VLBI Reference', wavelength=lam_s)
VLBI.set_pos_from_latlon(latitude=18.348221, longitude=-66.75159, altitude=0., \
                         azimuth=-67.522187313739707, elevation=89.962587674079273)
VLBI.set_receiver(rx.VLBI_S)
VLBI.set_transmitter(tx.COTS_L)

#HUSIR = Observer(HPBW=.058, radius=36.6/2, name='HUSIR', wavelength=lam_x)
#HUSIR.set_pos_from_latlon(latitude=42.623287, longitude=-71.488154, altitude=115.69, \
#                          azimuth=-66.386287923053672, elevation=3.0736023904233094)

p_HUSIR = np.array([[1492405.24925], [-4457266.35448], [4296880.2968]]) # From SATRAK DLLs and "official" MIT/LL LLH

HUSIR = Observer(HPBW=.058, radius=30.494994994994993/2, name='HUSIR', wavelength=lam_x)
HUSIR.set_pos_from_p(p_HUSIR, azimuth=90., elevation=75.)
#HUSIR.set_pos_from_latlon(latitude=42.623287, longitude=-71.488154, altitude=115.69, \
#                          azimuth=-90., elevation=75.)
HUSIR.set_transmitter(tx.HUSIR)
HUSIR.set_receiver(rx.HUSIR)

p_HAX = np.array([[1492488.40981], [-4457263.052], [4296833.47601]]) # From SATRAK DLLs and "official" MIT/LL LLH

HAX = Observer(HPBW=.1, radius=12.2/2, name='HAX', wavelength=lam_ku)
HAX.set_pos_from_p(p_HAX, azimuth=90., elevation=75.)
#HAX.set_pos_from_latlon(latitude=42.622835, longitude=-71.488291, altitude=101.11, \
#                          azimuth=90.0, elevation=75.)
HAX.set_transmitter(tx.HAX)
HAX.set_receiver(rx.HAX)

Millstone = Observer(HPBW=.6, radius=25.6/2, name='Millstone', wavelength=lam_l)
Millstone.set_pos_from_latlon(latitude=42.623287, longitude=-71.488154, altitude=115.69, \
                          azimuth=-66.386287923053672, elevation=3.0736023904233094)
Millstone.set_transmitter(tx.Millstone)

FPS_85 = Observer(HPBW=.7, radius=26.9/2, name='FPS-85', wavelength=300./442)
FPS_85.set_pos_from_latlon(latitude=30.5, longitude=273.7, altitude=0., \
                            azimuth=0., elevation=90.)
FPS_85.set_transmitter(tx.FPS_85)

FPS_16 = Observer(HPBW=.6, radius=3.9, name='FPS-16', wavelength=300./5480)
FPS_16.set_pos_from_latlon(latitude=28.226390, longitude=-80.599261, altitude=0., \
                            azimuth=0., elevation=90.)
FPS_16.set_transmitter(tx.FPS_16)




# Type               : Elevation over azimuth.
# Reflector Diameter : 20 meters  
# =============================================================================
#  F/D ratio          : 0.43 
#  Receiver           : dual frequency/dual polarizations
#                        Cooled (15K) HEMT at prime focus.
#  Frequency bands    : 2.2 to 2.6 GHz; 8.2 to 9.0 GHz 
#  Slew Speed         : 2 degrees per second, each axis. 
#  Surface Accuracy   : 0.8 mm rms. 
#  Geodetic Position  : 38 26'12.661" N.Lat (ref: NAD1983 datum)
#                       79 49'31.865" W.Long.
#                        Approx. height of elev.axis: 842 m
# 
# Performance Data:
# Calibrations in single-dish mode done in February find rms pointing residuals of 34", and other performance data as follows:
#                       X-band        S-band
#  System Temps          46 K          31 K
#  SEFD                 720 Jy        440 Jy
#  Aperture efficiency  55-59%        58-61%
#     
# Geocentric CIO position from VLBI data in Reference Frame Navy 1996-9.
#     X =   883772.7974 m  +- 4.3 mm
#     Y = -4924385.5975 m  +- 5.9 mm
#     z =  3944042.4991 m  +- 5.2 mm
# =============================================================================
