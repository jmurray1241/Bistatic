# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 11:13:46 2018

@author: jimurray
"""

import numpy as np
from .constants import A_e_per_G_k, speed_of_light

def G_k2G(G_k,wavelength):
    G = 4*np.pi*G_k*A_e_per_G_k/wavelength**2
    return G

def dB2lin(gain_dB):
    gain = 10.**(gain_dB/10.)
    return gain

def lin2dB(gain):
    gain_dB = 10.*np.log10(gain)
    return gain_dB

def f2lam(frequency):
    wavelength = speed_of_light/frequency
    return wavelength

def lam2f(wavelength):
    frequency = speed_of_light/wavelength
    return frequency

def deg2rad(angle):
    """Returns an angle in radians"""
    return angle*np.pi/180

def rad2deg(angle):
    """Returns an angle in degrees"""
    return angle*180./np.pi
