# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 12:40:48 2021

@author: maward2
"""

import numpy as np
from .constants import k_B

def marcumsq (SNR_truth_dB, SNR_tresh_dB, max_test_value = 1000.):
    #% This function uses Parl's method to compute PD
    
    a = np.sqrt(2.*10.**(SNR_truth_dB/10.))
    b = np.sqrt(2.*10.**(SNR_tresh_dB/10.))
    
    if (a < b):
        alphan0 = 1.0;
        dn = a / b;
    else:
        alphan0 = 0.;
        dn = b / a;
    #end
    alphan_1 = 0.;
    betan0 = 0.5;
    betan_1 = 0.;
    d1 = dn;
    n = 0;
    ratio = 2.0 / (a * b);
    r1 = 0.0;
    betan = 0.0;
    alphan = 0.0;
    while betan < max_test_value:
        n = n + 1;
        alphan = dn + ratio * n * alphan0 + alphan;
        betan = 1.0 + ratio * n * betan0 + betan;
        alphan_1 = alphan0;
        alphan0 = alphan;
        betan_1 = betan0;
        betan0 = betan;
        dn = dn * d1;
    #end
    PD = (alphan0 / (2.0 * betan0)) * np.exp( -(a-b)**2 / 2.0);
    if ( a >= b):
        PD = 1.0 - PD;
    #end
    
    #James Added
    if SNR_truth_dB < SNR_tresh_dB:
        PD = 0
    
    return PD

marcumsq = np.vectorize(marcumsq)

def SNR(RCS, Pt, Gt, Gr, wavelength, Rt, Rr, T, BWr, Lr, power_pattern_product = 1, tau = 1):
    return tau*RCS*Pt*Gt*Gr*wavelength**2/((4*np.pi)**3*Rt**2*Rr**2*k_B*T*BWr*Lr) *power_pattern_product

    
