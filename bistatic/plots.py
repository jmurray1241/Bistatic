# -*- coding: utf-8 -*-
"""
Created on Mon May 25 14:03:42 2020

@author: jimurray
"""

import os

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.spatial import ConvexHull

from .conversion import lin2dB

from .constants import MW, kW, W,\
                      ns, us, ms, s,\
                      km, m, cm, mm,\
                      GHz, MHz, kHz, Hz,\
                      MW, kW, W,\
                      deg, arcsec, arcmin,\
                      speed_of_light, k_B

def along_name(TXname, RXname, along):
    if along == 'tx':
        return TXname
    else: 
        return RXname

def gain_plot(x,y,G):
    X,Y = np.meshgrid(x,y)
    X=X/km
    Y=Y/km

    plt.subplot(221)
    plt.imshow(G,aspect = 'auto',vmin = 0, vmax = 1,extent=[X.min(),X.max(),Y.min(),Y.max()])
    plt.xlabel("X [km]")
    plt.ylabel("Y [km]")
    plt.colorbar().set_label("Gain",rotation = 270)
    plt.subplot(222,projection = '3d').plot_surface(Y,X,G,cmap='viridis')
    plt.xlabel("X [km]")
    plt.ylabel("Y [km]")
    plt.subplot(223)
    plt.imshow(lin2dB(G), aspect='auto', vmin=-200, vmax=0, extent=[X.min(),X.max(),Y.min(),Y.max()])
    plt.xlabel("X [km]")
    plt.ylabel("Y [km]")
    plt.colorbar().set_label("Gain [dB]",rotation = 270)
    plt.subplot(224,projection = '3d').plot_surface(Y,X,lin2dB(G),cmap='viridis')
    plt.xlabel("X [km]")
    plt.ylabel("Y [km]")
    plt.show()
    
def plot_gainVrange(R, gain, TXname, RXname, along):
    name = along_name(TXname, RXname, along)
    plt.subplots(figsize=(11,8.5))        
    plt.suptitle(TXname + "/" + RXname + " Peak Gain v. " + name + " slant range")
    plt.subplot(211)
    plt.grid()
    plt.plot(R,gain)
    plt.xlabel(name + " Slant Range [km]")
    plt.ylabel("Peak Gain Product")
    
    plt.subplot(212)
    plt.plot(R,lin2dB(gain))
    plt.xlabel(name + " Slant Range [km]")
    plt.ylabel("Peak Gain Product [dB]")
    plt.grid()
    plt.show()
    
def plot_RCSVrange(R, RCS, TXname, RXname, along, band):
    name = along_name(TXname, RXname, along)
    fig, ax = plt.subplots(figsize=(11,8.5))
    plt.title(TXname+"/"+RXname+" Minimum Single Hit RCS v. "+name+" slant range")
    ax.plot(R,lin2dB(RCS),label=TXname+"/"+RXname+': '+ band +" band")
    plt.xlabel(TXname + " Slant Range [km]")
    plt.ylabel("Minimum RCS [dBsm]")
    plt.legend()
    plt.grid()
    plt.show()

def plot_sizeVrange(R, size, TXname, RXname, along, band):
    name = along_name(TXname, RXname, along)
    fig, ax = plt.subplots(figsize=(11,8.5))
    plt.title(TXname+"/"+RXname+" Minimum Detectable Size (single hit) v. "+name+" slant range")
    plt.grid()
    ax.plot(R,size/mm,label=TXname+"/"+RXname+': '+ band+" band")
    plt.xlabel(TXname + " Slant Range [km]")
    plt.ylabel("Minimum Size [mm]")
    plt.legend()
    plt.show()
    
def plot_areas(areas, TXname, RXname):
    
    step = areas.iloc[0]['ALT2_KM'] - areas.iloc[0]['ALT1_KM']
    
    fig, ax = plt.subplots(figsize=(11,8.5))
    ax.bar(areas['ALT1_KM'],areas['AREA_KM^2'], step, align = 'edge')        
    plt.xlabel("Orbit Altitude [km]")
    plt.ylabel("Composite Beam Area [km^2]")
    #Formula for the pointing range in the title is wrong
    plt.title(TXname+"/"+RXname+" Bistatic Beam Area")#: Pointing " +str(round(R[np.argmax(C)]))+" km, Az: " + str(TX.azimuth)+ " El: " +str(TX.elevation))
    plt.grid()

def area_debug(x,y,G,h_sub, debug, thresh = .25):
    i,j = np.where( G >= thresh)
    foox = np.array([x[i]])    
    fooy = np.array([y[j]])
    points = np.concatenate([foox,fooy]).T   
    fig, ax = plt.subplots(figsize = (11,8.5))
    plt.imshow(lin2dB(G), aspect = 'auto',  vmin=-200, vmax=0, extent=[x.min(),x.max(),y.min(),y.max()])
    X = []
    Y = []
    for i in range(len(x)):
        for j in range(len(y)):
            X.append(x[i])
            Y.append(y[j])
            
    plt.plot(X,Y,'b.', alpha = .05)
    plt.plot(points.T[0],points.T[1],'r.', alpha = .05)

    try:
        hull = ConvexHull(points)
        foo = points[np.append(hull.vertices, hull.vertices[0])]        
        plt.plot(foo.T[0],foo.T[1],'r*-', alpha = 1)
    except:
        pass 
    plt.xlabel('x[m]')
    plt.ylabel('y[m]')
    plt.title("Altitude = " + str(h_sub) + " km")
    plt.savefig(os.path.join(debug, "Altitude _" + str(h_sub) + "_km.png"))
    plt.close()
