# -*- coding: utf-8 -*-
"""
Created on May 4 16:20:49 2018

@author: jimurray
"""
import numpy as np

import bistatic as bs
import observer2 as obs
import receiver as rx
import transmitter as tx
from gain_pattern import airy, gaussian

ant0 = obs.DSS_14
ant1 = obs.DSS_15

#Desired pointing for AO
ant0.set_pointing(azimuth = 90., elevation = 75.)
slant_range = 550. # in km

print "\n"

#Example 1
#Calculate pointing for VLBI
bs.point_ant2ant(slant_range, ant0, ant1)

print "\n"

#Example 2
#Calculate surface distance and bearing from AO to VLBI
baseline, bearing = bs.baseline_bearing(ant0, ant1)

print "\n"

#Example 3
#Calculate and plot gain pattern at intersection
print "Close figure to continue..."
xpoints, ypoints, gain = bs.g_wgs(slant_range, ant0, ant1, plot=True)

#Define ranges based on DSS 15 receive window with 50km bins
R_min = 375. # km
R_max = 1375. # km
dR = 50. #km
R = np.arange(R_min, R_max+dR, dR).astype('float')

#Example 4
#Calculate and plot peak gain versus range using the above pointing
print "Close figure to continue..."
gainVrange = bs.gainVrange(R, ant0, ant1, plot=True)

#Example 5
#Calculate and plot intersection area versus range using the above pointing
print "Close figure to continue..."
areaVrange = bs.areaVrange_tx(R, ant0, ant1, plot=True)
print "Total Area: " + str(sum(areaVrange)) + " km^2"

## Set desired transmitters and receivers
#ant0.set_transmitter(tx.AO_S)
#ant1.set_receiver(rx.VLBI_S)
#
##Example 6
##Calculate and plot the minimum detectable RCS versus range
#print "Close figure to continue..."
#RCSVrange = bs.RCSVrange(R, ant0.as_a('transmitter'), \
#                         ant1.as_a('receiver'), plot = True) 
#
##Example 7
##Calculate and plot the minimum detectable size versus range
#print "Close figure to continue..."
#sizeVrange = bs.sizeVrange(R, ant0.as_a('transmitter'),  \
#                           ant1.as_a('receiver'), plot = True) 
#
##Example 8
##Calculate and plot intersection area over which
##a 1cm object can be seen versus range 
#print "Close figure to continue..."
#areaVrange = bs.areaVrange_tx(R, ant0.as_a('transmitter'), \
#                              ant1.as_a('receiver'), plot=True, \
#                              param = 'size', thresh = .01, gain = gaussian)








