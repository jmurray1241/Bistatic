# -*- coding: utf-8 -*-
"""
Created on May 4 16:20:49 2018

@author: jimurray
"""
import numpy as np
import matplotlib as plt

#import golstone as g
import bistatic as g
import observer as obs

#Desired pointing for DSS 14
obs.DSS_14.set_pointing(azimuth = 90., elevation = 75.)
DSS_14_slant_range = 550. # in km

print "\n"

#Example 1
#Calculate pointing for DSS 15
DSS_15_slant_range, az, el = g.R_az_el(DSS_14_slant_range, obs.DSS_14, obs.DSS_15)
obs.DSS_15.set_pointing(az,el)

print "\n"

#Example 2
#Calculate surface distance and bearing from DSS 14 to DSS 15
baseline, bearing = g.baseline_bearing(obs.DSS_14, obs.DSS_15)

print "\n"

#Example 3
#Calculate and plot gain pattern at intersection
print "Close figure to continue..."
xpoints, ypoints, gain = g.g_wgs(DSS_14_slant_range, obs.DSS_14, obs.DSS_15, plot=True)

#Define ranges based on DSS 15 receive window with 50km bins
R_min = 375. # km
R_max = 1375. # km
dR = 50. #km
R = np.arange(R_min, R_max+dR, dR).astype('float')

#Example 4
#Calculate and plot peak gain versus range using the above pointing
print "Close figure to continue..."
gainVrange = g.gainVrange(R, obs.DSS_14, obs.DSS_15, plot=True)

#Example 5
#calculate and plot intersection area versus range using the above pointing
print "Close figure to continue..."
areaVrange = g.areaVrange(R, obs.DSS_14, obs.DSS_15, plot=True)


