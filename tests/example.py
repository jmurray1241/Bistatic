# -*- coding: utf-8 -*-
"""
Created on May 4 16:20:49 2018

@author: jimurray
"""
import numpy as np

import bistatic as bs
import observer2 as obs
import receiver as rx
import transmitter as tx
from gain_pattern import airy, gaussian

#Desired pointing for AO
obs.AO.set_pointing(azimuth = 0., elevation = 90.)
AO_slant_range = 1000. # in km

print "\n"

#Example 1
#Calculate pointing for VLBI
bs.point_ant2ant(AO_slant_range, obs.AO, obs.VLBI)

print "\n"

#Example 2
#Calculate surface distance and bearing from AO to VLBI
baseline, bearing = bs.baseline_bearing(obs.AO, obs.VLBI)

print "\n"

#Example 3
#Calculate and plot gain pattern at intersection
print "Close figure to continue..."
xpoints, ypoints, gain = bs.g_wgs(AO_slant_range, obs.AO, obs.VLBI, plot=True)

#Define ranges based on DSS 15 receive window with 50km bins
R_min = 375. # km
R_max = 1375. # km
dR = 50. #km
R = np.arange(R_min, R_max+dR, dR).astype('float')

#Example 4
#Calculate and plot peak gain versus range using the above pointing
print "Close figure to continue..."
gainVrange = bs.gainVrange(R, obs.AO, obs.VLBI, plot=True)

#Example 5
#Calculate and plot intersection area versus range using the above pointing
print "Close figure to continue..."
areaVrange = bs.areaVrange_tx(R, obs.AO, obs.VLBI, plot=True)

# Set desired transmitters and receivers
obs.AO.set_transmitter(tx.AO_S)
obs.VLBI.set_receiver(rx.VLBI_S)

#Example 6
#Calculate and plot the minimum detectable RCS versus range
print "Close figure to continue..."
RCSVrange = bs.RCSVrange(R, obs.AO.as_a('transmitter'), \
                         obs.VLBI.as_a('receiver'), plot = True) 

#Example 7
#Calculate and plot the minimum detectable size versus range
print "Close figure to continue..."
sizeVrange = bs.sizeVrange(R, obs.AO.as_a('transmitter'),  \
                           obs.VLBI.as_a('receiver'), plot = True) 

#Example 8
#Calculate and plot intersection area over which
#a 1cm object can be seen versus range 
print "Close figure to continue..."
areaVrange = bs.areaVrange_tx(R, obs.AO.as_a('transmitter'), \
                              obs.VLBI.as_a('receiver'), plot=True, \
                              param = 'size', thresh = .01, gain = gaussian)








